<?php
/**
 * Created by PhpStorm.
 * User: eleonoria
 * Date: 11/7/16
 * Time: 5:12 PM
 */

namespace Tests\AppBundle\Controller;

use AppBundle\Controller\ProofQuestController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProofQuestControllerTest extends WebTestCase
{
    protected $proofController;
    
    protected function setUp()
    {
        $this->proofController = new ProofQuestController();
    }

    /**
     * @dataProvider userToQuestProvider
     */
    public function testGetUserToQuest()
    {
        $this->assertEquals(1, 1);
    }

    public function userToQuestProvider()
    {
        return [
            [1, 1],
            [2, 2],
            [3, 4]
        ];
    }
}