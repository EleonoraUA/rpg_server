<?php

namespace Tests\AppBundle\Manager;

use AppBundle\Entity\UserToQuest;
use AppBundle\Manager\ProofQuestManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProofQuestManagerTest extends WebTestCase
{

    protected $proofManager;

    protected function setUp()
    {
        $em = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();
        $this->proofManager = new ProofQuestManager($em);
    }

    /**
     * @dataProvider questReviewsProvider
     */
    public function testGetNewStateOfConfirmedQuest(UserToQuest $userToQuest, $reviews)
    {
        $method = new \ReflectionMethod($this->proofManager, 'getNewStateOfConfirmedQuest');
        $method->setAccessible(true);
        $userToQuest = new UserToQuest();
        $this->assertEquals(4, $method->invokeArgs($this->proofManager, array($userToQuest, $reviews)));
    }
    
    public function questReviewsProvider()
    {
        return [
            [1, 1, 1],
            [1, 0, 1],
        ];
    }
}