<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * MonsterRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MonsterRepository extends EntityRepository
{
    public function findMonsters($minLvl, $maxLvl)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('p')
            ->from('AppBundle:Monster', 'p')
            ->where('p.lvl >= :min_lvl')
            ->andWhere('p.lvl <= :max_lvl')
            ->setParameter('min_lvl', $minLvl)
            ->setParameter('max_lvl', $maxLvl)
            ->getQuery()->getResult();
    }
}
