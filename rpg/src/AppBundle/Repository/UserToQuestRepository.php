<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * UserToQuestRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserToQuestRepository extends EntityRepository
{
    public function findQuestsToReviewByParameters($parameters)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('utq')
            ->from('AppBundle:UserToQuest', 'utq')
            ->where($this->findNotInReviewedQuests())
            ->andWhere('utq.state = :state')
            ->andWhere('utq.reviewCount < :review_count')
            ->andWhere('utq.user != :user')
            ->setParameters($parameters)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    public function findTakenQuestsByUserId($userId)
    {
        return $this->getEntityManager()
            ->createQuery("SELECT IDENTITY(u.quest) FROM AppBundle:UserToQuest u WHERE u.user = :user_id")
            ->setParameter('user_id', $userId)
            ->getScalarResult();
    }

    public function findNotInReviewedQuests()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->expr()
            ->notIn('utq.id', ':reviewedQuests');
    }

    public function findInProgressQuestsByUserId($userId)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('uq')
            ->from('AppBundle:UserToQuest', 'uq')
            ->where('uq.user = :userId')
            ->andWhere($this->findQuestsWithStates())
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getResult();
    }

    public function findQuestsWithStates()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->expr()
            ->in('uq.state', array(1, 2, 4));
    }
}
