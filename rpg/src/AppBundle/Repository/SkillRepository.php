<?php

namespace AppBundle\Repository;

use AppBundle\AppBundle;
use Doctrine\ORM\EntityRepository;

/**
 * SkillRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SkillRepository extends EntityRepository
{
    public function findSkillsBySkillsIds($skillIds)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('u')
            ->from('AppBundle:Skill', 'u')
            ->where($this->findSkills())
            ->setParameter('skillIds', $skillIds)
            ->getQuery()
            ->getResult();
    }

    public function findSkills()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->expr()
            ->in('u.id', ':skillIds');
    }
}
