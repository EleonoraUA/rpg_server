<?php
/**
 * Class File AccountController
 *
 * PHP version 5.5
 *
 * @package AppBundle\Controller
 */
namespace AppBundle\Controller;

use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\RequestResponseTypesTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AccountController controls account's resources
 *
 * @package AppBundle\Controller
 */
class AccountController extends Controller
{
    use ErrorCodesTrait;
    use RequestResponseTypesTrait;
    use UserInfoFieldsTrait;

    /**
     * Returns resources (gold, crystals) of user's account
     *
     * @param Request $request
     *
     * @Route("/resources", name="resources")
     *
     * @return JsonResponse
     */
    public function getResourcesAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->RESOURCES_REQUEST
        );

        $validatedResponse = $this->get('app.manager.account')->getAccountFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }

        $account = $validatedResponse[$this->ACCOUNT];
        $response[$this->RESOURCES] = array(
            $this->GOLD => $account->getGold(),
            $this->CRYSTALS => $account->getCrystals()
        );

        $response[$this->STATUS] = $this->STATUS_OK;
        return new JsonResponse($response);
    }


    /**
     * Returns resources (gold, crystals) of user's account
     *
     * @param Request $request
     *
     * @Route("/char_profile", name="char_profile")
     *
     * @return JsonResponse
     */
    public function charProfileAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->RESOURCES_REQUEST
        );

        $validatedResponse = $this->get('app.manager.validator')->getPersonageFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            new JsonResponse($validatedResponse);
        }
        $personage = $validatedResponse[$this->PERSONAGE];

        $response = $this->get('app.manager.account')->getPersonageProfile($personage);
        $response[$this->SKILLS] = $this->get('app.manager.skillapi')->getSkillsProfile($personage);
        $response[$this->STATUS] = $this->STATUS_OK;
        return new JsonResponse($response);
    }

    /**
     * Change user's avatar
     *
     * @param Request $request
     *
     * @Route("/change_avatar", name="change_avatar")
     *
     * @return JsonResponse
     */
    public function changeAvatarAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->CHANGE_AVATAR_REQUEST
        );

        $response = $this->get('app.manager.account')->changeAvatar($data);
        return new JsonResponse($response);
    }
}
