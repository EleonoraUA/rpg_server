<?php
/**
 * Class File FetchQuestController
 *
 * PHP version 5.5
 *
 * @package AppBundle\Controller
 */
namespace AppBundle\Controller;

use AppBundle\Manager\Traits\RequestResponseTypesTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class FetchQuestController is responsible for fetching quest for user
 * @package AppBundle\Controller
 */
class FetchQuestController extends Controller
{
    use RequestResponseTypesTrait;

    /**
     * Action to get quests that user can take
     *
     * @param Request $request
     *
     * @Route("/quests", name="get_quests_to_take")
     *
     * @return JsonResponse
     */
    public function getQuestsToTakeAction(Request $request)
    {
        $response = $this->get('app.manager.fetchquest')->createResponse($request, $this->QUESTS_REQUEST);
        return new JsonResponse($response);
    }

    /**
     * Action to get quests that were confirmed (reviewed true) by other users
     *
     * @param Request $request
     *
     * @Route("/confirmed_quests", name="get_confirmed_quests")
     *
     * @return JsonResponse
     */
    public function getConfirmedQuestsAction(Request $request)
    {
        $response = $this->get('app.manager.fetchquest')->createResponse($request, $this->CONFIRMED_QUESTS_REQUEST);
        return new JsonResponse($response);
    }

    /**
     * Action to get quests that user can upload a proof
     *
     * @param Request $request
     *
     * @Route("/in_progress_quests", name="get_in_progress_quests")
     *
     * @return JsonResponse
     */
    public function getQuestsInProgress(Request $request)
    {
        $response = $this->get('app.manager.fetchquest')->createResponse($request, $this->INPROGRESS_QUESTS_REQUEST);
        return new JsonResponse($response);
    }
}
