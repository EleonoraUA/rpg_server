<?php

namespace AppBundle\Controller;

use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\RequestResponseTypesTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class FriendsController
 * @package AppBundle\Controller
 */
class FriendsController extends Controller
{
    use ErrorCodesTrait;
    use RequestResponseTypesTrait;
    use UserInfoFieldsTrait;

    /**
     * @param Request $request
     *
     * @Route("/friends", name="friends")
     *
     * @return JsonResponse
     */
    public function friendsRequestAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->FRIENDS_REQUEST
        );

        $validatedResponse = $this->get('app.manager.validator')->getUserIdFromData($data);
        if ($validatedResponse[$this->STATUS] !== $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }
        $response = $this->get('app.manager.friends')->friendsRequest($validatedResponse[$this->USER_ID]);
        return new JsonResponse($response);
    }

    /**
     * @param Request $request
     *
     * @Route("/friend_send_request", name="friend_send_request")
     *
     * @return JsonResponse
     */
    public function friendSendRequestAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->FRIEND_ACTION_REQUEST
        );

        $validatedResponse = $this->get('app.manager.validator')->getUserIdFromData($data);
        if ($validatedResponse[$this->STATUS] !== $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }
        $response = $this->get('app.manager.friends')
            ->sendFriendRequest($validatedResponse[$this->USER_ID], $data[$this->FRIEND_ID]);
        return new JsonResponse($response);
    }

    /**
     * @param Request $request
     *
     * @Route("/friend_delete", name="friend_delete")
     *
     * @return JsonResponse
     */
    public function friendDeleteAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->FRIEND_ACTION_REQUEST
        );

        $validatedResponse = $this->get('app.manager.validator')->getUserIdFromData($data);
        if ($validatedResponse[$this->STATUS] !== $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }
        $response = $this->get('app.manager.friends')
            ->deleteUserFromFriends($validatedResponse[$this->USER_ID], $data[$this->FRIEND_ID]);
        return new JsonResponse($response);
    }

    /**
     * @param Request $request
     *
     * @Route("/friend_accept_request", name="friend_accept_request")
     *
     * @return JsonResponse
     */
    public function friendAcceptRequestAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->FRIEND_ACTION_REQUEST
        );

        $validatedResponse = $this->get('app.manager.validator')->getUserIdFromData($data);
        if ($validatedResponse[$this->STATUS] !== $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }
        $response = $this->get('app.manager.friends')
            ->acceptFriendRequest($data[$this->FRIEND_ID], $validatedResponse[$this->USER_ID]);
        return new JsonResponse($response);
    }

    /**
     * @param Request $request
     *
     * @Route("/friend_skip_request", name="friend_skip_request")
     *
     * @return JsonResponse
     */
    public function friendSkipRequestAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->FRIEND_ACTION_REQUEST
        );

        $validatedResponse = $this->get('app.manager.validator')->getUserIdFromData($data);
        if ($validatedResponse[$this->STATUS] !== $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }
        $response = $this->get('app.manager.friends')
            ->skipFriendRequest($data[$this->FRIEND_ID], $validatedResponse[$this->USER_ID]);
        return new JsonResponse($response);
    }

    /**
     * @param Request $request
     *
     * @Route("/friend_cancel_request", name="friend_cancel_request")
     *
     * @return JsonResponse
     */
    public function friendCancelRequestAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->FRIEND_ACTION_REQUEST
        );

        $validatedResponse = $this->get('app.manager.validator')->getUserIdFromData($data);
        if ($validatedResponse[$this->STATUS] !== $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }
        $response = $this->get('app.manager.friends')
            ->cancelFriendRequest($validatedResponse[$this->USER_ID], $data[$this->FRIEND_ID]);
        return new JsonResponse($response);
    }

    /**
     * @param Request $request
     *
     * @Route("/friend_add_request", name="friend_add_request")
     *
     * @return JsonResponse
     */
    public function friendAddRequestAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->ADD_FRIEND_REQUEST
        );

        $validatedResponse = $this->get('app.manager.validator')->getUserIdFromData($data);
        if ($validatedResponse[$this->STATUS] !== $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }
        $response = $this->get('app.manager.friends')
            ->friendAddRequestByFriendName($validatedResponse[$this->USER_ID], $data[$this->USERNAME]);
        return new JsonResponse($response);
    }
}
