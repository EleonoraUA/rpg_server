<?php
/**
 * Class File ShopController
 *
 * PHP version 5.5
 *
 * @package AppBundle\Controller
 */
namespace AppBundle\Controller;

use AppBundle\Entity\Personage;
use AppBundle\Entity\Purchase;
use AppBundle\Manager\AccountManager;
use AppBundle\Manager\JSONValidatorManager;
use AppBundle\Manager\Traits\RequestResponseTypesTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Manager\SkillAPIManager;

/**
 * Class ShopController controls account's resources
 *
 * @package AppBundle\Controller
 */
class ShopController extends Controller
{
    use RequestResponseTypesTrait;

    /**
     * Returns shop units
     *
     * @param Request $request
     *
     * @Route("/shop", name="shop")
     *
     * @return JsonResponse
     */
    public function shopAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->SHOP_REQUEST
        );

        return new JsonResponse(
            $this->get('app.manager.shop')->showShopUnits($data)
        );
    }

    /**
     * Returns shop units
     *
     * @param Request $request
     *
     * @Route("/unit_buy", name="unit_buy")
     *
     * @return JsonResponse
     */
    public function unitBuyAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->BUY_SHOP_REQUEST
        );
        return new JsonResponse(
            $this->get('app.manager.shop')->buyShopUnit($data)
        );
    }
}
