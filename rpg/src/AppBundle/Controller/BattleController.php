<?php
/**
 * Class File BattleController
 *
 * PHP version 5.5
 *
 * @package AppBundle\Controller
 */

namespace AppBundle\Controller;

use AppBundle\Manager\Traits\BattleTrait;
use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\QuestInfoFieldsTrait;
use AppBundle\Manager\Traits\RequestResponseTypesTrait;
use AppBundle\Manager\Traits\SkillFieldsTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Tools\Console\Command\ValidateSchemaCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\EventListener\ResizeFormListener;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class BattleController
 * Responsible for battle API
 *
 * @package AppBundle\Controller
 */
class BattleController extends Controller
{
    use ErrorCodesTrait;
    use RequestResponseTypesTrait;
    use BattleTrait;
    use SkillFieldsTrait;
    use UserInfoFieldsTrait;

    /**
     * Initialize the battle
     *
     * @param Request $request incoming request
     *
     * @Route("/battle_init", name="battle_init")
     *
     * @return JsonResponse
     */
    public function battleInitAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->ADVENTURE_BATTLE_INIT
        );

        if ($data === null) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_JSON,
                $this->TYPE => $this->ARENA_BATTLE_INIT
            ));
        }
        $token = $data[$this->TOKEN];
        $stageId = $data[$this->STAGE_ID];
        $personage = $this->get('app.manager.account')->getPersonageByUserToken($token);
        if (!$personage) {
            return new JsonResponse(array(
                $this->STATUS => $this->USER_DOES_NOT_EXIST,
                $this->TYPE => $this->ADVENTURE_BATTLE_INIT
            ));
        }
        $stage = $this->get('app.manager.location')->getStageById($stageId);
        if (!$stage) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_STAGE,
                $this->TYPE => $this->ADVENTURE_BATTLE_INIT
            ));
        }

        $error = $this->get('app.manager.battleresponse')->battleInitError($token, $personage, $stage);
        if ($error) {
            return $error;
        }
        return $this->get('app.manager.battleresponse')->generateMonsterBattle($token, $personage, $stage);
    }

    /**
     * Initialize the arena battle
     *
     * @param Request $request
     *
     * @Route("/arena_init", name="arena_init")
     *
     * @return JsonResponse
     */
    public function arenaInitAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->ARENA_BATTLE_INIT
        );

        if ($data === null) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_JSON,
                $this->TYPE => $this->ARENA_BATTLE_INIT
            ));
        }
        $token1 = $data[$this->PLAYER_1];
        $token2 = $data[$this->PLAYER_2];
        $skills1 = $data[$this->PLAYER_1_SKILLS];
        $skills2 = $data[$this->PLAYER_2_SKILLS];

        $validationRequest1 = new Request(array($this->TOKEN => $token1, $this->SKILLS => $skills1));
        $validationRequest2 = new Request(array($this->TOKEN => $token2, $this->SKILLS => $skills2));

        $validationResponse1 = json_decode($this->arenaPlayerValidationAction($validationRequest1), true);
        if ($validationResponse1[$this->STATUS] != $this->STATUS_OK) {
            return new JsonResponse($validationResponse1);
        }
        $validationResponse2 = json_decode($this->arenaPlayerValidationAction($validationRequest2), true);
        if ($validationResponse2[$this->STATUS] != $this->STATUS_OK) {
            return new JsonResponse($validationResponse2);
        }
        return $this->get('app.manager.battleresponse')->createArenaBattle($token1, $skills1, $token2, $skills2);
    }

    /**
     * Initialize the tournament battle
     *
     * @param Request $request
     *
     * @Route("/tournament_init", name="tournament_init")
     *
     * @return JsonResponse
     */
    public function tournamentInitAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->TOURNAMENT_BATTLE_INIT
        );

        if ($data === null) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_JSON,
                $this->TYPE => $this->TOURNAMENT_BATTLE_INIT
            ));
        }

        $token1 = $data[$this->PLAYER_1];
        $token2 = $data[$this->PLAYER_2];

        if ($this->get('app.manager.battle')->doesBattleExist($token1) ||
            $this->get('app.manager.battle')->doesBattleExist($token2)) {
            return new JsonResponse(
                array(
                    $this->STATUS => $this->BATTLE_ALREADY_EXISTS,
                    $this->TYPE => $this->TOURNAMENT_BATTLE_INIT
                )
            );
        }
        return $this->get('app.manager.battleresponse')->createTournamentBattle($token1, $token2);
    }


    /**
     * Check condition of battle
     *
     * @param Request $request incoming request
     *
     * @Route("/battle_condition", name="battle_condition")
     *
     * @return JsonResponse
     */
    public function battleConditionAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->ADVENTURE_BATTLE_CONDITION
        );
        if ($data === null) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_JSON,
                $this->TYPE => $this->ADVENTURE_BATTLE_CONDITION
            ));
        }
        $token = $data[$this->TOKEN];
        if ($this->get('app.manager.battle')->doesBattleExist($token) &&
            !$this->get('app.manager.battle')->isBattleType($token, $this->MONSTER)) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_BATTLE_TYPE,
                $this->TYPE => $this->ADVENTURE_BATTLE_CONDITION
            ));
        }
        return $this->get('app.manager.battleresponse')->battleConditionResponse($token);
    }

    /**
     * Check condition of pvp battle
     *
     * @param Request $request incoming request
     *
     * @Route("/pvp_battle_condition", name="pvp_battle_condition")
     *
     * @return JsonResponse
     */
    public function pvpConditionAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->PVP_BATTLE_CONDITION
        );
        if ($data === null) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_JSON,
                $this->TYPE => $this->PVP_BATTLE_CONDITION
            ));
        }
        $token = $data[$this->TOKEN];

        if (!$this->get('app.manager.battle')->doesBattleExist($token)) {
            return new JsonResponse(array(
                $this->STATUS => $this->BATTLE_DOES_NOT_EXIST,
                $this->TYPE => $this->PVP_BATTLE_CONDITION
            ));
        }

        $battleType = $this->get('app.manager.battle')->getBattleType($token);
        if ($battleType == $this->MONSTER) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_BATTLE_TYPE,
                $this->TYPE => $this->PVP_BATTLE_CONDITION
            ));
        }
        return $this->get('app.manager.battleresponse')->getPvpConditionResponse($token, $battleType);
    }

    /**
     * Perform using skill by user
     *
     * @param Request $request incoming request
     *
     * @Route("/skill_action", name="skill_action")
     *
     * @return JsonResponse
     */
    public function skillAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->SKILL_ACTION
        );
        if ($data === null) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_JSON,
                $this->TYPE => $this->SKILL_ACTION
            ));
        }
        $token = $data[$this->TOKEN];
        $skillId = $data[$this->SKILL_ID];
        if (!$this->get('app.manager.battle')->doesBattleExist($token)) {
            return new JsonResponse(array(
                $this->STATUS => $this->BATTLE_DOES_NOT_EXIST,
                $this->TYPE => $this->SKILL_ACTION
            ));
        }
        return $this->get('app.manager.battleresponse')->skillActionResponse($token, $skillId);
    }

    /**
     * Send current server time
     *
     * @param Request $request incoming request
     *
     * @Route("/time_request", name="time_request")
     *
     * @return JsonResponse
     */
    public function timeRequestAcrion(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->TIME_REQUEST
        );
        if ($data === null) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_JSON,
                $this->TYPE => $this->TIME_REQUEST
            ));
        }
        $token = $data[$this->TOKEN];
        $userId = $this->get('app.manager.redis')->getUserIdByToken($token);

        if (!$userId) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_TOKEN,
                $this->TYPE => $this->TIME_REQUEST
            ));
        }
        return new JsonResponse(array(
            $this->STATUS => $this->STATUS_OK,
            $this->TIME => time(),
            $this->TYPE => $this->TIME_REQUEST
        ));
    }

    /**
     * Perform monster turn
     *
     * @param Request $request incoming request
     *
     * @Route("/monster_turn", name="monster_turn")
     *
     * @return JsonResponse
     */
    public function monsterTurnAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->TURN_ACTION
        );
        if ($data === null) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_JSON,
                $this->TYPE => $this->TURN_ACTION
            ));
        }
        $token = $data[$this->TOKEN];
        $battleManager = $this->get('app.manager.battle');

        if (!$battleManager->doesBattleExist($token)) {
            return new JsonResponse(array(
                $this->STATUS => $this->BATTLE_DOES_NOT_EXIST,
                $this->TYPE => $this->TURN_ACTION
            ));
        }
        if (!$battleManager->isBattleType($token, $this->MONSTER)) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_BATTLE_TYPE,
                $this->TYPE => $this->TURN_ACTION
            ));
        }
        $monsterToken = $this->get('app.manager.redis')->getOpponentToken($token);
        $skillId = $battleManager->selectMonsterSkill($monsterToken);
        return $this->get('app.manager.battleresponse')->skillActionResponse($monsterToken, $skillId);
    }

    /**
     * Finish the turn
     *
     * @param Request $request incoming request
     *
     * @Route("/end_turn", name="end_turn")
     *
     * @return JsonResponse
     */
    public function endTurnAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->TURN_ACTION
        );

        if ($data === null) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_JSON,
                $this->TYPE => $this->TURN_ACTION
            ));
        }

        $token = $data[$this->TOKEN];
        $this->get('app.manager.battle')->resetSkillsDamage($token);
        $this->get('app.manager.battle')->endTurn($token);

        return $this->battleConditionAction($request);
    }



    /**
     * Finish the battle
     *
     * @param Request $request incoming request
     *
     * @Route("/end_battle", name="end_battle")
     *
     * @return JsonResponse
     */
    public function endBattle(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->END_BATTLE_REQUEST
        );

        if ($data === null) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_JSON,
                $this->TYPE => $this->END_BATTLE_REQUEST
            ));
        }

        $token = $data[$this->TOKEN];
        $this->get('app.manager.battle')->endBattle($token);

        return new JsonResponse(array(
            $this->STATUS => $this->STATUS_OK,
            $this->TYPE => $this->END_BATTLE_REQUEST
        ));
    }

    /**
     * Send battle condition when socket connection was closed
     *
     * @param Request $request
     *
     * @Route("/unexpected_close", name="unexpected_close")
     *
     * @return JsonResponse
     */
    public function unexpectedCloseAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->END_BATTLE_REQUEST
        );

        if ($data === null) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_JSON,
                $this->TYPE => $this->END_BATTLE_REQUEST
            ));
        }
        $token = $data[$this->TOKEN];
        if (!$this->get('app.manager.battle')->doesBattleExist($token)) {
            return new JsonResponse(array(
                $this->STATUS => $this->BATTLE_DOES_NOT_EXIST,
                $this->TYPE => $this->END_BATTLE_REQUEST
            ));
        }
        return $this->get('app.manager.battleresponse')->unexpectedCloseResponse($token);
    }

    /**
     * Paying for arena entrance
     *
     * @param Request $request
     *
     * @Route("/arena_pay", name="arena_pay")
     *
     * @return JsonResponse
     */
    public function arenaPayAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->ARENA_PAY_REQUEST
        );

        if ($data === null) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_JSON,
                $this->TYPE => $this->ARENA_PAY_REQUEST
            ));
        }

        $userId = $this->get('app.manager.jsonvalidator')->getUserIdFromData($data);
        if (!$this->get('app.manager.account')->payForArena($userId)) {
            return new JsonResponse(array(
                $this->STATUS => $this->NOT_ENOUGH_MONEY,
                $this->TYPE => $this->ARENA_PAY_REQUEST
            ));
        }
        return new JsonResponse(array(
            $this->STATUS => $this->STATUS_OK,
            $this->TYPE => $this->ARENA_PAY_REQUEST
        ));
    }

    /**
     * Validate arena player
     *
     * @param Request $request
     *
     * @Route("/arena_player_validation", name="arena_player_validation")
     *
     * @return JsonResponse
     */
    public function arenaPlayerValidationAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->ARENA_VALIDATION_REQUEST
        );

        if ($data === null) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_JSON,
                $this->TYPE => $this->ARENA_BATTLE_INIT
            ));
        }
        $token = $data[$this->TOKEN];
        if ($this->get('app.manager.battle')->doesBattleExist($token)) {
            return new JsonResponse(array(
                $this->STATUS => $this->BATTLE_ALREADY_EXISTS,
                $this->TYPE => $this->ARENA_BATTLE_INIT
            ));
        }
        $personage = $this->get('app.manager.account')->getPersonageByUserToken($token);
        if (!$personage) {
            return new JsonResponse(array(
                $this->STATUS => $this->USER_DOES_NOT_EXIST,
                $this->TYPE => $this->ARENA_BATTLE_INIT
            ));
        }
        if (!$this->get('app.manager.skillapi')->arenaSkillsAreValid($personage, $data[$this->SKILLS])) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_ARENA_SKILLS,
                $this->TYPE => $this->ARENA_BATTLE_INIT
            ));
        }

        return new JsonResponse(array(
            $this->STATUS => $this->STATUS_OK,
            $this->TYPE => $this->ARENA_VALIDATION_REQUEST
        ));
    }
    
    /**
     * Gets info about accessible locations
     *
     * @param Request $request
     *
     * @Route("/locations", name="locations")
     *
     * @return JsonResponse
     */
    public function locationsAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->LOCATIONS_REQUEST
        );

        if ($data === null) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_JSON,
                $this->TYPE => $this->LOCATIONS_REQUEST
            ));
        }
        if (!$this->get('app.manager.jsonvalidator')->getUserIdFromData($data)) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_TOKEN,
                $this->TYPE => $this->LOCATIONS_REQUEST
            ));
        }
        $token = $data[$this->TOKEN];
        $personage = $this->get('app.manager.account')->getPersonageByUserToken($token);
        $locations = $this->get('app.manager.location')->getAccessibleLocations($personage);
        return new JsonResponse(array(
            $this->STATUS => $this->STATUS_OK,
            $this->LOCATIONS => $locations,
            $this->TYPE => $this->LOCATIONS_REQUEST
        ));
    }
    
    /**
     * Gets info about location
     *
     * @param Request $request
     *
     * @Route("/location_info", name="location_info")
     *
     * @return JsonResponse
     */
    public function locationInfoAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->LOCATION_INFO_REQUEST
        );

        if ($data === null) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_JSON,
                $this->TYPE => $this->LOCATION_INFO_REQUEST
            ));
        }
        if (!$this->get('app.manager.jsonvalidator')->getUserIdFromData($data)) {
            return new JsonResponse(array(
                $this->STATUS => $this->WRONG_TOKEN,
                $this->TYPE => $this->LOCATION_INFO_REQUEST
            ));
        }
        $token = $data[$this->TOKEN];
        $locationId = $data[$this->LOCATION_ID];
        if (!$this->get('app.manager.location')->locationExists($locationId)) {
            return new JsonResponse(array(
                $this->STATUS => $this->LOCATION_DOES_NOT_EXISTS,
                $this->TYPE => $this->LOCATION_INFO_REQUEST
            ));
        }
        $personage = $this->get('app.manager.account')->getPersonageByUserToken($token);
        $locationInfo = $this->get('app.manager.location')->getInfoAboutLocation($personage, $locationId);
        return new JsonResponse(array(
            $this->STATUS => $this->STATUS_OK,
            $this->LOCATION_INFO => $locationInfo,
            $this->TYPE => $this->LOCATION_INFO_REQUEST
        ));
    }
}
