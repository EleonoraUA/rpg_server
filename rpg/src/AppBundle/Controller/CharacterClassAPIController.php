<?php
/**
 * Class File CharacterClassAPIController
 *
 * PHP version 5.5
 *
 * @package AppBundle\Controller
 */

namespace AppBundle\Controller;

use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Doctrine\ORM\Query\AST\InstanceOfExpression;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManager;
use AppBundle\Repository;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CharacterClassAPIController
 *
 * @package AppBundle\Controller
 */
class CharacterClassAPIController extends Controller
{
    use ErrorCodesTrait;
    use UserInfoFieldsTrait;

    /**
     * Returns all the classes
     *
     * @Route("/classes", name="get_classes")
     *
     * @return JsonResponse
     */
    public function getClassesAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $classes = $em
            ->getRepository('AppBundle:CharacterClass')
            ->findAll();

        if (!$classes) {
            return new JsonResponse(
                array(
                    $this->STATUS => $this->CLASSES_NOT_FOUND
                )
            );
        }
        foreach ($classes as $class) {
             $response[$this->CLASSES][] = array(
                 $this->CLASS_ID => $class->getId()
             );
        }

        $response[$this->STATUS] = $this->STATUS_OK;
        return new JsonResponse($response);
    }

    /**
     * Returns class by it's id
     *
     * @param $class_id
     *
     * @Route("/class/{class_id}", name="get_class_by_id", requirements={"class_id": "\d+"})
     *
     * @return JsonResponse
     */
    public function getClassByIdAction($class_id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $characterClass = $em
            ->getRepository('AppBundle:CharacterClass')
            ->findOneBy(array($this->ID => $class_id));

        if (!$characterClass) {
            return new JsonResponse(
                array(
                    $this->STATUS => $this->CLASS_NOT_FOUND
                )
            );
        }
        $response[$this->CHAR_CLASS] = array(
            $this->NAME => $characterClass->getName(),
            $this->DESCRIPTION => $characterClass->getDescription()
        );
        $response[$this->STATUS] = $this->STATUS_OK;
        return new JsonResponse($response);
    }
}
