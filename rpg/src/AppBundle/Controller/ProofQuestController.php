<?php
/**
 * Class File ProofQuestController
 *
 * PHP version 5.5
 *
 * @package AppBundle\Controller
 */
namespace AppBundle\Controller;

use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\QuestInfoFieldsTrait;
use AppBundle\Manager\Traits\RequestResponseTypesTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProofQuestController
 *
 * @package AppBundle\Controller
 */
class ProofQuestController extends Controller
{
    use ErrorCodesTrait;
    use RequestResponseTypesTrait;
    use UserInfoFieldsTrait;
    use QuestInfoFieldsTrait;

    /**
     * Action to add proof of quest
     *
     * @param Request $request incoming request
     *
     * @Route("/prove_quest", name="prove_quest")
     *
     * @return JsonResponse
     */
    public function proveQuestAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->get('json'),
            $this->DONE_QUEST_PROVE
        );

        $validatedResponse = $this
            ->get('app.manager.proofquest')
            ->getUserIdFromDataQuestIdUserToQuest($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }

        $file = $request->files->get($this->PROVE_IMAGE_1);
        if ($file === null) {
            return new JsonResponse(
                array(
                    $this->STATUS => $this->SERVER_DID_NOT_GET_FILE
                )
            );
        }

        return new JsonResponse(
            $this
                ->get('app.manager.proofquest')
                ->uploadQuestProofImage(
                    $file,
                    $validatedResponse[$this->USER_ID],
                    $validatedResponse[$this->QUEST_ID],
                    $validatedResponse[$this->USER_TO_QUEST]
                )
        );
    }


    /**
     * Action to get quest to review
     *
     * @param Request $request incoming request
     *
     * @Route("/review_quests", name="review_quests")
     *
     * @return JsonResponse
     */
    public function reviewQuestsAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->REVIEW_QUESTS
        );

        $validatedResponse = $this
            ->get('app.manager.validator')
            ->getUserIdFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }

        return new JsonResponse(
            $this
                ->get('app.manager.proofquest')
                ->getQuestToReview(
                    $validatedResponse[$this->USER_ID]
                )
        );
    }

    /**
     * Action to write result of reviewing quest
     *
     * @param Request $request incoming request
     *
     * @Route("/review_result", name="review_result")
     *
     * @return JsonResponse
     */
    public function reviewResultAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->REVIEW_RESULT
        );

        $validatedResponse = $this
            ->get('app.manager.validator')
            ->getUserIdFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }

        return new JsonResponse(
            $this
                ->get('app.manager.proofquest')
                ->checkQuestReviewResult(
                    $validatedResponse[$this->USER_ID],
                    $data[$this->RESULT]
                )
        );
    }

    /**
     * Action to get reward of done quest
     *
     * @param Request $request incoming request
     *
     * @Route("/get_quest_reward", name="get_quest_reward")
     *
     * @return JsonResponse
     */
    public function getQuestRewardAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->GET_QUEST_REWARD
        );

        return new JsonResponse(
            $this->get('app.manager.proofquest')->sendQuestReward($data)
        );
    }
}
