<?php
/**
 * Class File LoginController
 *
 * @package AppBundle\Controller
 */

namespace AppBundle\Controller;

use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\RequestResponseTypesTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LoginController
 * Responsible for login, signout, quit
 *
 * @package AppBundle\Controller
 */
class LoginController extends BaseController
{
    use ErrorCodesTrait;
    use RequestResponseTypesTrait;
    use UserInfoFieldsTrait;

    /**
     * Login user
     *
     * @param Request $request incoming request
     *
     * @Route("/login", name="login")
     *
     * @return JsonResponse
     */
    public function loginAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->USER_LOGIN
        );

        if ($data === null) {
            return new JsonResponse(
                array(
                    $this->STATUS => $this->WRONG_JSON
                )
            );
        }

        $email = $data[$this->EMAIL];
        $password = $data[$this->PASSWORD];

        $user = $this->container
            ->get('fos_user.user_manager')
            ->findUserBy(array($this->EMAIL => $email));
        if (!$user) {
            return new JsonResponse(
                array(
                    $this->STATUS => $this->USER_DOES_NOT_EXIST
                )
            );
        }
        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        if (!$encoder->isPasswordValid(
            $user->getPassword(),
            $password,
            $user->getSalt()
        )) {
            return new JsonResponse(
                array(
                    $this->STATUS => $this->WRONG_PASSWORD
                )
            );
        }
        $userUtils = $this->container->get('fos_user.util.token_generator');
        $token = $userUtils->generateToken();

        $this->get('app.manager.redis')->addOnlineUser($user->getId(), $token);
        $em = $this->getDoctrine()->getManager();
        $account = $em
            ->getRepository('AppBundle:Account')
            ->findOneBy(
                array(
                    $this->USER => $user
                )
            );
        $personage = $em
            ->getRepository('AppBundle:Personage')
            ->findOneBy(
                array(
                    $this->ACCOUNT => $account
                )
            );

        $skills = $this->get('app.manager.skillapi')->getSelectedSkillsOfPersonage($personage);
        $skillIds = array();
        foreach ($skills as $skill) {
            $skillIds[] = $skill->getId();
        }

        $characters = array(
            array(
                $this->CHAR_NAME => $personage->getName(),
                $this->CHAR_ID =>$personage->getId(),
                $this->CLASS_ID => $personage->getClass()->getId(),
                $this->AVATAR_ID => $account->getAvatarId(),
                $this->SKILLS => $skillIds
            )
        );

        $response = new JsonResponse(array(
            $this->STATUS => $this->STATUS_OK,
            $this->TOKEN => $token,
            $this->USERNAME => $user->getUsername(),
            $this->CHARACTERS => $characters
        ));
        return $response;
    }

    /**
     * Logout user
     *
     * @param Request $request incoming request
     *
     * @Route("/signout", name="signout")
     *
     * @return JsonResponse
     */
    public function signoutAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->USER_LOGOUT
        );

        $validatedResponse = $this->get('app.manager.validator')->getUserIdFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }

        $token = $data[$this->TOKEN];
        $this->get('app.manager.redis')->signoutUser($token);
        return new JsonResponse(
            array(
                $this->STATUS => $this->STATUS_OK
            )
        );
    }

    /**
     * Check if given token has already exist
     *
     * @param Request $request incoming request
     *
     * @Route("/token_exists", name="token_exists")
     *
     * @return JsonResponse
     */
    public function tokenExistsAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->TOKEN_EXIST
        );

        $validatedResponse = $this->get('app.manager.validator')->getUserIdFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }

        return new JsonResponse(
            array(
                $this->STATUS => $this->STATUS_OK
            )
        );
    }

    /**
     * Delete all necessary information when user quit
     *
     * @param Request $request incoming request
     *
     * @Route("/force_quit", name="forcequit")
     *
     * @return JsonResponse
     */
    public function forceQuitAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->USER_LOGOUT
        );

        $validatedResponse = $this->get('app.manager.validator')->getUserIdFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }

        $token = $data[$this->TOKEN];
        $this->get('app.manager.redis')->signoutUser($token);
        $this->get('app.manager.battle')->endBattle($token);

        return new JsonResponse(
            array(
                $this->STATUS => $this->STATUS_OK
            )
        );
    }
}
