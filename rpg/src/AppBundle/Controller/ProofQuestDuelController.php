<?php

namespace AppBundle\Controller;

use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\QuestInfoFieldsTrait;
use AppBundle\Manager\Traits\RequestResponseTypesTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ProofQuestDuelController extends Controller
{
    use ErrorCodesTrait;
    use RequestResponseTypesTrait;
    use UserInfoFieldsTrait;
    use QuestInfoFieldsTrait;

    /**
     * Action to add proof of quest
     *
     * @param Request $request incoming request
     *
     * @Route("/prove_duel", name="prove_duel")
     *
     * @return JsonResponse
     */
    public function proveQuestDuelAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->get('json'),
            $this->DONE_DUEL_PROVE
        );

        $validatedResponse = $this
            ->get('app.manager.proofquestduel')
            ->getQuestDuelToProveFromData($data);

        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }

        $file = $request->files->get($this->PROVE_IMAGE_1);
        if ($file === null) {
            return new JsonResponse(
                array(
                    $this->STATUS => $this->SERVER_DID_NOT_GET_FILE
                )
            );
        }

        return new JsonResponse(
            $this
                ->get('app.manager.proofquestduel')
                ->uploadQuestDuelProofImage(
                    $file,
                    $validatedResponse[$this->USER_ID],
                    $validatedResponse[$this->QUEST_DUEL_REQUEST]
                )
        );
    }

    /**
     * Action to get quest to review
     *
     * @param Request $request incoming request
     *
     * @Route("/review_duel_quests", name="review_duel_quests")
     *
     * @return JsonResponse
     */
    public function reviewQuestsAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->REVIEW_QUESTS
        );

        $validatedResponse = $this
            ->get('app.manager.validator')
            ->getUserIdFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }

        return new JsonResponse(
            $this
                ->get('app.manager.proofquestduel')
                ->getQuestDuelToReview(
                    $validatedResponse[$this->USER_ID]
                )
        );
    }

    /**
     * Action to write result of reviewing duel quest
     *
     * @param Request $request incoming request
     *
     * @Route("/review_duel_result", name="review_duel_result")
     *
     * @return JsonResponse
     */
    public function reviewDuelResultAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->REVIEW_DUEL_RESULT
        );
        $validatedResponse = $this
            ->get('app.manager.validator')
            ->getUserIdFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }

        return new JsonResponse(
            $this
                ->get('app.manager.proofquestduel')
                ->checkQuestDuelReviewResult(
                    $validatedResponse[$this->USER_ID],
                    $data[$this->RESULT]
                )
        );
    }
}
