<?php
/**
 * Class File SkillAPIController
 *
 * PHP version 5.5
 *
 * @package AppBundle\Controller
 */
namespace AppBundle\Controller;

use AppBundle\Manager\Traits\RequestResponseTypesTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class SkillAPIController
 *
 * @package AppBundle\Controller
 */
class SkillAPIController extends Controller
{
    use RequestResponseTypesTrait;

    /**
     * Returns 15 skills for arena battle for certain class
     *
     * @param Request $request
     *
     * @Route("/arena_skills", name="arena_skills")
     *
     * @return JsonResponse
     */
    public function getArenaSkillsAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->ARENA_SKILLS_REQUEST
        );

        return new JsonResponse(
            $this->get('app.manager.skillapi')->getResponseForArenaSkillsAction($data)
        );
    }

    /**
     * Returns all skills of current personage
     *
     * @param Request $request
     *
     * @Route("/skills", name="get_skills")
     *
     * @return JsonResponse
     */
    public function getSkillsAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->SKILLS_REQUEST
        );

        return new JsonResponse(
            $this->get('app.manager.skillapi')->getResponseForSkillsAction($data)
        );
    }

    /**
     * Returns skill by it's ID
     *
     * @param int $skill_id
     *
     * @Route ("/skill/{skill_id}", name="get_skill_by_id", requirements={"skill_id": "\d+"})
     *
     * @return JsonResponse
     */
    public function getSkillByIdAction($skill_id)
    {
        return new JsonResponse(
            $this->get('app.manager.skillapi')->getResponseForSkillByIdAction($skill_id)
        );
    }

    /**
     * Returns selected skills of user
     *
     * @param Request $request
     *
     * @Route ("/select_skills", name="select_skills")
     *
     * @return JsonResponse
     */
    public function selectSkillsAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->SELECT_SKILLS_REQUEST
        );

        return new JsonResponse(
            $this->get('app.manager.skillapi')->getResponseForSelectSkillsAction($data)
        );
    }
}
