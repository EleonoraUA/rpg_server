<?php
/**
 * Class File RegistrationController
 *
 * @package AppBundle\Controller
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Account;
use AppBundle\Entity\CharToSkill;
use AppBundle\Entity\Personage;
use AppBundle\Entity\User;
use AppBundle\Manager\AccountManager;
use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\RequestResponseTypesTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Doctrine\DBAL\Exception\DatabaseObjectExistsException;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Manager\JSONValidatorManager;

/**
 * Class RegistrationController
 * Responsible for registration
 *
 * @package AppBundle\Controller
 */
class RegistrationController extends BaseController
{
    use ErrorCodesTrait;
    use RequestResponseTypesTrait;
    use UserInfoFieldsTrait;

    /**
     * Register new user
     *
     * @param Request $request incoming request
     *
     * @Route("/register", name="register")
     *
     * @return JsonResponse
     */
    public function registerAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->USER_REGISTER
        );

        if ($data === null) {
            return new JsonResponse(
                array(
                    $this->STATUS => $this->WRONG_JSON
                )
            );
        }
        $email = $data[$this->EMAIL];
        $username = $data[$this->USERNAME];
        $password = $data[$this->PASSWORD];

        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $user->setEmail($email);
        $user->setUsername($username);
        $user->setPlainPassword($password);
        $user->setEnabled(true);
        $user->addRole('ROLE_USER');

        $character = $data[$this->CHARACTER];

        $createPersonage = $this->get('app.manager.account')->createPersonageForUserRegister($user, $character);
        if (!$createPersonage[$this->STATUS] == $this->STATUS_OK) {
            return new JsonResponse($createPersonage);
        }
        $validator = $this->get('validator');
        $errors = $validator->validate($user, null, array('registration'));
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                return new JsonResponse(
                    array(
                        $this->STATUS => (int)$error->getMessage()
                    )
                );
            }
        }
        try {
            $userManager->updateUser($user);
        } catch (\Doctrine\DBAL\DBALException $e) {
            $status = $this->get('app.manager.account')->handlingException($email, $password);
            if (!$status == $this->STATUS_OK) {
                return new JsonResponse(
                    array(
                        $this->STATUS => $status
                    )
                );
            }
        }
        $this->get('app.manager.account')->createCharToSkillWithPersonage($createPersonage[$this->PERSONAGE]);
        return new JsonResponse(
            array(
                $this->STATUS => $this->STATUS_OK
            )
        );
    }
}
