<?php
/**
 * Class File TakeQuestController
 *
 * PHP version 5.5
 *
 * @package AppBundle\Controller
 */
namespace AppBundle\Controller;

use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\QuestInfoFieldsTrait;
use AppBundle\Manager\Traits\QuestStatesTrait;
use AppBundle\Manager\Traits\RequestResponseTypesTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TakeQuestController is responsible for changing state of quest by user
 *
 * @package AppBundle\Controller
 */
class TakeQuestController extends Controller
{
    use ErrorCodesTrait;
    use QuestStatesTrait;
    use RequestResponseTypesTrait;
    use UserInfoFieldsTrait;
    use QuestInfoFieldsTrait;

    /**
     * Change quest's state so user can add a proof
     *
     * @param Request $request incoming request
     *
     * @Route("/accept_quest", name="accept_quest")
     *
     * @return JsonResponse
     */
    public function acceptQuestAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->TAKE_QUEST
        );
        $validatedResponse = $this
            ->get('app.manager.takequest')
            ->getUserToQuestFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }
        if (!$validatedResponse[$this->USER_TO_QUEST]) {
            return new JsonResponse(
                $this->get('app.manager.takequest')->changeQuestToState(
                    $this->IN_PROGRESS,
                    $validatedResponse[$this->USER_ID],
                    $validatedResponse[$this->QUEST_ID]
                )
            );
        }
        return new JsonResponse(
            array(
                $this->STATUS => $this->QUEST_IS_NOT_ACCEPTED
            )
        );
    }

    /**
     * Change quest's state so user will not see it
     *
     * @param Request $request incoming request
     *
     * @Route("/skip_quest", name="skip_quest")
     *
     * @return JsonResponse
     */
    public function skipQuestAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->TAKE_QUEST
        );
        $validatedResponse = $this
            ->get('app.manager.takequest')
            ->getUserToQuestFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }
        $userId = $validatedResponse[$this->USER_ID];
        $questId = $validatedResponse[$this->QUEST_ID];
        $userToQuest = $validatedResponse[$this->USER_TO_QUEST];
        if (!$userToQuest) {
            return new JsonResponse(
                $this->get('app.manager.takequest')->changeQuestToState(
                    $this->SKIPPED,
                    $userId,
                    $questId
                )
            );
        }
        if ($userToQuest->getState() === $this->IN_PROGRESS || $userToQuest->getState() === $this->REVIEWED_FALSE) {
            return new JsonResponse(
                $this->get('app.manager.takequest')->changeQuestToState(
                    $this->SKIPPED,
                    $userId,
                    $questId
                )
            );
        }
        return new JsonResponse(
            array(
                $this->STATUS => $this->QUEST_IS_NOT_SKIPPED
            )
        );
    }
}
