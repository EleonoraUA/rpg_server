<?php

namespace AppBundle\Controller;

use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\QuestInfoFieldsTrait;
use AppBundle\Manager\Traits\QuestStatesTrait;
use AppBundle\Manager\Traits\RequestResponseTypesTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RequestQuestDuelController extends Controller
{
    use QuestStatesTrait;
    use ErrorCodesTrait;
    use RequestResponseTypesTrait;
    use UserInfoFieldsTrait;
    use QuestInfoFieldsTrait;

    /**
     * @param Request $request
     *
     * @Route("/duel_send_request", name="duel_send_request")
     *
     * @return JsonResponse
     */
    public function questDuelSendRequestAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->FRIEND_ACTION_REQUEST
        );

        $validatedResponse = $this->get('app.manager.validator')->getUserIdFromData($data);
        if ($validatedResponse[$this->STATUS] !== $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }
        $response = $this
            ->get('app.manager.questduel')
            ->sendQuestDuelRequest($validatedResponse[$this->USER_ID], $data[$this->FRIEND_ID]);
        return new JsonResponse($response);
    }

    /**
     * Change quest's state so user will not see it
     *
     * @param Request $request incoming request
     *
     * @Route("/duel_accept_request", name="accept_duel_request")
     *
     * @return JsonResponse
     */
    public function acceptQuestDuelAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->TAKE_DUEL_QUEST
        );

        $validatedResponse = $this
            ->get('app.manager.questduel')
            ->getDuelToChangeStateFromData($data);
        if ($validatedResponse[$this->STATUS] != $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }

        return new JsonResponse(
            $this->get('app.manager.questduel')->changeQuestDuelRequestToState(
                $validatedResponse[$this->QUEST_DUEL_REQUEST],
                $this->IN_PROGRESS
            )
        );
    }

    /**
     * Change quest's state so user will not see it
     *
     * @param Request $request incoming request
     *
     * @Route("/duel_skip_request", name="skip_duel_request")
     *
     * @return JsonResponse
     */
    public function skipQuestDuelAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->TAKE_DUEL_QUEST
        );

        $validatedResponse = $this
            ->get('app.manager.questduel')
            ->getDuelToChangeStateFromData($data);
        if ($validatedResponse[$this->STATUS] != $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }

        if ($validatedResponse[$this->QUEST_DUEL_REQUEST]->getState() == $this->IN_PROGRESS) {
            return new JsonResponse(
                array($this->STATUS => $this->QUEST_DUEL_CAN_NOT_BE_SKIPPED)
            );
        }
        return new JsonResponse(
            $this->get('app.manager.questduel')->changeQuestDuelRequestToState(
                $validatedResponse[$this->QUEST_DUEL_REQUEST],
                $this->SKIPPED
            )
        );
    }


    /**
     * Action to get reward of done quest
     *
     * @param Request $request incoming request
     *
     * @Route("/get_quest_duel_reward", name="get_quest_reward")
     *
     * @return JsonResponse
     */
    public function getQuestRewardAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->GET_QUEST_REWARD
        );

        return new JsonResponse(
            $this->get('app.manager.proofquest')->sendQuestReward($data)
        );
    }
}
