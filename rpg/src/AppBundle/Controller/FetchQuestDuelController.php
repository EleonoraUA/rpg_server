<?php

namespace AppBundle\Controller;

use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\QuestInfoFieldsTrait;
use AppBundle\Manager\Traits\RequestResponseTypesTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FetchQuestDuelController extends Controller
{
    use ErrorCodesTrait;
    use RequestResponseTypesTrait;
    use UserInfoFieldsTrait;
    use QuestInfoFieldsTrait;

    /**
     * @param Request $request incoming request
     *
     * @Route("/duel_incoming_requests", name="duel_incoming_requests")
     *
     * @return JsonResponse
     */
    public function getDuelIncomingRequestAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->GET_DUEL_QUESTS
        );

        $validatedResponse = $this
            ->get('app.manager.fetchquestduel')
            ->getIncomingDuelQuests($data);
        if ($validatedResponse[$this->STATUS] != $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }
        $duelQuests = $validatedResponse[$this->DUEL_QUESTS];
        return new JsonResponse(
            array(
                $this->STATUS => $this->STATUS_OK,
                $this->DUEL_QUESTS => $duelQuests
            )
        );
    }

    /**
     * @param Request $request incoming request
     *
     * @Route("/duel_in_progress", name="duel_in_progress")
     *
     * @return JsonResponse
     */
    public function getQuestDuelInProgressAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->GET_DUEL_QUESTS
        );

        $validatedResponse = $this
            ->get('app.manager.fetchquestduel')
            ->getInProgressDuelQuests($data);
        if ($validatedResponse[$this->STATUS] != $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }
        $duelQuests = $validatedResponse[$this->DUEL_QUESTS];
        return new JsonResponse(
            array(
                $this->STATUS => $this->STATUS_OK,
                $this->DUEL_QUESTS => $duelQuests
            )
        );
    }

    /**
     * @param Request $request incoming request
     *
     * @Route("/duel_confirmed", name="duel_confirmed")
     *
     * @return JsonResponse
     */
    public function getQuestDuelConfirmedAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->GET_DUEL_QUESTS
        );

        $validatedResponse = $this
            ->get('app.manager.fetchquestduel')
            ->getConfirmedDuelQuests($data);
        if ($validatedResponse[$this->STATUS] != $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }
        $duelQuests = $validatedResponse[$this->DUEL_QUESTS];
        return new JsonResponse(
            array(
                $this->STATUS => $this->STATUS_OK,
                $this->DUEL_QUESTS => $duelQuests
            )
        );
    }

    /**
     * @param Request $request incoming request
     *
     * @Route("/active_duels", name="active_duels")
     *
     * @return JsonResponse
     */
    public function getActiveDuelQuestsAction(Request $request)
    {
        $data = $this->get('app.manager.jsonvalidator')->getValidatedJsonFromRequest(
            $request->getContent(),
            $this->COUNT_ACTIVE_DUELS
        );

        $validatedResponse = $this
            ->get('app.manager.fetchquestduel')
            ->getActiveDuelQuests($data);
        if ($validatedResponse[$this->STATUS] != $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }
        return new JsonResponse(
            array(
                $this->STATUS => $this->STATUS_OK,
                $this->ACTIVE_DUELS => $validatedResponse[$this->ACTIVE_DUELS]
            )
        );
    }
}
