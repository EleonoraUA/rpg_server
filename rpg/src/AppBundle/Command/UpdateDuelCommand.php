<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

const DAYS_TO_PROVE_QUEST = 3;

class UpdateDuelCommand extends Command
{
    protected function configure()
    {
        $this->setName('questduel:state:update');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        global $kernel;
        $questsToUpdate = $kernel->getContainer()->get('app.manager.questduel')->getDuelQuestsWithState(1);

        if ($questsToUpdate) {
            foreach ($questsToUpdate as $duel) {
                $daysLeft = $duel->getDaysLeft();
                if ($daysLeft > 0) {
                    $acceptedDate = $duel->getAcceptedAt();
                    $timeNow = new \DateTime("2016-12-16 16:00:00");
                    //$timeNow = new \DateTime("now");
                    $interval = (int)$timeNow->diff($acceptedDate)->format('%d');
                    $duel->setDaysLeft(DAYS_TO_PROVE_QUEST - $interval);
                } else {
                    $duel->setState(2);
                    $senderProveFilePath = dirname(dirname(dirname(dirname(__FILE__)))).'/web/uploads/duels/'.
                        $duel->getSender()->getId().'/'.$duel->getId().'.jpg';
                    $accepterProveFilePath = dirname(dirname(dirname(dirname(__FILE__)))).'/web/uploads/duels/'.
                        $duel->getAccepter()->getId().'/'.$duel->getId().'.jpg';
                    $reward = $kernel->getContainer()
                        ->get('app.manager.questduel')
                        ->getRewardForDuelQuest($duel->getQuest()->getId());
                    if ((!file_exists($senderProveFilePath)) && file_exists($accepterProveFilePath)) {
                        // if accepter loaded file and sender didnt
                        $kernel->getContainer()
                            ->get('app.manager.account')
                            ->sendMoneyReward($duel->getAccepter()->getId(), $reward);
                    } elseif ((!file_exists($accepterProveFilePath)) && file_exists($senderProveFilePath)) {
                        // if sender loaded file and accepter didnt
                        $kernel->getContainer()
                            ->get('app.manager.account')
                            ->sendMoneyReward($duel->getSender()->getId(), $reward);
                    }
                    if ($reward->getSkillId() == 0) {
                        $duel->setGotReward(true);
                    }
                }
                
                $kernel->getContainer()->get('app.manager.questduel')->updateQuestDuelEntity($duel);
            }
        }
    }
}
