<?php
/**
 * Class File ProofQuestManager
 *
 * PHP version 5.5
 *
 * @package AppBundle\Manager
 */
namespace AppBundle\Manager;

use AppBundle\Entity\CharToSkill;
use AppBundle\Entity\Quest;
use AppBundle\Entity\QuestReward;
use AppBundle\Entity\UserToQuest;
use AppBundle\Entity\QuestReview;
use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\QuestInfoFieldsTrait;
use AppBundle\Manager\Traits\QuestStatesTrait;
use AppBundle\Manager\Traits\SkillFieldsTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;

/**
 * Class ProofQuestManager
 *
 * @package AppBundle\Manager
 */
class ProofQuestManager extends Manager
{
    use QuestStatesTrait;
    use ErrorCodesTrait;
    use QuestInfoFieldsTrait;
    use UserInfoFieldsTrait;
    use SkillFieldsTrait;

    /**
     * @param $file
     * @param $userId
     * @param $questId
     * @param UserToQuest $userToQuest
     *
     * @return array
     */
    public function uploadQuestProofImage($file, $userId, $questId, UserToQuest $userToQuest)
    {
        $extension = $file->getClientOriginalExtension();

        if ('.'.$extension === $this->JPG_EXTENSION) {
            $filename = $questId.$this->JPG_EXTENSION;
        } else {
            return array(
                $this->STATUS => $this->NOT_A_PICTURE
            );
        }

        if (filesize($file) > $this->FILESIZE) {
            return array(
                $this->STATUS => $this->TOO_BIG_PICTURE
            );
        }
        
        $dirname = dirname(dirname(dirname(__DIR__))).'/web'.$this->PROOF_QUEST_IMAGE_URL.$userId;
        $shouldUnlinkFiles = glob($dirname.'/'.$questId.'.*', GLOB_BRACE);

        foreach ($shouldUnlinkFiles as $shouldUnlink) {
            $exist = file_exists($shouldUnlink);

            if ($exist) {
                unlink($shouldUnlink);
            }
        }

        $file->move($dirname, $filename);
        $userToQuest->setState($this->IS_DONE);
        $this->getEm()->persist($userToQuest);
        $this->getEm()->flush();

        return array($this->STATUS => $this->STATUS_OK);
    }

    /**
     *
     *
     * @param $userId
     *
     * @return mixed
     */
    public function getQuestToReview($userId)
    {
        $questOnReview = $this->getEm()
            ->getRepository('AppBundle:QuestReview')
            ->findOneBy(
                array(
                    $this->REVIEWER => $userId,
                    $this->RESULT => null
                )
            );

        $uploadImageURL = "";
        $quest = new Quest();
        if (is_null($questOnReview)) {
            $reviewedQuests = $this
                ->getEm()
                ->getRepository('AppBundle:QuestReview')
                ->findReviewedQuestsByUserId($userId);
            if (empty($reviewedQuests)) {
                $parameters = array(
                    $this->STATE => $this->IS_DONE,
                    $this->REVIEW_COUNT => $this->REVIEW_COUNT_NUMBER,
                    $this->USER => $userId
                );
                $questsToReview = $this
                    ->getEm()
                    ->getRepository('AppBundle:UserToQuest')
                    ->findQuestsToReviewByParameters($parameters);
            } else {
                foreach ($reviewedQuests as $reviewedQuest) {
                    $reviewedQuestId[] = $reviewedQuest->getUserToQuest()->getId();
                }

                $parameters = array(
                    $this->REVIEWED_QUESTS => $reviewedQuestId,
                    $this->STATE => $this->IS_DONE,
                    $this->REVIEW_COUNT => $this->REVIEW_COUNT_NUMBER,
                    $this->USER => $userId
                );
                $questsToReview = $this
                    ->getEm()
                    ->getRepository('AppBundle:UserToQuest')
                    ->findQuestsToReviewByParameters($parameters);
            }

            if (empty($questsToReview)) {
                return array(
                    $this->STATUS => $this->STATUS_OK,
                    $this->QUESTS => array()
                );
            }
        } else {
            $quest = $questOnReview->getUserToQuest()->getQuest();
            $userId = $questOnReview->getUserToQuest()->getUser()->getId();
            $uploadImageURL = $this->PROOF_QUEST_IMAGE_URL.$userId.'/'.$quest->getId().$this->JPG_EXTENSION;
        }

        if (!empty($questsToReview) && isset($questsToReview)) {
            $quest = $questsToReview[0]->getQuest();
            $user = $this->getEm()->getRepository('AppBundle:User')->findOneBy(array($this->ID => $userId));
            $review = new QuestReview();
            $review->setReviewer($user);
            $review->setResult(null);
            $review->setUserToQuest($questsToReview[0]);
            $this->getEm()->persist($review);

            $questsToReview[0]->updateReviewCount();
            $this->getEm()->persist($questsToReview[0]);
            $this->getEm()->flush();

            $userId = $questsToReview[0]->getUser()->getId();
            $uploadImageURL = $this->PROOF_QUEST_IMAGE_URL.$userId.'/'.$quest->getId().$this->JPG_EXTENSION;
        }

        $response[$this->QUESTS][] = array(
            $this->QUEST_ID => $quest->getId(),
            $this->NAME => $quest->getName(),
            $this->DESCRIPTION => $quest->getDescription(),
            $this->STATE => $this->TO_REVIEW,
            $this->PROVE_IMAGE_1 => $uploadImageURL,
            $this->REWARD => array(
                $this->GOLD => $this->GOLD_REVIEW_REWARD,
                $this->CRYSTALS => $this->CRYSTALS_REVIEW_REWARD,
                $this->SKILL_ID => $this->NO_SKILLS_REVIEW_REWARD
            )
        );

        $response[$this->STATUS] = $this->STATUS_OK;
        return $response;
    }

    /**
     * @param $userId
     *
     * @param $result
     *
     * @return array
     */
    public function checkQuestReviewResult($userId, $result)
    {
        global $kernel;
        $questOnReview = $this->getEm()
            ->getRepository('AppBundle:QuestReview')
            ->findOneBy(
                array(
                    $this->REVIEWER => $userId,
                    $this->RESULT => null
                )
            );
        if ($questOnReview) {
            $questOnReview->setResult($result);
            $this->getEm()->persist($questOnReview);
            $this->getEm()->flush();
            $this->checkQuestWasConfirmed($questOnReview->getUserToQuest());
            $kernel->getContainer()
                ->get('app.manager.account')
                ->sendMoneyReward(
                    $userId,
                    new QuestReward($this->GOLD_REVIEW_REWARD, $this->CRYSTALS_REVIEW_REWARD, null)
                );
        } else {
            return array($this->STATUS => $this->NO_QUEST_FOR_VOTING);
        }
        return array($this->STATUS => $this->STATUS_OK);
    }

    /**
     * @param UserToQuest $userToQuest
     *
     * @return bool
     */
    public function checkQuestWasConfirmed(UserToQuest $userToQuest)
    {
        $reviews = $this
            ->getEm()
            ->getRepository('AppBundle:QuestReview')
            ->findReviewResultByUserToQuest($userToQuest->getId());
        $response = false;
        if (count($reviews) == $this->REVIEW_COUNT_NUMBER) {
            $response = true;
            $newState = $this->getNewStateOfConfirmedQuest($reviews);
            if ($newState == $this->REVIEWED_TRUE) {
                global $kernel;
                $kernel->getContainer()
                    ->get('app.manager.account')
                    ->sendMoneyReward($userToQuest->getUser()->getId(), $userToQuest->getQuest()->getReward());
                if ($userToQuest->getQuest()->getReward() == 0) {
                    $userToQuest->setGotReward(true);
                }
            }
            $userToQuest->setState($newState);
            $this->getEm()->persist($userToQuest);
            $this->getEm()->flush();
        }
        return $response;
    }

    /**
     * @param $reviews
     *
     * @return mixed
     */
    private function getNewStateOfConfirmedQuest($reviews)
    {
        $trueCount = 0;
        $falseCount = 0;
        foreach ($reviews as $review) {
            if ($review[$this->RESULT]) {
                $trueCount++;
            } else {
                $falseCount++;
            }
        }
        if ($trueCount > $falseCount) {
            $newState = $this->REVIEWED_TRUE;
        } else {
            $newState = $this->REVIEWED_FALSE;
        }

        return $newState;
    }

    /**
     * Find user's quest that user can prove
     *
     * @access public
     *
     * @param $userId
     * @param $questId
     *
     * @return null|UserToQuest
     */
    public function findUsersQuestToProve($userId, $questId)
    {
        $userToQuest = $this->getEm()
            ->getRepository('AppBundle:UserToQuest')
            ->findOneBy(
                array(
                    $this->USER => $userId,
                    $this->QUEST => $questId
                )
            );
        if ($userToQuest) {
            $state = $userToQuest->getState();
            if ($state === $this->IN_PROGRESS || $state === $this->REVIEWED_FALSE) {
                return $userToQuest;
            }
        }
        return null;
    }

    /**
     * Create response with status and set reward
     *
     * @param $data
     *
     * @return array
     */
    public function sendQuestReward($data)
    {
        global $kernel;
        $validatedResponse = $kernel->getContainer()->get('app.manager.validator')->getUserIdFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return $validatedResponse;
        }

        $userId = $validatedResponse[$this->USER_ID];
        $quest = $kernel->getContainer()->get('app.manager.quest')->findQuestByQuestID($data[$this->QUEST_ID]);
        if (!$quest) {
            return  array(
                $this->STATUS => $this->QUEST_WITH_SUCH_ID_NOT_FOUND
            );
        }

        $userToQuest = $kernel
            ->getContainer()
            ->get('app.manager.takequest')
            ->findUserToQuestByUserIDAndQuestID($userId, $quest->getId());
        if (!$userToQuest) {
            return array(
                $this->STATUS => $this->USER_HAS_NO_SUCH_QUEST
            );
        }

        if ($userToQuest->getReviewCount() < $this->REVIEW_COUNT_NUMBER ||
            $userToQuest->getState() != $this->REVIEWED_TRUE
        ) {
            return array(
                $this->STATUS => $this->QUEST_IS_NOT_REVIEWED
            );
        }

        if ($userToQuest->hasGotReward() == 1) {
            return array(
                $this->STATUS => $this->USER_HAD_ALREADY_GOT_REWARD
            );
        }

        if ($userToQuest->getQuest()->getReward()->getSkillId()) {
            $personage = $kernel->getContainer()->get('app.manager.account')->getPersonageByUserId($userId);
            $count = $this->getEm()
                ->getRepository('AppBundle:CharToSkill')
                ->countCharToSkillByPersonageId($personage->getId());
            if ($personage->getBagSize() <= $count) {
                return array(
                    $this->STATUS => $this->NO_FREE_SLOT_FOR_SKILL
                );
            }
        }
        $kernel->getContainer()
            ->get('app.manager.account')
            ->sendSkillReward($userId, $userToQuest->getQuest()->getReward());
        $userToQuest->setGotReward(true);
        $this->getEm()->persist($userToQuest);
        $this->getEm()->flush();

        return array(
            $this->STATUS => $this->STATUS_OK
        );
    }


    /**
     * Create a response with status, userId, questId, userToQuest
     *
     * @param $data
     *
     * @return array
     */
    public function getUserIdFromDataQuestIdUserToQuest($data)
    {
        global $kernel;
        $validatedResponse = $kernel
            ->getContainer()
            ->get('app.manager.validator')
            ->getUserIdFromDataQuestId($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return $validatedResponse;
        }

        $userId = $validatedResponse[$this->USER_ID];
        $questId = $validatedResponse[$this->QUEST_ID];
        $userToQuest = $this
            ->findUsersQuestToProve($userId, $questId);
        if ($userToQuest === null) {
            return array(
                $this->STATUS => $this->USER_HAS_NO_SUCH_QUEST
            );
        }
        return array(
            $this->STATUS => $this->STATUS_OK,
            $this->USER_ID => $userId,
            $this->QUEST_ID => $questId,
            $this->USER_TO_QUEST => $userToQuest
        );
    }
}
