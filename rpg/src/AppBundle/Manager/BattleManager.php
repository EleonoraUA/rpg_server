<?php
/**
 * Class File BattleManager
 *
 * @package AppBundle\Manager
 */

namespace AppBundle\Manager;

use AppBundle\Manager\Traits\BattleTrait;
use AppBundle\Manager\Traits\EffectsTrait;
use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\RequestResponseTypesTrait;
use AppBundle\Manager\Traits\SkillFieldsTrait;

/**
 * Class BattleManager
 * Manage with battle processes
 *
 * @package AppBundle\Manager
 */
class BattleManager extends Manager
{
    use ErrorCodesTrait;
    use RequestResponseTypesTrait;
    use BattleTrait;
    use SkillFieldsTrait;
    use EffectsTrait;

    /**
     * @return RedisManager|object
     */
    private function getRedisManager()
    {
        global $kernel;
        return $kernel->getContainer()->get('app.manager.redis');
    }

    /**
     * Perform using skill
     *
     * @param string $token user's token
     *
     * @param int $skillId id of the used skill
     *
     * @access public
     */
    public function performUsingSkill($token, $skillId)
    {
        if ($this->opponentIsStunned($token)) {
            $this->getRedisManager()->unstunOpponent($token);
        }
        $atk = $this->getRedisManager()->getAttack($token);
        $skill = $this->getSkillById($token, $skillId);
        $damage = $this->countDamageFromUsingSkill($skill, $atk);
        if ($this->attackIsIncreased($token)) {
            $damage = $damage * $this->EFFECT_MULTIPLIER;
        }
        if ($this->attackIsBroken($token)) {
            $damage = $damage / $this->EFFECT_MULTIPLIER;
        }
        $opponent = $this->getRedisManager()->getOpponentToken($token);
        if ($this->defenceIsIncreased($opponent)) {
            $damage = $damage / $this->EFFECT_MULTIPLIER;
        }
        if ($this->defenceIsBroken($opponent)) {
            $damage = $damage * $this->EFFECT_MULTIPLIER;
        }
        $damage = ceil($damage);
        $this->getRedisManager()->setSkillsDamage($token, $skillId, $damage);
        if ($damage < 0) {
            $this->performHealing($token, $damage);
        } else {
            $this->performDamageDealing($token, $damage);
        }
        if ($skill[$this->EFFECTS]) {
            foreach ($skill[$this->EFFECTS] as $effect) {
                $this->getRedisManager()->addEffect($token, $effect);
            }
        }
        $this->getRedisManager()->updateSkillCooldown($token, $skillId);
    }

    /**
     * Checks if attack power is increased
     *
     * @param string $token
     *
     * @return bool
     */
    private function attackIsIncreased($token)
    {
        $effects = $this->getRedisManager()->getEffects($token);
        if (!$effects) {
            return false;
        }
        foreach ($effects as $effect) {
            if ($effect[$this->EFFECT_ID] == $this->INCREASE_ATTACK_EFFECT) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if attack power is decreased
     *
     * @param string $token
     *
     * @return bool
     */
    private function attackIsBroken($token)
    {
        $effects = $this->getRedisManager()->getEffects($token);
        if (!$effects) {
            return false;
        }
        foreach ($effects as $effect) {
            if ($effect[$this->EFFECT_ID] == $this->ATTACK_BREAK_EFFECT) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if defence is increased
     *
     * @param string $token
     *
     * @return bool
     */
    private function defenceIsIncreased($token)
    {
        $effects = $this->getRedisManager()->getEffects($token);
        if (!$effects) {
            return false;
        }
        foreach ($effects as $effect) {
            if ($effect[$this->EFFECT_ID] == $this->INCREASE_DEFENCE_EFFECT) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if defence is decreased
     *
     * @param $token
     *
     * @return bool
     */
    private function defenceIsBroken($token)
    {
        $effects = $this->getRedisManager()->getEffects($token);
        if (!$effects) {
            return false;
        }
        foreach ($effects as $effect) {
            if ($effect[$this->EFFECT_ID] == $this->DEFENCE_BREAK_EFFECT) {
                return true;
            }
        }
        return false;
    }

    /**
     * Count amount of damage made by using skill by personage with given attack
     *
     * @param array $skill information about used skill
     *
     * @param int $atk amount of personage's attack
     *
     * @access private
     *
     * @return int
     */
    private function countDamageFromUsingSkill($skill, $atk)
    {
        $rawDamage = $skill[$this->MULTIPLIER] * $atk;
        $percent = rand(0, 10)/100;
        $delta = $rawDamage * $percent;
        if (rand(0, 1)) {
            $damage = $rawDamage + $delta;
        } else {
            $damage = $rawDamage - $delta;
        }
        return $damage;
    }

    /**
     * Healing personage and opponent
     *
     * @param string $token user's token
     *
     * @param int $hpAmount amount of healed hp
     *
     * @access private
     */
    private function performHealing($token, $hpAmount)
    {
        $maxHp = $this->getRedisManager()->getMaxHp($token);
        $myHp = $this->getRedisManager()->getHp($token);
        $myHp = ($myHp - $hpAmount > $maxHp) ? $maxHp : ($myHp - $hpAmount);
        $this->getRedisManager()->setHp($token, $myHp);
    }

    /**
     * Dealing damage to the opponent
     *
     * @param string $token user's token
     *
     * @param int $damage amount of dealt damage
     *
     * @access private
     */
    private function performDamageDealing($token, $damage)
    {
        $opponentMaxHp = $this->getRedisManager()->getOpponentHp($token);
        $opponentMaxHp = ($opponentMaxHp - $damage < 0) ? 0 : ($opponentMaxHp - $damage);
        $this->getRedisManager()->setOpponentHp($token, $opponentMaxHp);
    }

    /**
     * Check if someone won
     *
     * @param string $token user's token
     *
     * @access public
     *
     * @return bool
     */
    public function didWin($token)
    {
        if ($this->getRedisManager()->getOpponentHp($token) == 0) {
            return true;
        }
        return false;
    }

    /**
     * Get skill information by id
     *
     * @param string $token user's token
     *
     * @param int $skillId id of used skill
     *
     * @access private
     *
     * @return null|$skill
     */
    public function getSkillById($token, $skillId)
    {
        $skills = $this->getRedisManager()->getSkills($token);
        foreach ($skills as $skill) {
            if ($skill[$this->SKILL_ID] === $skillId &&
                $skill[$this->CURRENT_CD] === 0) {
                return $skill;
            }
        }
        return null;
    }

    /**
     * Select which skill monster use
     *
     * @param string $token user's token
     *
     * @access public
     *
     * @return null|int
     */
    public function selectMonsterSkill($token)
    {
        $skills = $this->getRedisManager()->getSkills($token);

        usort($skills, function ($skill1, $skill2) {
            if ($skill1[$this->MULTIPLIER] === $skill2[$this->MULTIPLIER]) {
                return 0;
            } else {
                return $skill1[$this->MULTIPLIER] > $skill2[$this->MULTIPLIER] ? -1 : 1;
            }
        });
        
        if (rand(0, 10) < 4) {
            $skills = array_reverse($skills);
        }
        foreach ($skills as $skill) {
            if ($skill[$this->CURRENT_CD] === 0) {
                return $skill[$this->SKILL_ID];
            }
        }
        return null;
    }

    /**
     * Checks if opponent is stunned
     *
     * @param string $token
     *
     * @return bool
     */
    public function opponentIsStunned($token)
    {
        $opponent = $this->getRedisManager()->getOpponentToken($token);
        $effects = $this->getRedisManager()->getEffects($opponent);
        if (!$effects) {
            return false;
        }
        foreach ($effects as $effect) {
            if ($effect[$this->EFFECT_ID] == $this->STUN_EFFECT) {
                return true;
            }
        }
        return false;
    }

    /**
     * End turn of user with given token
     *
     * @param string $token user's token
     *
     * @access public
     */
    public function endTurn($token)
    {
        $redisManager = $this->getRedisManager();
        $redisManager->setTurnTime($token);
        if (!$this->opponentIsStunned($token)) {
            $redisManager->setCurrentTurn($token);
        }
        $redisManager->updateOpponentCooldown($token);

        $opponent = $redisManager->getOpponentToken($token);
        $opponentEffects = $redisManager->getEffects($opponent);
        if ($opponentEffects) {
            foreach ($opponentEffects as $effect) {
                if ($effect[$this->EFFECT_ID] == $this->CONTINUOUS_DAMAGE_EFFECT) {
                    $maxHp = $redisManager->getMaxHp($opponent);
                    $damage = ceil($maxHp * $this->CONTINUOUS_DAMAGE_MULTIPLIER);
                    $this->performDamageDealing($token, $damage);
                }
            }
            foreach ($opponentEffects as $effect) {
                if ($redisManager->getHp($opponent) > 0 && $effect[$this->EFFECT_ID] == $this->HEALING_EFFECT) {
                    $maxHp = $redisManager->getMaxHp($opponent);
                    $damage = ceil($maxHp * $this->HEALING_MULTIPLIER);
                    $this->performHealing($opponent, -$damage);
                }
            }
        }
        $redisManager->updateEffectsDuration($token);
        $redisManager->updateTickEffectsDuration($opponent);
        if ($redisManager->isProtected($token)) {
            $redisManager->deleteDebuffs($token);
        }
    }

    /**
     * Check if battle exists
     *
     * @param string $token user's token
     *
     * @return bool
     */
    public function doesBattleExist($token)
    {
        return $this->getRedisManager()->getOpponentToken($token) ? true : false;
    }

    /**
     * Check if battle is over
     *
     * @param string $token user's token
     *
     * @return bool
     */
    public function battleIsOver($token)
    {
        $isOver = false;
        if (!$this->getRedisManager()->getHp($token) || !$this->getRedisManager()->getOpponentHp($token)) {
            $isOver = true;
        }
        return $isOver;
    }

    /**
     * Delete battle
     *
     * @param string $token user's token
     */
    public function endBattle($token)
    {
        $redisManager = $this->getRedisManager();
        $opponentToken = $redisManager->getOpponentToken($token);
        $redisManager->deleteBattleInfo($token);
        $redisManager->deleteBattleInfo($opponentToken);
    }

    /**
     * Set skills damage to zero
     *
     * @param string $token user's token
     */
    public function resetSkillsDamage($token)
    {
        $this->getRedisManager()->setSkillsDamage($token);
    }

    /**
     * Check if opponent is monster or not
     *
     * @param string $token user's token
     *
     * @param string $battleType
     *
     * @return bool
     */
    public function isBattleType($token, $battleType)
    {
        $type = $this->getRedisManager()->getBattleType($token);
        return $type == $battleType ? true : false;
    }
    
    /**
     * Gets type of the battle
     *
     * @param string $token
     *
     * @return string
     */
    public function getBattleType($token)
    {
        return $this->getRedisManager()->getBattleType($token);
    }
}
