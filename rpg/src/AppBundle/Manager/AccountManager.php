<?php
/**
 * Class File AccountManager
 *
 * @package AppBundle\Manager
 */

namespace AppBundle\Manager;

use AppBundle\Entity\CharacterClass;
use AppBundle\Entity\ClearingStage;
use AppBundle\Entity\Personage;
use AppBundle\Entity\Account;
use AppBundle\Entity\QuestReward;
use AppBundle\Entity\Skill;
use AppBundle\Entity\Tournament;
use AppBundle\Entity\User;
use AppBundle\Entity\CharToSkill;
use AppBundle\Manager\Traits\BattleTrait;
use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;

/**
 * Class AccountManager
 * Manage account operations
 *
 * @package AppBundle\Manager
 */
class AccountManager extends Manager
{
    use ErrorCodesTrait;
    use BattleTrait;
    use UserInfoFieldsTrait;

    /**
     * Send reward to user after defeating monster
     *
     * @param string $token user's token
     *
     * @param array $reward information about reward
     *
     * @access public
     */
    public function sendReward($token, $reward)
    {
        global $kernel;
        $userId = $kernel->getContainer()->get('app.manager.redis')->getUserIdByToken($token);
        if ($userId) {
            $account = $this->getEm()->getRepository('AppBundle:User')->find($userId)->getAccount();
            if (isset($reward[$this->GOLD]) && is_int($reward[$this->GOLD])) {
                $account->setGold($account->getGold() + $reward[$this->GOLD]);
            }
            if (isset($reward[$this->CRYSTALS]) && is_int($reward[$this->CRYSTALS])) {
                $account->setCrystals($account->getCrystals() + $reward[$this->CRYSTALS]);
            }
            if (isset($reward[$this->EXP]) && is_int($reward[$this->EXP])) {
                $this->setExperienceToPersonage($token, $reward[$this->EXP]);
            }
            $this->getEm()->flush();
        }
    }
    
    public function sendSkillReward($userId, $skillId)
    {
        global $kernel;
        $account = $this->getEm()->getRepository('AppBundle:Account')->findOneBy(array($this->USER => $userId));
        $personage = $this
            ->getEm()
            ->getRepository('AppBundle:Personage')
            ->findOneBy(array($this->ACCOUNT => $account->getId()));

        $skill = $this->getEm()->getRepository('AppBundle:Skill')->findOneBy(array($this->ID => $skillId));
        if (!$kernel->getContainer()->get('app.manager.skillapi')->setSkillToPersonage($personage, $skill)) {
            $personage->setAtk($personage->getAtk() + 1);
            $this->getEm()->persist($personage);
            $this->getEm()->flush();
        }
    }

    public function sendMoneyReward($userId, QuestReward $reward)
    {
        $account = $this->getEm()->getRepository('AppBundle:Account')->findOneBy(array($this->USER => $userId));
        $account->setGold($account->getGold() + $reward->getGold());
        $account->setCrystals($account->getCrystals() + $reward->getCrystals());
        $this->getEm()->persist($account);
        $this->getEm()->flush();
    }


    /**
     * Get personage of user with given id
     *
     * @param int $userId id of the user
     *
     * @access public
     *
     * @return Personage
     */
    public function getPersonageByUserId($userId)
    {
        $em = $this->getEm();
        $account = $this->getAccountByUserId($userId);
        $personage = $em->getRepository('AppBundle:Personage')->findOneBy(array($this->ACCOUNT => $account));
        return $personage;
    }

    /**
     * Get account of user with given id
     *
     * @param int $userId id of the user
     *
     * @access public
     *
     * @return Account
     */
    public function getAccountByUserId($userId)
    {
        $em = $this->getEm();
        $user = $em->getRepository('AppBundle:User')->find($userId);
        $account = $em->getRepository('AppBundle:Account')->findOneBy(array($this->USER => $user));
        return $account;
    }

    /**
     * Get personage of user with given token
     *
     * @param string $token user's token
     *
     * @access public
     *
     * @return Personage
     */
    public function getPersonageByUserToken($token)
    {
        global $kernel;
        $userId = $kernel->getContainer()->get('app.manager.redis')->getUserIdByToken($token);
        return $this->getPersonageByUserId($userId);
    }

    /**
     * Create CharToSkill and first personage for user
     *
     * @param Personage $personage
     *
     * @access public
     */
    public function createCharToSkillWithPersonage(Personage $personage)
    {
        $em = $this->getEm();
        $class = $personage->getClass();
        $stats = $em
            ->getRepository('AppBundle:StatsGradation')
            ->findOneBy(array($this->CHAR_CLASS => $class, $this->LVL => 1));
        $personage->setAtk($stats->getAtk());
        $personage->setHp($stats->getHp());
        $em->persist($personage);
        $autoAttack = $this->getAutoattackForClass($class);
        $charToSkill = new CharToSkill($personage, $autoAttack, true);
        $em->persist($charToSkill);
        $stage = $em->getRepository('AppBundle:Stage')->find(1);
        $clearingStage = new ClearingStage($stage, $personage, false);
        $em->persist($clearingStage);
        $em->flush();
    }


    /**
     * Create personage for user register
     *
     * @param User $user
     *
     * @param $character
     *
     * @return array
     */
    public function createPersonageForUserRegister(User $user, $character)
    {
        global $kernel;
        $em = $this->getEm();
        $account = new Account($user);
        $avatarId = $character[$this->AVATAR_ID];
        if ($avatarId < $this->MIN_AVATAR_ID || $avatarId > $this->MAX_AVATAR_ID) {
            return array(
                $this->STATUS => $this->WRONG_AVATAR_ID
            );
        }
        $account->setAvatarId($avatarId);
        $em->persist($account);
        $user->setAccount($account);
        if (!$this->classWithIdExists($character[$this->CLASS_ID])) {
            return array(
                $this->STATUS => $this->WRONG_CLASS
            );
        }
        $class = $em
            ->getRepository('AppBundle:CharacterClass')
            ->findOneBy(array($this->ID => $character[$this->CLASS_ID]));
        $personage = new Personage($account, $character[$this->CHAR_NAME], $class);

        $validator = $kernel->getContainer()->get('validator');
        $allErrors = $validator->validate($personage, null, array('registration'));
        if (count($allErrors) > 0) {
            foreach ($allErrors as $error) {
                return array(
                    $this->STATUS => (int)$error->getMessage()
                );
            }
        }
        return array(
            $this->STATUS => $this->STATUS_OK,
            $this->PERSONAGE => $personage
        );
    }

    /**
     * Check if class with given id exists
     *
     * @param int $classId id of character's class
     *
     * @access private
     *
     * @return bool
     */
    private function classWithIdExists($classId)
    {
        $class = $this
            ->getEm()
            ->getRepository('AppBundle:CharacterClass')
            ->find($classId);
        if ($class) {
            return true;
        }
        return false;
    }

    public function handlingException($email, $username)
    {
        $repository = $this
            ->getEm()
            ->getRepository('AppBundle:User');
        $existingUser = $repository
            ->findOneBy(array($this->EMAIL => $email));
        if ($existingUser) {
            return $this->EMAIL_IS_ALREADY_TAKEN;
        }
        $existingUser = $repository->findOneBy(
            array(
                $this->USERNAME => $username
            )
        );
        if ($existingUser) {
            return $this->USERNAME_IS_ALREADY_TAKEN;
        }
        return $this->STATUS_OK;
    }

    /**
     * Get autoattack
     *
     * @param CharacterClass $class
     *
     * @return Skill
     */
    private function getAutoattackForClass($class)
    {
        $classSkills = $class->getSkills();
        foreach ($classSkills as $classSkill) {
            if ($classSkill->getRequiredLvl() == 1 && $classSkill->getCd() == 1) {
                return $classSkill;
            }
        }
        return null;
    }

    /**
     * Generate exp reward
     *
     * @param int $stageId id of the monster
     *
     * @return int
     */
    public function getExpReward($stageId)
    {
        $stage = $this->getEm()->getRepository('AppBundle:Stage')->find($stageId);
        if (!$stageId) {
            return 0;
        }
        $exp = rand($stage->getMinExp(), $stage->getMaxExp());
        return $exp;
    }

    /**
     * Set experience to personage
     *
     * @param string $token user's token
     *
     * @param int $exp
     */
    public function setExperienceToPersonage($token, $exp)
    {
        $personage = $this->getPersonageByUserToken($token);
        $cur_lvl = $personage->getLvl();
        $max_exp = $this
            ->getEm()
            ->getRepository('AppBundle:Experience')
            ->findOneBy(array($this->LVL => $cur_lvl))->getMaxExp();
        $cur_exp = $personage->getExp();
        if ($cur_exp + $exp > $max_exp) {
            if ($cur_lvl != $this->MAX_LVL) {
                $transfer_exp = $exp - ($max_exp - $cur_exp);
                $this->levelUp($personage);
                $personage->setExp($transfer_exp);
            } else {
                $personage->setExp($max_exp);
            }
        } else {
            $personage->setExp($cur_exp + $exp);
        }
        $this->getEm()->flush();
    }

    /**
     * Increase personage's level
     *
     * @param Personage $personage
     */
    public function levelUp($personage)
    {
        $new_lvl = $personage->getLvl() + 1;
        $personage->setLvl($new_lvl);
        $stats = $this
            ->getEm()
            ->getRepository('AppBundle:StatsGradation')
            ->findOneBy(array($this->LVL => $new_lvl, $this->CHAR_CLASS => $personage->getClass()));
        $personage->setAtk($stats->getAtk());
        $personage->setHp($stats->getHp());
        if ($personage->getLvl() == $this->MAX_LVL) {
            $personage->setActiveSkillsBagSize($personage->getActiveSkillsBagSize() + 1);
        }
        $this->getEm()->flush();
    }

    /**
     * Returns user by userId
     *
     * @param int $userId
     *
     * @access public
     *
     * @return null|object
     */
    public function getUserByUserId($userId)
    {
        $user = $this
            ->getEm()
            ->getRepository('AppBundle:User')
            ->findOneBy(array($this->ID => $userId));
        return $user;
    }

    /**
     * Get information about personage's profile
     *
     * @param Personage $personage
     *
     * @return array
     */
    public function getPersonageProfile($personage)
    {
        $profile[$this->CURRENT_LVL] = $personage->getLvl();
        $profile[$this->CURRENT_EXP] = $personage->getExp();
        $profile[$this->PERSONAGE_BAG_SIZE] = $personage->getBagSize();
        $profile[$this->PERSONAGE_ACTIVE_SKILLS_BAG_SIZE] = $personage->getActiveSkillsBagSize();
        $profile[$this->MAX_EXP] = $this->getEm()
            ->getRepository('AppBundle:Experience')
            ->findOneBy(array($this->LVL => $personage->getLvl()))
            ->getMaxExp();
        $profile[$this->CHAR_ATK] = $personage->getAtk();
        $profile[$this->CHAR_HP] = $personage->getHp();
        return $profile;
    }

    /**
     * Set experience to personage
     *
     * @param int $userId user's id
     *
     * @return bool
     */
    public function payForArena($userId)
    {
        $user = $this->getUserByUserId($userId);
        $account = $user->getAccount();
        if ($account->getCrystals() < $this->ARENA_PRICE) {
            return false;
        }
        $account->setCrystals($account->getCrystals() - $this->ARENA_PRICE);
        $this->getEm()->flush();
        return true;
    }

    /**
     * Set gold to user
     *
     * @param int $userId
     *
     * @param int $gold
     */
    public function setGoldToUser($userId, $gold)
    {
        $account = $this->getEm()->getRepository('AppBundle:User')->find($userId)->getAccount();
        $account->setGold($account->getGold() + $gold);
        $this->getEm()->flush();
    }

    /**
     * Add bag slots to personage
     *
     * @param Personage $personage
     *
     * @param int $count
     */
    public function setBagSlotsToPersonage($personage, $count)
    {
        $personage->setBagSize($personage->getBagSize() + $count);
        $this->getEm()->flush();
    }

    /**
     * Add active slot for skill
     *
     * @param Personage $personage
     *
     * @param int $count
     */
    public function setActiveSlotToPersonage($personage, $count)
    {
        $personage->setActiveSkillsBagSize($personage->getActiveSkillsBagSize() + $count);
        $this->getEm()->flush();
    }

    /**
     * Expend gold
     *
     * @param Personage $personage
     *
     * @param int $gold
     *
     * @return bool
     */
    public function expendGold($personage, $gold)
    {
        $account = $personage->getAccount();
        if ($account->getGold() < $gold) {
            return false;
        }
        $account->setGold($account->getGold() - $gold);
        $this->getEm()->flush();
        return true;
    }

    /**
     * Expend crystals
     *
     * @param Personage $personage
     *
     * @param int $crystals
     *
     * @return bool
     */
    public function expendCrystals($personage, $crystals)
    {
        $account = $personage->getAccount();
        if ($account->getCrystals() < $crystals) {
            return false;
        }
        $account->setCrystals($account->getCrystals() - $crystals);
        $this->getEm()->flush();
        return true;
    }

    public function personageWonTournamentBattle($token)
    {
        $personage = $this->getPersonageByUserToken($token);
        $tournamentInfo = $this
            ->getEm()
            ->getRepository('AppBundle:Tournament')
            ->findOneBy(array($this->PERSONAGE => $personage));
        if ($tournamentInfo) {
            $tournamentInfo->setWinCount($tournamentInfo->getWinCount() + 1);
            $tournamentInfo->setTotalAmount($tournamentInfo->getTotalAmount() + 1);
            $rang = $this
                ->getEm()
                ->getRepository('AppBundle:Rang')
                ->findRangWithWinCount($tournamentInfo->getWinCount());
            if ($rang->getLvl() < $tournamentInfo->getBestRang()->getLvl()) {
                $tournamentInfo->setBestRang($rang);
            }
        } else {
            $rang = $this->getEm()->getRepository('AppBundle:Rang')->findRang();
            $tournamentInfo = new Tournament();
            $tournamentInfo->setPersonage($personage);
            $tournamentInfo->setTotalAmount(1);
            $tournamentInfo->setWinCount(1);
            $tournamentInfo->setBestRang($rang);
            $this->getEm()->persist($tournamentInfo);
        }
        $this->getEm()->flush();
    }

    public function personageLoseTournamentBattle($token)
    {
        $personage = $this->getPersonageByUserToken($token);
        $tournamentInfo = $this
            ->getEm()
            ->getRepository('AppBundle:Tournament')
            ->findOneBy(array($this->PERSONAGE => $personage));
        if ($tournamentInfo) {
            if ($tournamentInfo->getWinCount() != 0) {
                $tournamentInfo->setWinCount($tournamentInfo->getWinCount() - 1);
            }
            $tournamentInfo->setTotalAmount($tournamentInfo->getTotalAmount() + 1);
        } else {
            $rang = $this->getEm()->getRepository('AppBundle:Rang')->findRang();
            $tournamentInfo = new Tournament();
            $tournamentInfo->setPersonage($personage);
            $tournamentInfo->setWinCount(0);
            $tournamentInfo->setTotalAmount(1);
            $tournamentInfo->setBestRang($rang);
            $this->getEm()->persist($tournamentInfo);
        }
        $this->getEm()->flush();
    }

    public function getAccountFromData($data)
    {
        global $kernel;
        $validatedResponse = $kernel
            ->getContainer()
            ->get('app.manager.validator')
            ->getUserIdFromData($data);

        if (!isset($validatedResponse[$this->USER_ID])) {
            return $validatedResponse;
        }
        $account = $this->getEm()
            ->getRepository('AppBundle:Account')
            ->findOneBy(array($this->USER => $validatedResponse[$this->USER_ID]));

        if (!$account) {
            return array(
                $this->STATUS => $this->USER_DOES_NOT_EXIST
            );
        }
        return array(
            $this->STATUS => $this->STATUS_OK,
            $this->ACCOUNT => $account
        );
    }

    public function changeAvatar($data)
    {
        $validatedResponse = $this->getAccountFromData($data);
        if (!isset($validatedResponse[$this->ACCOUNT])) {
            return $validatedResponse;
        }
        $avatarId = $data[$this->AVATAR_ID];
        if ($avatarId < $this->MIN_AVATAR_ID || $avatarId > $this->MAX_AVATAR_ID) {
            return array($this->STATUS => $this->WRONG_AVATAR_ID);
        }
        $validatedResponse[$this->ACCOUNT]->setAvatarId($avatarId);
        $this->getEm()->persist($validatedResponse[$this->ACCOUNT]);
        $this->getEm()->flush();

        return array($this->STATUS => $this->STATUS_OK);
    }
}
