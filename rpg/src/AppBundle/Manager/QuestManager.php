<?php
/**
 * Class File QuestManager
 *
 * PHP version 5.5
 *
 * @package AppBundle\Manager
 */
namespace AppBundle\Manager;

use AppBundle\Entity\UserToQuest;
use AppBundle\Manager\Traits\QuestInfoFieldsTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Doctrine\ORM\EntityManager;

/**
 * Class QuestManager is used to store logic for quests
 *
 * @package AppBundle\Manager
 */
class QuestManager extends Manager
{
    use UserInfoFieldsTrait;
    use QuestInfoFieldsTrait;

    /**
     * Finding certain quest by ID
     *
     * @access public
     *
     * @param $questId
     *
     * @return null|object
     */
    public function findQuestByQuestID($questId)
    {
        return $this->getEm()->getRepository('AppBundle:Quest')->find($questId);
    }

    /**
     * Finding all quests
     *
     * @access public
     *
     * @return array
     */
    public function findAllQuests()
    {
        return $this->getEm()->getRepository('AppBundle:Quest')->findAll();
    }

    /**
     * Finding all user's quests in certain state
     *
     * @param $userId
     * @param $state
     *
     * @return array
     */
    public function findUsersQuestsWithState($userId, $state)
    {
        return $this->getEm()
            ->getRepository('AppBundle:UserToQuest')
            ->findBy(
                array(
                    $this->USER => $userId,
                    $this->STATE => $state
                )
            );
    }

    /**
     * Creating new user's quest with params
     *
     * @access public
     *
     * @param EntityManager $em
     * @param $userId
     * @param $questId
     *
     * @return UserToQuest
     */
    public function createNewUserToQuest(EntityManager $em, $userId, $questId)
    {
        $user = $em
            ->getRepository('AppBundle:User')
            ->findOneBy(
                array(
                    $this->ID => $userId
                )
            );
        $quest = $this->findQuestByQuestID($questId);
        $userToQuest = new UserToQuest();
        $userToQuest->setQuest($quest);
        $userToQuest->setUser($user);
        $userToQuest->setReviewCount(0);
        $userToQuest->setGotReward(0);
        return $userToQuest;
    }

    /**
     * Is class of personage and quest the same
     *
     * @access public
     *
     * @param $userId
     * @param $questId
     *
     * @return bool
     */
    public function isTheSameClassForQuestAndPersonage($userId, $questId)
    {
        global $kernel;
        $personage = $kernel->getContainer()->get('app.manager.account')->getPersonageByUserId($userId);
        $quests = $personage->getClass()->getQuests();
        $result = false;
        if ($quests) {
            foreach ($quests as $quest) {
                if ($quest->getId() == $questId) {
                    $result = true;
                }
            }
        }
        return $result;
    }

    /**
     * Is lvl of personage and quest the same
     *
     * @access public
     *
     * @param $userId
     * @param $questId
     *
     * @return bool
     */
    public function isTheSameLvlForQuestAndPersonage($userId, $questId)
    {
        global $kernel;
        $personage = $kernel
            ->getContainer()
            ->get('app.manager.account')
            ->getPersonageByUserId($userId);
        $quest = $this->getEm()->getRepository('AppBundle:Quest')->findOneBy(array($this->ID => $questId));
        $lvl = $personage->getLvl();
        $result = false;
        if ($lvl && $lvl >= $quest->getRequiredLvl()) {
            $result = true;
        }
        return $result;
    }
}
