<?php
/**
 * Class File TakeQuestManager
 *
 * PHP version 5.5
 *
 * @package AppBundle\Manager
 */
namespace AppBundle\Manager;

use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\QuestInfoFieldsTrait;
use AppBundle\Manager\Traits\QuestStatesTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Doctrine\DBAL\DBALException;

/**
 * Class TakeQuestManager is used to store logic for taking user's quests
 *
 * @package AppBundle\Manager
 */
class TakeQuestManager extends Manager
{
    use QuestStatesTrait;
    use ErrorCodesTrait;
    use UserInfoFieldsTrait;
    use QuestInfoFieldsTrait;
    
    /**
     * Finding UserToQuest by user's ID and quest's ID
     *
     * @access public
     *
     * @param $userId
     * @param $questId
     *
     * @return null|object
     */
    public function findUserToQuestByUserIDAndQuestID($userId, $questId)
    {
        return $this->getEm()
            ->getRepository('AppBundle:UserToQuest')
            ->findOneBy(
                array(
                    $this->USER => $userId,
                    $this->QUEST => $questId
                )
            );
    }

    /**
     * Change quest to specified state
     *
     * @access public
     *
     * @param $state
     * @param $userId
     * @param $questId
     *
     * @return array
     */
    public function changeQuestToState($state, $userId, $questId)
    {
        $em = $this->getEm();
        $userToQuest = $this->findUserToQuestByUserIDAndQuestID($userId, $questId);
        global $kernel;
        if (is_null($userToQuest)) {
            $userToQuest = $kernel
                ->getContainer()
                ->get('app.manager.quest')
                ->createNewUserToQuest($em, $userId, $questId);
        }

        if ($state === $this->SKIPPED) {
            $userToQuest->setState($this->SKIPPED);
        } else {
            $userToQuest->setState($this->IN_PROGRESS);
        }
        $em->persist($userToQuest);
        $em->flush();
        if ($userToQuest !== null) {
            try {
                $em->persist($userToQuest);
                $em->flush();
            } catch (DBALException $e) {
                return array(
                    $this->STATUS => $this->QUEST_IS_ALREADY_IN_PROGRESS
                );
            }
        }
        return array(
            $this->STATUS => $this->STATUS_OK
        );
    }

    public function getUserToQuestFromData($data)
    {
        global $kernel;
        $validatedResponse = $kernel
            ->getContainer()
            ->get('app.manager.validator')
            ->getUserIdFromDataQuestId($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return $validatedResponse;
        }
        $userId = $validatedResponse[$this->USER_ID];
        $questId = $validatedResponse[$this->QUEST_ID];
        $userToQuest = $this
            ->findUserToQuestByUserIDAndQuestID(
                $userId,
                $questId
            );
        return array(
            $this->STATUS => $this->STATUS_OK,
            $this->USER_ID => $userId,
            $this->QUEST_ID => $questId,
            $this->USER_TO_QUEST => $userToQuest
        );
    }
}
