<?php
/**
 * Class File ShopManager
 *
 * PHP version 5.5
 *
 * @package AppBundle\Manager
 */
namespace AppBundle\Manager;

use AppBundle\Entity\Purchase;
use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\ShopFieldsTrait;
use AppBundle\Manager\Traits\SkillFieldsTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;

/**
 * Class ShopManager
 *
 * @package AppBundle\Manager
 */
class ShopManager extends Manager
{
    use ErrorCodesTrait;
    use ShopFieldsTrait;
    use UserInfoFieldsTrait;
    use SkillFieldsTrait;

    /**
     * Show all show units
     *
     * @param $data
     *
     * @return array
     */
    public function showShopUnits($data)
    {
        global $kernel;
        $validatedResponse = $kernel->getContainer()->get('app.manager.validator')->getUserIdFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return $validatedResponse;
        }

        $userId = $validatedResponse[$this->USER_ID];
        $personage = $kernel->getContainer()->get('app.manager.account')->getPersonageByUserId($userId);
        $units = $this->getEm()->getRepository('AppBundle:ShopUnit')->findAll();
        $response[$this->SHOP_UNITS] = array();
        $purchases = $this->getEm()->getRepository('AppBundle:Purchase')->findBy(array($this->PERSONAGE => $personage));
        $boughtUnits = array();
        foreach ($purchases as $purchase) {
            $boughtUnits[] = $purchase->getShopUnit();
        }
        foreach ($units as $unit) {
            if (!in_array($unit, $boughtUnits)) {
                if (($unit->getSkill() && $personage->getClass()->getSkills()->contains($unit->getSkill()))
                    || !$unit->getSkill()
                ) {
                    $array[$this->UNIT_ID] = $unit->getId();
                    $array[$this->UNIT_NAME] = $unit->getName();
                    $array[$this->UNIT_TYPE] = $unit->getType();
                    $array[$this->UNIT_COUNT] = $unit->getCount();
                    $array[$this->UNIT_PRICE] = $unit->getPriceCount();
                    $array[$this->PRICE_TYPE] = $unit->getPriceType();
                    if ($unit->getSkill()) {
                        $array[$this->SKILL_ID] = $unit->getSkill()->getId();
                    } else {
                        $array[$this->SKILL_ID] = 0;
                    }
                    $response[$this->SHOP_UNITS][] = $array;
                }
            }
        }
        $response[$this->STATUS] = $this->STATUS_OK;
        return $response;
    }

    /**
     * Buy shop unit
     *
     * @param $data
     *
     * @return array
     */
    public function buyShopUnit($data)
    {
        global $kernel;
        $validatedResponse = $kernel->getContainer()
            ->get('app.manager.validator')
            ->getUserIdFromData($data);
        if ($validatedResponse[$this->STATUS] != $this->STATUS_OK) {
            return $validatedResponse;
        }
        $userId = $validatedResponse[$this->USER_ID];
        $unit = $this->getEm()->getRepository('AppBundle:ShopUnit')->find($data[$this->UNIT_ID]);
        if (!$unit) {
            return array(
                $this->STATUS => $this->WRONG_UNIT
            );
        }
        $personage = $kernel->getContainer()->get('app.manager.account')->getPersonageByUserId($userId);
        $purchase = $this->getEm()
            ->getRepository('AppBundle:Purchase')
            ->findOneBy(array($this->PERSONAGE => $personage, $this->SHOP_UNIT => $unit));
        if ($unit->getIsOneTime() && $purchase) {
            return array(
                $this->STATUS => $this->UNIT_IS_ALREADY_BOUGHT
            );
        }
        $qb = $this->getEm()
            ->getRepository('AppBundle:CharToSkill')
            ->createQueryBuilder('cts')
            ->select('COUNT(cts)')
            ->where('cts.char = :charId')
            ->setParameter('charId', $personage->getId());
        $count = (int)$qb->getQuery()->getSingleScalarResult();
        if ($personage->getBagSize() <= $count) {
            return array(
                $this->STATUS => $this->NO_FREE_SLOT_FOR_SKILL
            );
        }
        switch ($unit->getPriceType()) {
            case $this->GOLD:
                if (!$kernel->getContainer()
                    ->get('app.manager.account')
                    ->expendGold($personage, $unit->getPriceCount())) {
                    return array(
                        $this->STATUS => $this->NOT_ENOUGH_MONEY
                    );
                }
                break;
            case $this->CRYSTALS:
                if (!$kernel->getContainer()
                    ->get('app.manager.account')
                    ->expendCrystals($personage, $unit->getPriceCount())) {
                    return array(
                        $this->STATUS => $this->NOT_ENOUGH_MONEY
                    );
                }
                break;
            default:
                return array(
                    $this->STATUS => $this->WRONG_UNIT
                );
        }
        switch ($unit->getType()) {
            case $this->GOLD:
                $kernel->getContainer()
                    ->get('app.manager.account')
                    ->setGoldToUser($userId, $unit->getCount());
                break;
            case $this->BAG_SLOTS:
                $kernel->getContainer()
                    ->get('app.manager.account')
                    ->setBagSlotsToPersonage($personage, $unit->getCount());
                break;
            case $this->ACTIVE_SLOT:
                $kernel->getContainer()
                    ->get('app.manager.account')
                    ->setActiveSlotToPersonage($personage, $unit->getCount());
                break;
            case $this->SKILL:
                if (!$kernel->getContainer()
                    ->get('app.manager.skillapi')
                    ->setSkillToPersonage($personage, $unit->getSkill())) {
                    return array(
                        $this->STATUS => $this->WRONG_UNIT
                    );
                }
                break;
        }
        if ($unit->getIsOneTime()) {
            $purchase = new Purchase($personage, $unit);
            $this->getEm()->persist($purchase);
            $this->getEm()->flush();
        }
        $response[$this->STATUS] = $this->STATUS_OK;
        return $response;
    }
}
