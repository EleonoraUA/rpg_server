<?php
/**
 * Created by PhpStorm.
 * User: eleonoria
 * Date: 12/6/16
 * Time: 6:23 PM
 */

namespace AppBundle\Manager;

use AppBundle\Entity\FriendRequest;
use AppBundle\Entity\Personage;
use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\FriendsStateTrait;
use AppBundle\Manager\Traits\QuestInfoFieldsTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Doctrine\DBAL\DBALException;

/**
 * Class FriendsManager
 * @package AppBundle\Manager
 */
class FriendsManager extends Manager
{
    use ErrorCodesTrait;
    use FriendsStateTrait;
    use UserInfoFieldsTrait;
    use QuestInfoFieldsTrait;

    /**
     * @param $userId
     * @return array
     */
    public function friendsRequest($userId)
    {
        $user = $this->getEm()->getRepository('AppBundle:User')->find($userId);
        if (!$user) {
            return array(
                $this->STATUS => $this->USER_DOES_NOT_EXIST
            );
        }
        $friends = $this->getEm()->getRepository('AppBundle:FriendRequest')->findAllFriendsByUserId($userId);
        $friendsResponse = array();

        if ($friends) {
            global $kernel;
            foreach ($friends as $friend) {
                $findUserFriend = $this->findUserFriend($friend, $userId);
                $userFriend = $findUserFriend[$this->USER];
                if ($friend->getState() == $this->CONFIRMED_FRIENDS_REQUEST) {
                    $state = $this->CONFIRMED_FRIENDS;
                } elseif ($findUserFriend[$this->STATE] == $this->ACCEPTER) {
                    $state = $this->OUTCOMING_REQUEST;
                } else {
                    $state = $this->INCOMING_REQUEST;
                }
                $account = $this->getEm()
                    ->getRepository('AppBundle:Account')
                    ->findOneBy(
                        array(
                            $this->USER => $userFriend
                        )
                    );
                $personage = $this->getEm()
                    ->getRepository('AppBundle:Personage')
                    ->findOneBy(
                        array(
                            $this->ACCOUNT => $account
                        )
                    );
                $character = $this->getFriendPersonageProfile($personage);
                $isOnline = $kernel
                    ->getContainer()
                    ->get('app.manager.redis')
                    ->isOnlineUserByUserId($userFriend->getId());
                $friendResponse = $this->createFriendsResponseWithParams(
                    $userFriend->getId(),
                    $userFriend->getUsername(),
                    $state,
                    $isOnline,
                    $character
                );
                $friendsResponse[] = $friendResponse;
            }
        }
        return array(
            $this->STATUS => $this->STATUS_OK,
            $this->FRIENDS => $friendsResponse
        );
    }

    /**
     * Add friend request
     *
     * @param $userId
     * @param $friendName
     *
     * @return array
     */
    public function friendAddRequestByFriendName($userId, $friendName)
    {
        $friend = $this->getEm()
            ->getRepository('AppBundle:User')
            ->findOneBy(array($this->USERNAME => $friendName));
        if (!$friend) {
            return array(
                $this->STATUS => $this->USER_NOT_FOUND
            );
        }
        return $this->sendFriendRequest($userId, $friend->getId());
    }

    /**
     * Send friend request
     *
     * @param $senderId
     * @param $accepterId
     *
     * @return array
     */
    public function sendFriendRequest($senderId, $accepterId)
    {
        if ($senderId == $accepterId) {
            return array(
                $this->STATUS => $this->USER_ID_CANNOT_BE_EQUAL_TO_FRIEND
            );
        }

        $sender = $this->getEm()->getRepository('AppBundle:User')->find($senderId);
        $accepter = $this->getEm()->getRepository('AppBundle:User')->find($accepterId);

        if (!$accepter) {
            return array(
                $this->STATUS => $this->USER_DOES_NOT_EXIST
            );
        }

        $requestExists = $this->getEm()
            ->getRepository('AppBundle:FriendRequest')
            ->findFriendsByUserIds($senderId, $accepterId);

        if ($requestExists != null) {
            return array(
                $this->STATUS => $this->FRIEND_REQUEST_IS_ALREADY_SENT
            );
        }

        $friendRequest = new FriendRequest();
        $friendRequest->setSender($sender);
        $friendRequest->setAccepter($accepter);
        $friendRequest->setState($this->NOT_REVIEWED_REQUEST);
        
        try {
            $this->getEm()->persist($friendRequest);
            $this->getEm()->flush();
        } catch (DBALException $e) {
            return array(
                $this->STATUS => $this->FRIEND_REQUEST_IS_ALREADY_SENT
            );
        }

        return array(
            $this->STATUS => $this->STATUS_OK
        );
    }

    /**
     * Cancel friend request
     *
     * @param int $senderId
     * @param int $accepterId
     *
     * @return array
     */
    public function cancelFriendRequest($senderId, $accepterId)
    {
        $requestToCancel = $this->getEm()->getRepository('AppBundle:FriendRequest')->findOneBy(array(
            $this->FRIEND_REQUEST_SENDER => $senderId,
            $this->FRIEND_REQUEST_ACCEPTER => $accepterId,
            $this->STATE => $this->NOT_REVIEWED_REQUEST
        ));

        if (!$requestToCancel) {
            return array(
                $this->STATUS => $this->FRIEND_REQUEST_NOT_FOUND
            );
        }

        $this->getEm()->remove($requestToCancel);
        $this->getEm()->flush();

        return array(
            $this->STATUS => $this->STATUS_OK
        );
    }

    /**
     * Accept friend request
     *
     * @param $senderId
     * @param $accepterId
     *
     * @return array
     */
    public function acceptFriendRequest($senderId, $accepterId)
    {
        if ($senderId == $accepterId) {
            return array(
                $this->STATUS => $this->USER_ID_CANNOT_BE_EQUAL_TO_FRIEND
            );
        }

        $sender = $this->getEm()->getRepository('AppBundle:User')->find($senderId);
        if (!$sender) {
            return array(
                $this->STATUS => $this->USER_DOES_NOT_EXIST
            );
        }
        $friendRequest = $this
            ->getEm()
            ->getRepository('AppBundle:FriendRequest')
            ->findOneBy(
                array(
                    $this->FRIEND_REQUEST_SENDER => $senderId,
                    $this->FRIEND_REQUEST_ACCEPTER => $accepterId
                )
            );

        if (!$friendRequest) {
            return array(
                $this->STATUS => $this->FRIEND_REQUEST_NOT_FOUND
            );
        }
        $friendStatus = $friendRequest->getState();

        if ($friendStatus == $this->CONFIRMED_FRIENDS_REQUEST) {
            return array(
                $this->STATUS => $this->ARE_ALREADY_FRIENDS
            );
        }

        $friendRequest->setState($this->CONFIRMED_FRIENDS_REQUEST);
        $this->getEm()->persist($friendRequest);
        $this->getEm()->flush();
        return array(
            $this->STATUS => $this->STATUS_OK
        );
    }

    /**
     * Skip friend request
     *
     * @param $senderId
     * @param $accepterId
     *
     * @return array
     */
    public function skipFriendRequest($senderId, $accepterId)
    {
        if ($senderId == $accepterId) {
            return array(
                $this->STATUS => $this->USER_ID_CANNOT_BE_EQUAL_TO_FRIEND
            );
        }

        $sender = $this->getEm()->getRepository('AppBundle:User')->find($senderId);
        if (!$sender) {
            return array(
                $this->STATUS => $this->USER_DOES_NOT_EXIST
            );
        }
        $friendRequest = $this
            ->getEm()
            ->getRepository('AppBundle:FriendRequest')
            ->findOneBy(
                array(
                    $this->FRIEND_REQUEST_SENDER => $senderId,
                    $this->FRIEND_REQUEST_ACCEPTER => $accepterId
                )
            );

        if (!$friendRequest) {
            return array(
                $this->STATUS => $this->FRIEND_REQUEST_NOT_FOUND
            );
        }

        $friendStatus = $friendRequest->getState();
        if ($friendStatus == $this->CONFIRMED_FRIENDS_REQUEST) {
            return array(
                $this->STATUS => $this->ARE_ALREADY_FRIENDS
            );
        }
        $this->getEm()->remove($friendRequest);
        $this->getEm()->flush();
        return array(
            $this->STATUS => $this->STATUS_OK
        );
    }

    /**
     * Delete user from friends
     *
     * @param int $userId
     * @param int $friendId
     *
     * @return array
     */
    public function deleteUserFromFriends($userId, $friendId)
    {
        $friends = $this->getEm()
            ->getRepository('AppBundle:FriendRequest')
            ->findFriendsByUserIds($userId, $friendId, $this->CONFIRMED_FRIENDS_REQUEST);
        if (!$friends) {
            return array(
                $this->STATUS => $this->FRIEND_NOT_FOUND
            );
        }

        $this->getEm()->remove($friends);
        $this->getEm()->flush();
        return array(
            $this->STATUS => $this->STATUS_OK
        );
    }

    /**
     * Create a friends response with params
     *
     * @param $userId
     * @param $name
     * @param $state
     * @param $isOnline
     * @param $character
     *
     * @return array
     */
    private function createFriendsResponseWithParams(
        $userId,
        $name,
        $state,
        $isOnline,
        $character
    ) {
        $friend = array(
            $this->FRIEND_ID => $userId,
            $this->NAME => $name,
            $this->STATE => $state,
            $this->IS_ONLINE => $isOnline,
            $this->CHARACTER => $character
        );
        return $friend;
    }

    /**
     * Returns entity of user who is a friend from friendRequest
     *
     * @param FriendRequest $friends
     * @param $userId
     *
     * @return array
     */
    private function findUserFriend(FriendRequest $friends, $userId)
    {
        if ($friends->getSender()->getId() == $userId) {
            return array($this->USER => $friends->getAccepter(), $this->STATE => $this->ACCEPTER);
        } elseif ($friends->getAccepter()->getId() == $userId) {
            return array($this->USER => $friends->getSender(), $this->STATE => $this->SENDER);
        }
    }

    /**
     * Get information about friend's personage profile
     *
     * @param Personage $personage
     *
     * @return array
     */
    public function getFriendPersonageProfile(Personage $personage)
    {
        $profile[$this->NAME] = $personage->getName();
        $profile[$this->LVL] = $personage->getLvl();
        $profile[$this->CLASS_ID] = $personage->getClass()->getId();
        $profile[$this->AVATAR_ID] = $personage->getAccount()->getAvatarId();
        return $profile;
    }
}
