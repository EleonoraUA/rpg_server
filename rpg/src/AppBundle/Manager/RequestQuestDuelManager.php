<?php

namespace AppBundle\Manager;

use AppBundle\Entity\QuestDuelRequest;
use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\FriendsStateTrait;
use AppBundle\Manager\Traits\QuestInfoFieldsTrait;
use AppBundle\Manager\Traits\QuestStatesTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Doctrine\DBAL\DBALException;

/**
 * Class RequestQuestDuelManager is used to store logic for duel quests
 *
 * @package AppBundle\Manager
 */
class RequestQuestDuelManager extends Manager
{
    use QuestStatesTrait;
    use ErrorCodesTrait;
    use FriendsStateTrait;
    use UserInfoFieldsTrait;
    use QuestInfoFieldsTrait;

    const DAYS_COUNT_TO_PROVE_QUEST = 3;

    public function sendQuestDuelRequest($senderId, $accepterId)
    {
        $sender = $this->getEm()->getRepository('AppBundle:User')->find($senderId);
        $accepter = $this->getEm()->getRepository('AppBundle:User')->find($accepterId);

        if (!$accepter) {
            return array(
                $this->STATUS => $this->USER_DOES_NOT_EXIST
            );
        }

        $areFriends = $this->getEm()
            ->getRepository('AppBundle:FriendRequest')
            ->findFriendsByUserIds($senderId, $accepterId, $this->CONFIRMED_FRIENDS_REQUEST);
        
        $requestExists = $this->getEm()
            ->getRepository('AppBundle:QuestDuelRequest')
            ->findQuestDuelRequest($senderId, $accepterId, array($this->IN_PROGRESS, $this->EMPTY_STATE));

        if ($areFriends === null || $requestExists) {
            return array(
                $this->STATUS => $this->DUEL_REQUEST_CANNOT_BE_SENT
            );
        }

        $doneQuests = $this->getEm()->getRepository('AppBundle:QuestDuelRequest')->findAllDoneQuestsOfPlayers($senderId, $accepterId);
        if (!empty($doneQuests)) {
            $doneQuests = array_map('current', $doneQuests);
            $availableQuests = $this->getEm()->getRepository('AppBundle:QuestDuel')->findAvailableQuests($doneQuests);
        } else {
            $availableQuests = $this->getEm()->getRepository('AppBundle:QuestDuel')->findAll();
        }
        if (empty($availableQuests)) {
            return array(
                $this->STATUS => $this->NO_QUESTS_TO_DO_WITH_THIS_USER
            );
        }
        $questIndex = array_rand($availableQuests);
        $questDuelRequest = new QuestDuelRequest($availableQuests[$questIndex]->getQuest(), $sender, $accepter);

        try {
            $this->getEm()->persist($questDuelRequest);
            $this->getEm()->flush();
        } catch (DBALException $e) {
            return array(
                $this->STATUS => $this->QUEST_DUEL_REQUEST_IS_ALREADY_SENDED
            );
        }

        return array(
            $this->STATUS => $this->STATUS_OK
        );
    }

    /**
     * Finding certain duel quest by ID
     *
     * @access public
     *
     * @param $questId
     *
     * @return null|object
     */
    public function findQuestByQuestID($questId)
    {
        return $this->getEm()->getRepository('AppBundle:QuestDuel')->findOneBy(array($this->QUEST =>$questId));
    }


    /**
     * Finding QuestDuelRequest by user's ID and quest's ID
     *
     * @access public
     *
     * @param $userId
     * @param $friendId
     *
     * @return null|object
     */
    public function findQuestDuelWithParams($userId, $friendId)
    {
        return $this->getEm()
            ->getRepository('AppBundle:QuestDuelRequest')
            ->findOneBy(
                array(
                    $this->FRIEND_REQUEST_ACCEPTER => $userId,
                    $this->FRIEND_REQUEST_SENDER => $friendId,
                    $this->STATE => 0
                )
            );
    }

    /**
     * Finding all QuestDuelRequests by user's ID
     *
     * @access public
     *
     * @param $userId
     *
     * @return array
     */
    public function findQuestDuelRequestByUserID($userId)
    {
        return $this->getEm()
            ->getRepository('AppBundle:QuestDuelRequest')
            ->find($userId);
    }

    public function getDuelToChangeStateFromData($data)
    {
        global $kernel;
        $validatedResponse = $kernel
            ->getContainer()
            ->get('app.manager.validator')
            ->getUserIdFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return $validatedResponse;
        }

        $userId = $validatedResponse[$this->USER_ID];
        $questId = $data[$this->QUEST_ID];

        $quest = $this->findQuestByQuestID($questId);
        if (!$quest) {
            return array(
                $this->STATUS => $this->QUEST_WITH_SUCH_ID_NOT_FOUND
            );
        }

        $friendId = $data[$this->FRIEND_ID];
        $friends = $this->getEm()
            ->getRepository('AppBundle:FriendRequest')
            ->findFriendsByUserIds($userId, $friendId, $this->CONFIRMED_FRIENDS_REQUEST);
        if (!$friends) {
            return array(
                $this->STATUS => $this->FRIEND_NOT_FOUND
            );
        }

        $questDuelRequest = $this
            ->findQuestDuelWithParams(
                $userId,
                $friendId
            );
        if (!$questDuelRequest) {
            return array(
                $this->STATUS => $this->QUEST_DUEL_REQUEST_NOT_FOUND
            );
        }

        return array(
            $this->STATUS => $this->STATUS_OK,
            $this->QUEST_DUEL_REQUEST => $questDuelRequest
        );
    }

    /**
     * Change duel quest to specified state
     *
     * @access public
     *
     * @param $questDuelRequest
     * @param $state
     *
     * @return array
     */
    public function changeQuestDuelRequestToState(QuestDuelRequest $questDuelRequest, $state)
    {
        $em = $this->getEm();
//        if ($questDuelRequest->getState() == $state ||
//            $questDuelRequest->getState() == $this->ACCEPTER_WIN ||
//            $questDuelRequest->getState() == $this->SENDER_WIN) {
//            return array(
//                $this->STATUS => $this->QUEST_DUEL_IS_ALREADY_IN_PROGRESS
//            );
//        }
        switch ($state) {
            case $this->IN_PROGRESS:
                $questDuelRequest->setState($state);
                $questDuelRequest->setAcceptedAt(new \DateTime("now"));
                $questDuelRequest->setDaysLeft(self::DAYS_COUNT_TO_PROVE_QUEST);
                $em->persist($questDuelRequest);
                break;
            case $this->SKIPPED:
                $em->remove($questDuelRequest);
                break;
        }
        $em->flush();

//        if ($questDuelRequest !== null) {
//            try {
//                $em->persist($questDuelRequest);
//                $em->flush();
//            } catch (DBALException $e) {
//                return array(
//                    $this->STATUS => $this->QUEST_IS_ALREADY_IN_PROGRESS
//                );
//            }
//        }
        return array(
            $this->STATUS => $this->STATUS_OK
        );
    }

    public function getDuelQuestsWithState($state)
    {
        $quests = $this->getEm()->getRepository('AppBundle:QuestDuelRequest')->findBy(
            array(
                $this->STATE => $state
            )
        );

        return $quests;
    }
    
    public function getRewardForDuelQuest($questId)
    {
        $quest = $this->getEm()->getRepository('AppBundle:Quest')->find($questId);
        return $quest->getReward();
    }
    
    public function updateQuestDuelEntity(QuestDuelRequest $duel)
    {
        $this->getEm()->persist($duel);
        $this->getEm()->flush();
    }
}
