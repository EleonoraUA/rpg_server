<?php
/**
 * Class File ValidatorManager
 *
 * PHP version 5.5
 *
 * @package AppBundle\Manager
 */
namespace AppBundle\Manager;

use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\FriendsStateTrait;
use AppBundle\Manager\Traits\JsonFieldsTrait;
use AppBundle\Manager\Traits\QuestInfoFieldsTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;

/**
 * Class ValidatorManager
 * Validate data and return status
 *
 * @package AppBundle\Manager
 */
class ValidatorManager extends Manager
{
    use ErrorCodesTrait;
    use FriendsStateTrait;
    use UserInfoFieldsTrait;
    use QuestInfoFieldsTrait;

    /**
     * Return status and user_id
     *
     * @param $data
     *
     * @return array
     */
    public function getUserIdFromData($data)
    {
        if ($data === null) {
            return array(
                $this->STATUS => $this->WRONG_JSON
            );
        }
        global $kernel;
        $userId = $kernel->getContainer()->get('app.manager.jsonvalidator')->getUserIdFromData($data);
        if (!$userId) {
            return array(
                $this->STATUS => $this->WRONG_TOKEN
            );
        }
        return array(
            $this->STATUS => $this->STATUS_OK,
            $this->USER_ID => $userId
        );
    }

    /**
     * Return status and personage
     *
     * @param $data
     *
     * @return array
     */
    public function getPersonageFromData($data)
    {
        $validatedResponse = $this->getUserIdFromData($data);
        if ($validatedResponse[$this->STATUS] != $this->STATUS_OK) {
            return $validatedResponse;
        }
        global $kernel;
        $personage = $kernel
            ->getContainer()
            ->get('app.manager.account')
            ->getPersonageByUserId($validatedResponse[$this->USER_ID]);
        if (!$personage) {
            return array(
                $this->STATUS => $this->USER_DOES_NOT_EXIST
            );
        }
        return array(
            $this->STATUS => $this->STATUS_OK,
            $this->PERSONAGE => $personage
        );
    }

    /**
     * @param $data
     * @return array
     */
    public function getUserIdFromDataQuestId($data)
    {
        global $kernel;
        $validatedResponse = $this->getUserIdFromData($data);
        if ($validatedResponse[$this->STATUS] != $this->STATUS_OK) {
            return $validatedResponse;
        }

        $userId = $validatedResponse[$this->USER_ID];
        $questId = $data[$this->QUEST_ID];

        $quest = $this->getEm()
            ->getRepository('AppBundle:Quest')
            ->findOneBy(array($this->ID => $questId));
        if (!$quest) {
            return array(
                $this->STATUS => $this->QUEST_WITH_SUCH_ID_NOT_FOUND
            );
        }

        $isTheSameClass = $kernel
            ->getContainer()
            ->get('app.manager.quest')
            ->isTheSameClassForQuestAndPersonage($userId, $questId);
        if (!$isTheSameClass) {
            return array(
                $this->STATUS => $this->DIFFERENT_CLASS_OF_PERSONAGE_AND_QUEST
            );
        }

        $isTheSameLvl = $kernel
            ->getContainer()
            ->get('app.manager.quest')
            ->isTheSameLvlForQuestAndPersonage($userId, $questId);
        if (!$isTheSameLvl) {
            return array(
                $this->STATUS => $this->DIFFERENT_LVL_OF_PERSONAGE_AND_QUEST
            );
        }

        return array(
            $this->STATUS => $this->STATUS_OK,
            $this->USER_ID => $userId,
            $this->QUEST_ID => $questId
        );
    }

    public function getRequestIdsFromData($data)
    {
        $validatedResponse = $this->getUserIdFromData($data);
        if ($validatedResponse[$this->STATUS] != $this->STATUS_OK) {
            return $validatedResponse;
        }

        $userId = (int)$validatedResponse[$this->USER_ID];
        $questId = (int)$data[$this->QUEST_ID];

        $quest = $this->getEm()
            ->getRepository('AppBundle:Quest')
            ->findOneBy(array($this->ID => $questId));
        if (!$quest) {
            return array(
                $this->STATUS => $this->QUEST_WITH_SUCH_ID_NOT_FOUND
            );
        }

        $friendId = $data[$this->FRIEND_ID];
        $friends = $this->getEm()
            ->getRepository('AppBundle:FriendRequest')
            ->findFriendsByUserIds($userId, $friendId, $this->CONFIRMED_FRIENDS_REQUEST);
        if (!$friends) {
            return array(
                $this->STATUS => $this->FRIEND_NOT_FOUND
            );
        }

        return array(
            $this->STATUS => $this->STATUS_OK,
            $this->USER_ID => $userId,
            $this->FRIEND_ID => $friendId,
            $this->QUEST_ID => $questId
        );
    }
}
