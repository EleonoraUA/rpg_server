<?php
/**
 * Class File SkillAPIManager
 *
 * PHP version 5.5
 *
 * @package AppBundle\Manager
 */
namespace AppBundle\Manager;

use AppBundle\Manager\Traits\BattleTrait;
use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Entity\Personage;
use AppBundle\Entity\Skill;
use AppBundle\Entity\CharToSkill;
use AppBundle\Manager\Traits\SkillFieldsTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;

/**
 * Class SkillAPIManager is used to store logic for skills
 *
 * @package AppBundle\Manager
 */
class SkillAPIManager extends Manager
{
    use ErrorCodesTrait;
    use UserInfoFieldsTrait;
    use SkillFieldsTrait;
    use BattleTrait;

    /**
     * Get information about personage's skills
     *
     * @param Personage $personage
     *
     * @return array
     */
    public function getSkillsProfile($personage)
    {
        $skills = $this->getEm()->getRepository('AppBundle:CharToSkill')->findBy(array($this->CHAR => $personage));
        $skills_profile = array();
        foreach ($skills as $skill) {
            $skill_profile[$this->SKILL_ID] = $skill->getSkill()->getId();
            $skill_profile[$this->IS_SELECTED_SKILL] = $skill->getIsSelected();
            $skill_profile[$this->REQUIRED_LVL] = $skill->getSkill()->getRequiredLvl();
            $skills_profile[] = $skill_profile;
        }
        return $skills_profile;
    }

    /**
     * Get all skills of personage
     *
     * @param Personage $personage personage entity
     *
     * @access public
     *
     * @return array
     */
    public function getSkillsOfPersonage($personage)
    {
        $em = $this->getEm();
        $char_skills = $em->getRepository('AppBundle:CharToSkill')->findBy(array($this->CHAR => $personage));
        $skills = array();
        foreach ($char_skills as $char_skill) {
            array_push($skills, $char_skill);
        }
        return $skills;
    }

    /**
     * Get skills for personage when arena initialize
     *
     * @param array $skillIds
     *
     * @return array
     */
    public function getArenaSkillsForPersonage($skillIds)
    {
        return $this->getEm()->getRepository('AppBundle:Skill')->findSkillsBySkillsIds($skillIds);
    }

    /**
     * Get selected skills of personage
     *
     * @param Personage $personage personage entity
     *
     * @access public
     *
     * @return array
     */
    public function getSelectedSkillsOfPersonage($personage)
    {
        $em = $this->getEm();
        $char_skills = $em
            ->getRepository('AppBundle:CharToSkill')
            ->findBy(array($this->CHAR => $personage, $this->IS_SELECTED => true));
        $skills = array();
        foreach ($char_skills as $char_skill) {
            array_push($skills, $char_skill->getSkill());
        }
        return $skills;
    }

    /**
     * Check if skill can be used
     *
     * @param Personage $personage
     *
     * @param Skill $skill
     *
     * @return bool
     */
    public function skillCanBeUsed($personage, $skill)
    {
        if (!$personage->getClass()->getSkills()->contains($skill)) {
            return false;
        }
        return true;
    }

    /**
     * Add skill to personage
     *
     * @param Personage $personage
     *
     * @param Skill $skill
     *
     * @return bool
     */
    public function setSkillToPersonage($personage, $skill)
    {
        if (!$personage->getClass()->getSkills()->contains($skill)) {
            return false;
        }
        if ($this->getEm()
            ->getRepository('AppBundle:CharToSkill')
            ->findOneBy(array($this->CHAR => $personage, $this->SKILL => $skill))) {
            return false;
        }
        $charToSkill = new CharToSkill($personage, $skill, false);
        $this->getEm()->persist($charToSkill);
        $this->getEm()->flush();
        return true;
    }

    /**
     * Get skill by skill_id
     *
     * @param $skillId
     *
     * @return null|object
     */
    public function getSkillByID($skillId)
    {
        $skill = $this->getEm()->getRepository('AppBundle:Skill')->findOneBy(array($this->ID => $skillId));
        return $skill;
    }

    /**
     * Count selected skills in database
     *
     * @param $allPersonageSkills
     * @param $skills
     *
     * @return int
     */
    public function countChanges($allPersonageSkills, $skills)
    {
        $countChanges = 0;
        foreach ($allPersonageSkills as $skill) {
            if (in_array($skill->getSkill()->getId(), $skills)) {
                $skill->setIsSelected(1);
                $countChanges++;
            } else {
                $skill->setIsSelected(0);
            }
            $this->getEm()->persist($skill);
        }
        return $countChanges;
    }

    /**
     * Validate arena skills
     *
     * @param Personage $personage
     *
     * @param array $skillIds
     *
     * @return bool
     */
    public function arenaSkillsAreValid($personage, $skillIds)
    {
        if (count($skillIds) != $this->ARENA_SKILLS_NUMBER) {
            return false;
        }
        $skills = $this->getEm()->getRepository('AppBundle:Skill')->findSkillsBySkillsIds($skillIds);
        if (count($skills) != $this->ARENA_SKILLS_NUMBER) {
            return false;
        }
        foreach ($skills as $skill) {
            if (!$this->skillCanBeUsed($personage, $skill)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Create a response for getSkillByIdAction
     *
     * @param $skillId
     *
     * @return array
     */
    public function getResponseForSkillByIdAction($skillId)
    {
        $skill = $this
            ->getEm()
            ->getRepository('AppBundle:Skill')
            ->findOneBy(array($this->ID => $skillId));
        if (!$skill) {
            return array(
                $this->STATUS => $this->SKILL_WITH_SUCH_ID_NOT_FOUND
            );
        } else {
            $response[$this->SKILL] = array(
                $this->NAME => $skill->getName(),
                $this->DESCRIPTION =>$skill->getDescription(),
                $this->MULTIPLIER => $skill->getMultiplier()
            );
        }
        $response[$this->STATUS] = $this->STATUS_OK;
        return $response;
    }

    /**
     * Create a response for selectSkillsAction
     *
     * @param $data
     *
     * @return array
     */
    public function getResponseForSelectSkillsAction($data)
    {
        global $kernel;
        $validatedResponse = $kernel->getContainer()->get('app.manager.validator')->getPersonageFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return $validatedResponse;
        }

        $personage = $validatedResponse[$this->PERSONAGE];
        $allPersonageSkills = $this->getSkillsOfPersonage($personage);
        if (!$allPersonageSkills) {
            return array(
                $this->STATUS => $this->PERSONAGE_HAS_NO_ANY_SKILLS
            );
        }

        $skillsId = $data[$this->SKILLS];
        if (!$skillsId) {
            return array(
                $this->STATUS => $this->EMPTY_SKILLS_TO_SELECT
            );
        }

        if (count($skillsId) > $personage->getActiveSkillsBagSize()) {
            return array(
                $this->STATUS => $this->EXCEED_ACTIVE_SKILLS_BAG_SIZE
            );
        }

        $skills = array_unique($skillsId);
        foreach ($skillsId as $skillId) {
            $skill = $this->getSkillByID($skillId);
            if (!$skill) {
                return array(
                    $this->STATUS => $this->SKILL_WITH_SUCH_ID_NOT_FOUND
                );
            }
            if ($skill->getRequiredLvl() > $personage->getLvl()) {
                return array(
                    $this->STATUS => $this->LEVEL_OF_PERSONAGE_LESS_THAN_SKILLS
                );
            }
        }

        $countChanges = $this->countChanges($allPersonageSkills, $skills);
        if ($countChanges < count($skills)) {
            return array(
                $this->STATUS => $this->PERSONAGE_HAS_NO_SUCH_SKILLS
            );
        }

        $this->getEm()->flush();
        return array(
            $this->STATUS => $this->STATUS_OK
        );
    }

    /**
     * Create a response with status and arena skills of personage
     *
     * @param $data
     *
     * @return array
     */
    public function getResponseForArenaSkillsAction($data)
    {
        global $kernel;
        $validatedResponse = $kernel->getContainer()->get('app.manager.validator')->getPersonageFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return $validatedResponse;
        }

        $personage = $validatedResponse[$this->PERSONAGE];
        $skills = $personage->getClass()->getSkills();
        if (!$skills) {
            return array(
                $this->STATUS => $this->SKILLS_NOT_FOUND
            );
        } else {
            foreach ($skills as $skill) {
                $skillsArray[] = $skill->getId();
            }
        }
        shuffle($skillsArray);

        if (count($skills) > $this->NUMBER_OF_SKILLS_FOR_ARENA) {
            $skillsArray = array_slice($skillsArray, 0, $this->NUMBER_OF_SKILLS_FOR_ARENA);
        }

        $response[$this->SKILLS] = $skillsArray;
        $response[$this->STATUS] = $this->STATUS_OK;

        return $response;
    }

    /**
     * Create a response with status all skills of personage
     *
     * @param $data
     *
     * @return mixed
     */
    public function getResponseForSkillsAction($data)
    {
        global $kernel;
        $validatedResponse = $kernel->getContainer()->get('app.manager.validator')->getPersonageFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return $validatedResponse;
        }
        $personage = $validatedResponse[$this->PERSONAGE];

        $skills = $this->getSkillsOfPersonage($personage);
        if (empty($skills)) {
            $response[$this->SKILLS] = array();
        } else {
            foreach ($skills as $skill) {
                $response[$this->SKILLS][] = array(
                    $this->SKILL_ID => $skill->getSkill()->getId(),
                    $this->IS_SELECTED_SKILL => $skill->getIsSelected()
                );
            }
        }
        $response[$this->STATUS] = $this->STATUS_OK;
        return $response;
    }

    public function getEffectsForSkill($skillId)
    {
        return $this->getEm()->getRepository('AppBundle:SkillToEffect')->findEffectsBySkill($skillId);
    }
}
