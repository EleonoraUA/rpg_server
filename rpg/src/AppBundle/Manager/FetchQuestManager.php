<?php
/**
 * Class File FetchQuestManager
 *
 * PHP version 5.5
 *
 * @package AppBundle\Manager
 */
namespace AppBundle\Manager;

use AppBundle\Manager\Traits\BattleTrait;
use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\QuestInfoFieldsTrait;
use AppBundle\Manager\Traits\QuestStatesTrait;
use AppBundle\Manager\Traits\RequestResponseTypesTrait;
use AppBundle\Manager\Traits\SkillFieldsTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class FetchQuestManager is used to store logic for fetching user's quests
 * @package AppBundle\Manager
 */
class FetchQuestManager extends Manager
{
    use QuestStatesTrait;
    use ErrorCodesTrait;
    use RequestResponseTypesTrait;
    use UserInfoFieldsTrait;
    use QuestInfoFieldsTrait;
    use SkillFieldsTrait;
    use BattleTrait;

    /**
     * Create response depends on quest's state
     *
     * @param $request
     *
     * @param $type
     *
     * @return null|JsonResponse
     */
    public function createResponse($request, $type)
    {
        global $kernel;
        $data = $kernel
            ->getContainer()
            ->get('app.manager.jsonvalidator')
            ->getValidatedJsonFromRequest($request->getContent(), $type);

        $validatedResponse = $kernel->getContainer()->get('app.manager.validator')->getUserIdFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return new JsonResponse($validatedResponse);
        }

        $userId = $validatedResponse[$this->USER_ID];
        $response = null;
        switch ($type) {
            case $this->QUESTS_REQUEST:
                $response = $this->getAvailableToTakeQuests($userId);
                break;
            case $this->CONFIRMED_QUESTS_REQUEST:
                $response  =  $this->getConfirmedQuests($userId);
                break;
            case $this->INPROGRESS_QUESTS_REQUEST:
                $response =  $this->getInProgressQuests($userId);
                break;
        }
        return $response;
    }

    /**
     * Get quests that user can take
     *
     * @access public
     *
     * @param int $userId
     *
     * @return JsonResponse
     */
    public function getAvailableToTakeQuests($userId)
    {
        global $kernel;
        $userPersonage = $kernel->getContainer()->get('app.manager.account')->getPersonageByUserId($userId);
        $allPersonagesQuests = $userPersonage->getClass()->getQuests();
        $allowedQuestsToTake = array();
        $quests = array();
        foreach ($allPersonagesQuests as $personagesQuest) {
            if ($personagesQuest->getRequiredLvl() <= $userPersonage->getLvl()) {
                array_push($allowedQuestsToTake, $personagesQuest);
            }
        }
        $takenQuests = $this->getEm()->getRepository('AppBundle:UserToQuest')->findTakenQuestsByUserId($userId);
        if (empty($takenQuests)) {
            $quests = array_slice($allowedQuestsToTake, 0, 10);
        } else {
            $ids = array_map('current', $takenQuests);
            foreach ($allowedQuestsToTake as $questToTake) {
                if (!in_array($questToTake->getId(), $ids)) {
                    array_push($quests, $questToTake);
                }
            }
        }
        $quests = array_slice($quests, 0, 10);
        if (empty($quests)) {
            $response[$this->QUESTS] = array();
        } else {
            foreach ($quests as $quest) {
                $proveImageUrl = "";
                $response[$this->QUESTS][] = $this->createQuestResponseWithParams(
                    $quest->getRequiredLvl(),
                    $quest->getId(),
                    $quest->getName(),
                    $quest->getDescription(),
                    $this->EMPTY_STATE,
                    $proveImageUrl,
                    $quest->getReward(),
                    0
                );
            }
        }
        $response[$this->STATUS] = $this->STATUS_OK;
        return $response;
    }

    /**
     * Get quests that were confirmed (reviewed true) by other users
     *
     * @access public
     *
     * @param int $userId
     *
     * @return JsonResponse
     */
    public function getConfirmedQuests($userId)
    {
        global $kernel;
        $usersQuests = $kernel
            ->getContainer()
            ->get('app.manager.quest')
            ->findUsersQuestsWithState($userId, $this->REVIEWED_TRUE);
        if (!$usersQuests) {
            $response[$this->QUESTS] = array();
        } else {
            foreach ($usersQuests as $usersQuest) {
                $quest = $usersQuest->getQuest();
                $proveImageUrl = $this->PROOF_QUEST_IMAGE_URL.$userId.'/'.$quest->getId().$this->JPG_EXTENSION;
                $response[$this->QUESTS][] = $this->createQuestResponseWithParams(
                    $quest->getRequiredLvl(),
                    $quest->getId(),
                    $quest->getName(),
                    $quest->getDescription(),
                    $usersQuest->getState(),
                    $proveImageUrl,
                    $quest->getReward(),
                    $usersQuest->hasGotReward()
                );
            }
        }
        $response[$this->STATUS] = $this->STATUS_OK;
        return $response;
    }

    /**
     * Get quests that are in progress
     *
     * @access public
     *
     * @param int $userId
     *
     * @return JsonResponse
     */
    public function getInProgressQuests($userId)
    {
        $quests = $this->getEm()->getRepository('AppBundle:UserToQuest')->findInProgressQuestsByUserId($userId);
        if (!$quests) {
            $response[$this->QUESTS] = array();
        } else {
            foreach ($quests as $quest) {
                $otherQuest = $quest->getQuest();
                if ($quest->getState() === $this->IN_PROGRESS) {
                    $proveImageUrl = "";
                } else {
                    $proveImageUrl = $this->PROOF_QUEST_IMAGE_URL.$userId.'/'.$otherQuest->getId().$this->JPG_EXTENSION;
                }
                $response[$this->QUESTS][] = $this->createQuestResponseWithParams(
                    $otherQuest->getRequiredLvl(),
                    $otherQuest->getId(),
                    $otherQuest->getName(),
                    $otherQuest->getDescription(),
                    $quest->getState(),
                    $proveImageUrl,
                    $otherQuest->getReward(),
                    0
                );
            }
        }
        $response[$this->STATUS] = $this->STATUS_OK;
        return $response;
    }

    /**
     * Create response array of confirmed quests depending on quest parameters
     *
     * @access public
     *
     * @param $requiredLvl
     * @param $questId
     * @param $name
     * @param $description
     * @param $state
     * @param $proveImageUrl
     * @param $reward
     * @param $gotReward
     *
     * @return array
     */
    public function createQuestResponseWithParams(
        $requiredLvl,
        $questId,
        $name,
        $description,
        $state,
        $proveImageUrl,
        $reward,
        $gotReward
    ) {
        $quest = array(
            $this->LVL => $requiredLvl,
            $this->QUEST_ID => $questId,
            $this->NAME => $name,
            $this->DESCRIPTION => $description,
            $this->STATE => $state,
            $this->TYPE => 0,
            $this->PROVE_IMAGE_1 => $proveImageUrl,
            $this->REWARD => array(
                $this->GOLD => $reward->getGold(),
                $this->CRYSTALS => $reward->getCrystals(),
                $this->SKILL_ID => $reward->getSkillId()
            ),
            $this->GOT_REWARD => $gotReward
        );
        return $quest;
    }
}
