<?php


namespace AppBundle\Manager;

use AppBundle\Manager\Traits\BattleTrait;
use AppBundle\Manager\Traits\QuestInfoFieldsTrait;
use AppBundle\Manager\Traits\RequestResponseTypesTrait;
use AppBundle\Manager\Traits\ShopFieldsTrait;
use AppBundle\Manager\Traits\SkillFieldsTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;

/**
 * Class JSONValidatorManager
 *
 * @package AppBundle\Manager
 */
class JSONValidatorManager
{
    use RequestResponseTypesTrait;
    use BattleTrait;
    use QuestInfoFieldsTrait;
    use UserInfoFieldsTrait;
    use SkillFieldsTrait;
    use ShopFieldsTrait;

    const TOKEN_LENGTH = 43;
    
    /**
     * @var RedisManager
     * @access private
     */
    private $redisManager;

    /**
     * @return RedisManager
     */
    public function getRedisManager()
    {
        return $this->redisManager;
    }

    /**
     * JSONValidatorManager constructor.
     * @param RedisManager $redisManager
     */
    public function __construct(RedisManager $redisManager)
    {
        $this->redisManager = $redisManager;
    }

    /**
     * Returns decoded json if valid and null if not valid
     *
     * @param $json
     * @param $type
     *
     * @return mixed|null
     */
    public function getValidatedJsonFromRequest($json, $type)
    {
        $result = false;
        $data = json_decode($json, true);

        if (json_last_error() == JSON_ERROR_NONE) {
            switch ($type) {
                case $this->USER_REGISTER:
                    $result = $this->isValidUserRegisterRequest($data);
                    break;
                case $this->USER_LOGIN:
                    $result = $this->isValidUserLoginRequest($data);
                    break;
                case $this->DONE_QUEST_PROVE:
                    $result = $this->isValidDoneQuestProveRequest($data);
                    break;
                case $this->REVIEW_RESULT:
                    $result = $this->isValidReviewResultRequest($data);
                    break;
                case $this->SKILLS_REQUEST:
                    $result = $this->isValidSkillsRequest($data);
                    break;
                case $this->SKILL_ACTION:
                    $result = $this->isValidSkillActionRequest($data);
                    break;
                case $this->TAKE_QUEST:
                    $result = $this->isValidTakeQuestRequest($data);
                    break;
                case $this->GET_QUEST_REWARD:
                    $result = $this->isValidGetQuestRewardRequest($data);
                    break;
                case $this->ARENA_BATTLE_INIT:
                    $result = $this->isValidArenaInitRequest($data);
                    break;
                case $this->TOURNAMENT_BATTLE_INIT:
                    $result = $this->isValidTournamentRequest($data);
                    break;
                case $this->SELECT_SKILLS_REQUEST:
                    $result = $this->isValidSelectSkillsRequest($data);
                    break;
                case $this->BUY_SHOP_REQUEST:
                    $result = $this->isValidBuyShopRequest($data);
                    break;
                case $this->CHANGE_AVATAR_REQUEST:
                    $result = $this->isValidChangeAvatarRequest($data);
                    break;
                case $this->ADVENTURE_BATTLE_INIT:
                    $result = $this->isValidBattleInitRequest($data);
                    break;
                case $this->ADD_FRIEND_REQUEST:
                    $result = $this->isValidAddFriendRequest($data);
                    break;
                case $this->FRIEND_ACTION_REQUEST:
                    $result = $this->isValidFriendActionRequest($data);
                    break;
                case $this->TAKE_DUEL_QUEST:
                    $result = $this->isValidTakeDuelQuestActionRequest($data);
                    break;
                case $this->DONE_DUEL_PROVE:
                    $result = $this->isValidDoneQuestDuelProveRequest($data);
                    break;
                case $this->REVIEW_DUEL_RESULT:
                    $result = $this->isValidReviewResultRequest($data);
                    break;
                case $this->LOCATION_INFO_REQUEST:
                    $result = $this->isValidLocationInfoRequest($data);
                    break;
                default:
                    $result = $this->isValidRequest($data);
            }
        }
        
        if ($result) {
            return $data;
        } else {
            return null;
        }
    }

    /**
     * Returns user_id by token
     *
     * @param $data
     *
     * @return string
     */
    public function getUserIdFromData($data)
    {
        $userId = $this->getRedisManager()->getUserIdByToken($data[$this->TOKEN]);
        return $userId;
    }

    public function isValidTournamentRequest($data)
    {
        $result = false;
        if (isset($data[$this->PLAYER_1]) && is_string($data[$this->PLAYER_1]) &&
            strlen($data[$this->PLAYER_1]) == self::TOKEN_LENGTH &&
            isset($data[$this->PLAYER_2]) && is_string($data[$this->PLAYER_2]) &&
            strlen($data[$this->PLAYER_2]) == self::TOKEN_LENGTH) {
            $result = true;
        }
        return $result;
    }
    
    private function isValidLocationInfoRequest($data)
    {
        $result = false;
        if (isset($data[$this->TOKEN]) && is_string($data[$this->TOKEN]) &&
            strlen($data[$this->TOKEN]) == self::TOKEN_LENGTH &&
            isset($data[$this->LOCATION_ID]) && is_int($data[$this->LOCATION_ID])) {
            $result = true;
        }
        return $result;
    }

    /**
     * Сhecks the validity of the USER_REGISTER request
     *
     * @param $data
     *
     * @return bool
     */
    private function isValidUserRegisterRequest($data)
    {
        $result = false;
        if (isset($data[$this->EMAIL]) && is_string($data[$this->EMAIL]) &&
            isset($data[$this->USERNAME]) && is_string($data[$this->USERNAME]) &&
            isset($data[$this->PASSWORD]) && is_string($data[$this->PASSWORD]) &&
            isset($data[$this->CHARACTER][$this->CHAR_CLASS_ID]) &&
            is_int($data[$this->CHARACTER][$this->CHAR_CLASS_ID]) &&
            isset($data[$this->CHARACTER][$this->CHAR_NAME]) &&
            is_string($data[$this->CHARACTER][$this->CHAR_NAME]) &&
            isset($data[$this->CHARACTER][$this->AVATAR_ID]) &&
            is_int($data[$this->CHARACTER][$this->AVATAR_ID])) {
            $result = true;
        }
        return $result;
    }

    /**
     * Сhecks the validity of the FRIEND_ACTION_REQUEST request
     *
     * @param $data
     *
     * @return bool
     */
    private function isValidFriendActionRequest($data)
    {
        $result = false;
        if (isset($data[$this->TOKEN]) && is_string($data[$this->TOKEN]) &&
            strlen($data[$this->TOKEN]) == self::TOKEN_LENGTH &&
            isset($data[$this->FRIEND_ID]) && is_int($data[$this->FRIEND_ID])) {
            $result = true;
        }
        return $result;
    }

    /**
     * Сhecks the validity of the BUY_SHOP request
     *
     * @param $data
     *
     * @return bool
     */
    private function isValidBuyShopRequest($data)
    {
        $result = false;
        if (isset($data[$this->TOKEN]) && is_string($data[$this->TOKEN]) &&
            strlen($data[$this->TOKEN]) == self::TOKEN_LENGTH &&
            isset($data[$this->UNIT_ID]) && is_int($data[$this->UNIT_ID])) {
            $result = true;
        }
        return $result;
    }

    /**
     * Сhecks the validity of the TAKE_DUEL_QUEST request
     *
     * @param $data
     *
     * @return bool
     */
    private function isValidTakeDuelQuestActionRequest($data)
    {
        $result = false;
        if (isset($data[$this->TOKEN]) && is_string($data[$this->TOKEN]) &&
            strlen($data[$this->TOKEN]) == self::TOKEN_LENGTH &&
            isset($data[$this->QUEST_ID]) && is_int($data[$this->QUEST_ID]) &&
            isset($data[$this->FRIEND_ID]) && is_int($data[$this->FRIEND_ID])) {
            $result = true;
        }
        return $result;
    }

    /**
     * Сhecks the validity of the ADD_FRIEND_REQUEST request
     *
     * @param $data
     *
     * @return bool
     */
    private function isValidAddFriendRequest($data)
    {
        $result = false;
        if (isset($data[$this->TOKEN]) && is_string($data[$this->TOKEN]) &&
            strlen($data[$this->TOKEN]) == self::TOKEN_LENGTH &&
            isset($data[$this->USERNAME]) && is_string($data[$this->USERNAME])) {
            $result = true;
        }
        return $result;
    }

    /**
     * Сhecks the validity of the SELECT_SKILLS request
     *
     * @param $data
     *
     * @return bool
     */
    private function isValidSelectSkillsRequest($data)
    {
        $result = false;
        if (isset($data[$this->TOKEN]) && is_string($data[$this->TOKEN]) &&
            strlen($data[$this->TOKEN]) == self::TOKEN_LENGTH &&
            isset($data[$this->CHAR_ID]) && is_int($data[$this->CHAR_ID]) &&
            isset($data[$this->SKILLS]) && is_array($data[$this->SKILLS])) {
            $result = true;
        }
        return $result;
    }

    /**
     * Сhecks the validity of the ARENA_INIT request
     *
     * @param $data
     *
     * @return bool
     */
    private function isValidArenaInitRequest($data)
    {
        $result = false;
        if (isset($data[$this->PLAYER_1]) && is_string($data[$this->PLAYER_1]) &&
            strlen($data[$this->PLAYER_1]) == self::TOKEN_LENGTH &&
            isset($data[$this->PLAYER_2]) && is_string($data[$this->PLAYER_2]) &&
            strlen($data[$this->PLAYER_2]) == self::TOKEN_LENGTH &&
            isset($data[$this->PLAYER_1_SKILLS]) && is_array($data[$this->PLAYER_1_SKILLS]) &&
            count($data[$this->PLAYER_1_SKILLS]) == $this->ARENA_SKILLS_NUMBER &&
            isset($data[$this->PLAYER_2_SKILLS]) && is_array($data[$this->PLAYER_2_SKILLS]) &&
            count($data[$this->PLAYER_2_SKILLS]) == $this->ARENA_SKILLS_NUMBER) {
            $result = true;
        }
        return $result;
    }

    /**
     * Сhecks the validity of the ADVENTURE_BATTLE_INIT request
     *
     * @param $data
     *
     * @return bool
     */
    private function isValidBattleInitRequest($data)
    {
        $result = false;
        if (isset($data[$this->TOKEN]) && is_string($data[$this->TOKEN]) &&
            strlen($data[$this->TOKEN]) == self::TOKEN_LENGTH &&
            isset($data[$this->STAGE_ID]) && is_numeric($data[$this->STAGE_ID])) {
            $result = true;
        }
        return $result;
    }

    /**
     * Сhecks the validity of the TAKE_QUEST request
     *
     * @param $data
     *
     * @return bool
     */
    private function isValidTakeQuestRequest($data)
    {
        $result = false;
        if (isset($data[$this->TOKEN]) && is_string($data[$this->TOKEN]) &&
            strlen($data[$this->TOKEN]) == self::TOKEN_LENGTH &&
            isset($data[$this->QUEST_ID]) && is_int($data[$this->QUEST_ID])) {
            $result = true;
        }
        return $result;
    }

    /**
     * Сhecks the validity of the GET_QUEST_REWARD request
     *
     * @param $data
     *
     * @return bool
     */
    private function isValidGetQuestRewardRequest($data)
    {
        $result = false;
        if (isset($data[$this->TOKEN]) && is_string($data[$this->TOKEN]) &&
            strlen($data[$this->TOKEN]) == self::TOKEN_LENGTH &&
            isset($data[$this->QUEST_ID]) && is_int($data[$this->QUEST_ID])) {
            $result = true;
        }
        return $result;
    }

    /**
     * Сhecks the validity of the SKILL_ACTION request
     *
     * @param $data
     *
     * @return bool
     */
    private function isValidSkillActionRequest($data)
    {
        $result = false;
        if (isset($data[$this->TOKEN]) && is_string($data[$this->TOKEN]) &&
            strlen($data[$this->TOKEN]) == self::TOKEN_LENGTH &&
            isset($data[$this->SKILL_ID])&& is_int($data[$this->SKILL_ID])) {
            $result = true;
        }
        return $result;
    }

    /**
     * Сhecks the validity of the SKILLS_REQUEST request
     *
     * @param $data
     *
     * @return bool
     */
    private function isValidSkillsRequest($data)
    {
        $result = false;
        if (isset($data[$this->TOKEN]) && is_string($data[$this->TOKEN]) &&
            strlen($data[$this->TOKEN]) == self::TOKEN_LENGTH &&
            isset($data[$this->CHAR_ID]) && is_int($data[$this->CHAR_ID])) {
            $result = true;
        }
        return $result;
    }

    /**
     * Сhecks the validity of the REVIEW_RESULT request
     *
     * @param $data
     *
     * @return bool
     */
    private function isValidReviewResultRequest($data)
    {
        $result = false;
        if (isset($data[$this->TOKEN]) && is_string($data[$this->TOKEN]) &&
            strlen($data[$this->TOKEN]) == self::TOKEN_LENGTH &&
            isset($data[$this->RESULT]) && is_bool($data[$this->RESULT])) {
            $result = true;
        }
        return $result;
    }

    /**
     * Сhecks the validity of the DONE_QUEST_PROVE request
     *
     * @param $data
     *
     * @return bool
     */
    private function isValidDoneQuestProveRequest($data)
    {
        $result = false;
        if (isset($data[$this->TOKEN]) && is_string($data[$this->TOKEN]) &&
            strlen($data[$this->TOKEN]) == self::TOKEN_LENGTH &&
            isset($data[$this->QUEST_ID]) && is_int($data[$this->QUEST_ID])) {
            $result = true;
        }
        return $result;
    }

    /**
     * Сhecks the validity of the DONE_DUEL_PROVE request
     *
     * @param $data
     *
     * @return bool
     */
    private function isValidDoneQuestDuelProveRequest($data)
    {
        $result = false;
        if (isset($data[$this->TOKEN]) && is_string($data[$this->TOKEN]) &&
            strlen($data[$this->TOKEN]) == self::TOKEN_LENGTH &&
            isset($data[$this->QUEST_ID]) && is_int($data[$this->QUEST_ID]) &&
            isset($data[$this->FRIEND_ID]) && is_int($data[$this->FRIEND_ID])) {
            $result = true;
        }
        return $result;
    }
    
    /**
     * Сhecks the validity of the CHANGE_AVATAR_REQUEST request
     *
     * @param $data
     *
     * @return bool
     */
    private function isValidChangeAvatarRequest($data)
    {
        $result = false;
        if (isset($data[$this->TOKEN]) && is_string($data[$this->TOKEN]) &&
            strlen($data[$this->TOKEN]) == self::TOKEN_LENGTH &&
            isset($data[$this->AVATAR_ID]) && is_int($data[$this->AVATAR_ID])) {
            $result = true;
        }
        return $result;
    }

    /**
     * Сhecks the validity of the USER_LOGIN request
     *
     * @param $data
     *
     * @return bool
     */
    private function isValidUserLoginRequest($data)
    {
        $result = false;
        if (isset($data[$this->EMAIL]) && is_string($data[$this->EMAIL]) &&
            isset($data[$this->PASSWORD]) && is_string($data[$this->PASSWORD])) {
            $result = true;
        }
        return $result;
    }

    /**
     * Сhecks the validity of the request
     *
     * @param $data
     *
     * @return bool
     */
    private function isValidRequest($data)
    {
        $result = false;
        if (isset($data[$this->TOKEN]) && is_string($data[$this->TOKEN]) &&
            strlen($data[$this->TOKEN]) == self::TOKEN_LENGTH) {
            $result = true;
        }
        return $result;
    }
}
