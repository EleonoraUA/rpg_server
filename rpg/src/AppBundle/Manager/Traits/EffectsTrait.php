<?php

namespace AppBundle\Manager\Traits;

trait EffectsTrait
{
    public $INCREASE_ATTACK_EFFECT = 1;
    public $INCREASE_DEFENCE_EFFECT = 2;
    public $ATTACK_BREAK_EFFECT = 3;
    public $DEFENCE_BREAK_EFFECT = 4;
    public $CONTINUOUS_DAMAGE_EFFECT = 5;
    public $HEALING_EFFECT = 6;
    public $STUN_EFFECT = 7;
    public $PROTECTION_EFFECT = 8;
}
