<?php

namespace AppBundle\Manager\Traits;

trait QuestInfoFieldsTrait
{
    public $PROVE_IMAGE_1 = 'prove_image_1';
    public $PROVE_IMAGE_2 = 'prove_image_2';
    public $QUEST_ID = 'quest_id';
    public $USER_TO_QUEST = 'user_to_quest';
    public $RESULT = 'result';
    public $DUEL_QUESTS = 'quests';
    public $QUEST_DUEL_REQUEST = 'quest_duel_request';
    public $PROOF_QUEST_DUEL_IMAGE_URL = '/uploads/duels/';
    public $PROOF_QUEST_IMAGE_URL = '/uploads/proofs/';
    public $FRIEND_REQUEST_SENDER = 'sender';
    public $FRIEND_REQUEST_ACCEPTER = 'accepter';
    public $STATE= 'state';
    public $GOT_REWARD = 'got_reward';
    public $DAYS_LEFT = 'days_left';
    public $JPG_EXTENSION = '.jpg';
    public $ACTIVE_DUELS = 'active_duels';
    public $SENDER = 1;
    public $ACCEPTER = 2;
    public $REVIEWER = 'reviewer';
    public $REVIEW_COUNT = 'review_count';
    public $REVIEWED_QUESTS = 'reviewedQuests';
    public $REVIEW_COUNT_NUMBER = 3;
    public $GOLD_REVIEW_REWARD = 10;
    public $CRYSTALS_REVIEW_REWARD = 0;
    public $NO_SKILLS_REVIEW_REWARD = 0;
    public $FILESIZE = 10000000;
    public $QUEST = 'quest';
}
