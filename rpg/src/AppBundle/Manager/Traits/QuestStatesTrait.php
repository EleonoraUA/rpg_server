<?php

namespace AppBundle\Manager\Traits;

trait QuestStatesTrait
{
    public $EMPTY_STATE = 0;
    public $IN_PROGRESS = 1;
    public $IS_DONE = 2;
    public $REVIEWED_TRUE = 3;
    public $REVIEWED_FALSE = 4;
    public $SKIPPED = 5;
    public $TO_REVIEW = 6;
    public $OUTCOMING_DUEL_REQUEST = 7;
    public $INCOMING_DUEL_REQUEST = 8;
    public $SENDER_WIN = 9;
    public $ACCEPTER_WIN = 10;
}
