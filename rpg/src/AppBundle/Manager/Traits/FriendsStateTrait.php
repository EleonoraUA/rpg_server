<?php

namespace AppBundle\Manager\Traits;

trait FriendsStateTrait
{
    public $NOT_REVIEWED_REQUEST = 0;
    public $CONFIRMED_FRIENDS_REQUEST = 1;
    public $OUTCOMING_REQUEST = 0;
    public $INCOMING_REQUEST = 1;
    public $CONFIRMED_FRIENDS = 2;
}
