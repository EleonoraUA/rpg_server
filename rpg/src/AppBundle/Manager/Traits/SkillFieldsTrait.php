<?php

namespace AppBundle\Manager\Traits;

trait SkillFieldsTrait
{
    public $SKILLS_CONDITION = 'skills_condition';
    public $SKILLS_DAMAGE = 'skills_damage';
    public $MULTIPLIER = 'multiplier';
    public $SKILL = 'skill';
    public $SKILL_ID = 'skill_id';
    public $IS_SELECTED = 'isSelected';
    public $IS_SELECTED_SKILL = 'is_selected';
    public $EFFECTS = 'effects';
    public $EFFECT_ID = 'effect_id';
    public $EFFECT_MULTIPLIER = 1.5;
    public $CONTINUOUS_DAMAGE_MULTIPLIER = 0.10;
    public $HEALING_MULTIPLIER = 0.15;
    public $MAX_EFFECTS_NUMBER = 6;
}
