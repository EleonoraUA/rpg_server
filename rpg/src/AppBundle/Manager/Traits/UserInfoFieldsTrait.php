<?php

namespace AppBundle\Manager\Traits;

trait UserInfoFieldsTrait
{
    public $USER = 'user';
    public $ID = 'id';
    public $NAME = 'name';
    public $USERID = 'userId';
    public $USER_ID = 'user_id';
    public $CLASS_ID = 'class_id';
    public $TOKEN = 'token';
    public $USERNAME = 'username';
    public $EMAIL = 'email';
    public $PASSWORD = 'password';
    public $AVATAR_ID = 'avatar_id';
    public $STATUS = 'status';
    public $ACCOUNT = 'account';
    public $CHAR_ID = 'char_id';
    public $PERSONAGE = 'personage';
    public $CHAR = 'char';
    public $CHARACTER = 'character';
    public $CHARACTERS = 'characters';
    public $CHAR_NAME = 'name';
    public $CHAR_HP = 'hp';
    public $CHAR_ATK = 'attack';
    public $CHAR_CLASS = 'class';
    public $CHAR_CLASS_ID = 'class_id';
    public $LVL = 'lvl';
    public $PERSONAGE_BAG_SIZE = 'bag_size';
    public $PERSONAGE_ACTIVE_SKILLS_BAG_SIZE = 'active_skills_bag_size';
    public $CURRENT_LVL = 'current_level';
    public $EXP = 'exp';
    public $CURRENT_EXP = 'current_exp';
    public $MAX_EXP = 'max_exp';
    public $RESOURCES = 'resources';
    public $REWARD = 'reward';
    public $GOLD = 'gold';
    public $CRYSTALS = 'crystals';
    public $IS_ONLINE = 'is_online';
    public $FRIENDS = 'friends';
    public $FRIEND_ID = 'friend_id';
    public $QUESTS = 'quests';
    public $SKILLS = 'skills';
    public $CLASSES = 'classes';
    public $DESCRIPTION = 'description';
    public $MIN_AVATAR_ID = 1;
    public $MAX_AVATAR_ID = 10;
    public $MAX_LVL = 10;
}
