<?php

namespace AppBundle\Manager\Traits;

trait ShopFieldsTrait
{
    public $UNIT_NAME = 'unit_name';
    public $UNIT_ID = 'unit_id';
    public $UNIT_COUNT = 'unit_count';
    public $UNIT_TYPE = 'unit_type';
    public $UNIT_PRICE = 'unit_price';
    public $PRICE_TYPE = 'price_type';
    public $SHOP_UNITS = 'shop_units';
    public $SHOP_UNIT = 'shopUnit';
    public $BAG_SLOTS = 'bag_slots';
    public $ACTIVE_SLOT = 'active_slot';
}
