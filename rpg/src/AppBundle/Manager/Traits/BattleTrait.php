<?php

namespace AppBundle\Manager\Traits;

trait BattleTrait
{
    public $BATTLE_TYPE = 'BATTLE_TYPE';
    public $LOCATION_INFO = 'location_info';
    public $LOCATIONS = 'locations';
    public $LOCATION_ID = 'location_id';
    public $LOCATION = 'location';
    public $MONSTER = 'monster';
    public $ARENA = 'arena';
    public $TOURNAMENT = 'tournament';
    public $REQUIRED_LVL = 'required_lvl';
    public $CURRENT_TURN = 'is_current_turn';
    public $START_TIME = 'start_time';
    public $COOLDOWN = 'cooldown';
    public $CURRENT_HP = 'current_hp';
    public $ATTACK = 'atk';
    public $MAX_HP = 'max_hp';
    public $STAGE_ID = 'stage_id';
    public $STAGE = 'stage';
    public $CURRENT_CD = 'cur_cd';
    public $DAMAGE = 'damage';
    public $DURATION = 'duration';
    public $OPPONENT = 'opponent';
    public $DID_WIN = 'did_win';
    public $ONLINE_USERS = 'online_users';
    public $USER_TOKENS = 'user_tokens';
    public $BATTLE_CONDITION_RESPONSE = 'battle_condition';
    public $BATTLE_PLACE_ID = 'battle_place_id';
    public $PLAYER_INFO = 'player_info';
    public $OPPONENT_INFO = 'opponent_info';
    public $OPPONENT_NAME = 'name';
    public $IS_CURRENT_TURN = 'is_current_turn';
    public $WIN_COUNT = 'win_count';
    public $PLAYER_1 = 'player_1';
    public $PLAYER_2 = 'player_2';
    public $PLAYER_1_SKILLS = 'player_1_skills';
    public $PLAYER_2_SKILLS = 'player_2_skills';
    public $TYPE = 'type';
    public $TIME = 'time';
    public $IS_NOT_AVAILABLE = 2;
    public $ARENA_PRICE = 5;
    public $ARENA_SKILLS_NUMBER = 5;
    public $NUMBER_OF_SKILLS_FOR_ARENA = 15;
    public $MIN_GOLD_REWARD = 2;
    public $MAX_GOLD_REWARD = 8;
    public $BATTLE_TIME = 30;
}
