<?php
/**
 * Created by PhpStorm.
 * User: helenka
 * Date: 12/12/16
 * Time: 10:31 AM
 */

namespace AppBundle\Manager;

use AppBundle\Entity\QuestDuelRequest;
use AppBundle\Entity\QuestDuelReview;
use AppBundle\Manager\Traits\BattleTrait;
use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Entity\QuestReward;
use AppBundle\Manager\Traits\QuestInfoFieldsTrait;
use AppBundle\Manager\Traits\QuestStatesTrait;
use AppBundle\Manager\Traits\SkillFieldsTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Doctrine\DBAL\Types\Type;
use phpDocumentor\Reflection\Types\Array_;

class ProofQuestDuelManager extends Manager
{
    use QuestStatesTrait;
    use ErrorCodesTrait;
    use QuestInfoFieldsTrait;
    use UserInfoFieldsTrait;
    use SkillFieldsTrait;
    use BattleTrait;
    /**
     * @param $file
     * @param $userId
     * @param QuestDuelRequest $requestQuestDuel
     *
     * @return array
     */
    public function uploadQuestDuelProofImage($file, $userId, QuestDuelRequest $requestQuestDuel)
    {
        $extension = $file->getClientOriginalExtension();
        $requestId = $requestQuestDuel->getId();

        if ('.'.$extension === $this->JPG_EXTENSION) {
            $filename = $requestId.$this->JPG_EXTENSION;
        } else {
            return array(
                $this->STATUS => $this->NOT_A_PICTURE
            );
        }

        if (filesize($file) > $this->FILESIZE) {
            return array(
                $this->STATUS => $this->TOO_BIG_PICTURE
            );
        }

        $dirname = dirname(dirname(dirname(__DIR__))).'/web'.$this->PROOF_QUEST_DUEL_IMAGE_URL.$userId;
        $shouldUnlinkFiles = glob($dirname.'/'.$requestId.'.*', GLOB_BRACE);

        foreach ($shouldUnlinkFiles as $shouldUnlink) {
            $exist = file_exists($shouldUnlink);
            if ($exist) {
                unlink($shouldUnlink);
            }
        }

        $opponentId = $requestQuestDuel->getSender()->getId();
        if ($opponentId == $userId) {
            $opponentId = $requestQuestDuel->getAccepter()->getId();
        }

        $opponentDirname = dirname(dirname(dirname(__DIR__))).'/web'.$this->PROOF_QUEST_DUEL_IMAGE_URL.$opponentId;
        $opponentShouldUnlinkFiles = glob($opponentDirname.'/'.$requestId.'.*', GLOB_BRACE);
        $hasProof = false;
        foreach ($opponentShouldUnlinkFiles as $shouldUnlink) {
            $exist = file_exists($shouldUnlink);
            if ($exist) {
                $hasProof = true;
            }
        }

        $file->move($dirname, $filename);
        if (!$requestQuestDuel->getState($this->IN_PROGRESS)) {
            return array($this->STATUS => $this->QUEST_DUEL_IS_NOT_ACCEPTED);
        }
        if ($hasProof) {
            $requestQuestDuel->setState($this->IS_DONE);
        }
        $this->getEm()->persist($requestQuestDuel);
        $this->getEm()->flush();

        return array($this->STATUS => $this->STATUS_OK);
    }


    /**
     *
     * @param $data
     *
     * @return array
     */
    public function getQuestDuelToProveFromData($data)
    {
        global $kernel;
        $validatedResponse = $kernel
            ->getContainer()
            ->get('app.manager.validator')
            ->getRequestIdsFromData($data);

        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return $validatedResponse;
        }

        $userId = $validatedResponse[$this->USER_ID];
        $questId = $validatedResponse[$this->QUEST_ID];
        $friendId = $validatedResponse[$this->FRIEND_ID];

        $requestQuestDuel = $this
            ->getEm()
            ->getRepository('AppBundle:QuestDuelRequest')
            ->findQuestDuelToProve($userId, $friendId, $questId);

        if ($requestQuestDuel === null) {
            return array(
                $this->STATUS => $this->QUEST_DUEL_REQUEST_NOT_FOUND
            );
        }
        return array(
            $this->STATUS => $this->STATUS_OK,
            $this->USER_ID => $userId,
            $this->QUEST_DUEL_REQUEST => $requestQuestDuel
        );
    }


    /**
     *
     *
     * @param $userId
     *
     * @return mixed
     */
    public function getQuestDuelToReview($userId)
    {
        $questOnReview = $this->getEm()
            ->getRepository('AppBundle:QuestDuelReview')
            ->findOneBy(
                array(
                    $this-> REVIEWER => $userId,
                    $this->RESULT => null
                )
            );

        if (is_null($questOnReview)) {
            $reviewedQuests = $this->getEm()
                ->getRepository('AppBundle:QuestDuelReview')
                ->findReviewedQuestsByUserId($userId);

            if (empty($reviewedQuests)) {
                $parameters = array(
                    $this->STATE => $this->IS_DONE,
                    $this->REVIEW_COUNT => $this->REVIEW_COUNT_NUMBER,
                    $this->USERID => $userId
                );
                $questsToReview = $this
                    ->getEm()
                    ->getRepository('AppBundle:QuestDuelRequest')
                    ->findQuestsToReviewByParameters($parameters);
            } else {
                foreach ($reviewedQuests as $reviewedQuest) {
                    $reviewedQuestId[] = $reviewedQuest->getQuestDuelRequest()->getId();
                }

                $parameters = array(
                    $this->REVIEWED_QUESTS => $reviewedQuestId,
                    $this->STATE => $this->IS_DONE,
                    $this->REVIEW_COUNT => $this->REVIEW_COUNT_NUMBER,
                    $this->USERID => $userId
                );
                $questsToReview = $this
                    ->getEm()
                    ->getRepository('AppBundle:QuestDuelRequest')
                    ->findQuestsToReviewByParameters($parameters);
            }

            if (empty($questsToReview)) {
                return array(
                    $this-> STATUS => $this->STATUS_OK,
                    $this->QUESTS => array()
                );
            }
        } else {
            $quest = $questOnReview->getQuestDuelRequest();
            $senderId = $questOnReview->getQuestDuelRequest()->getSender()->getId();
            $accepterId = $questOnReview->getQuestDuelRequest()->getAccepter()->getId();
            $senderImageURL = $this->PROOF_QUEST_DUEL_IMAGE_URL.$senderId.'/'.$quest->getId().$this->JPG_EXTENSION;
            $accepterImageURL = $this->PROOF_QUEST_DUEL_IMAGE_URL.$accepterId.'/'.$quest->getId().$this->JPG_EXTENSION;
        }

        if (!empty($questsToReview) && isset($questsToReview)) {
            $quest = $questsToReview[0];
            $user = $this->getEm()->getRepository('AppBundle:User')->findOneBy(array($this->ID => $userId));
            $review = new QuestDuelReview();
            $review->setReviewer($user);
            $review->setResult(null);
            $review->setQuestDuelRequest($questsToReview[0]);
            $this->getEm()->persist($review);

            $questsToReview[0]->updateReviewCount();
            $this->getEm()->persist($questsToReview[0]);
            $this->getEm()->flush();

            $senderId = $questsToReview[0]->getSender()->getId();
            $accepterId  = $questsToReview[0]->getAccepter()->getId();
            $senderImageURL = $this->PROOF_QUEST_DUEL_IMAGE_URL.$senderId.'/'.$quest->getId().$this->JPG_EXTENSION;
            $accepterImageURL = $this->PROOF_QUEST_DUEL_IMAGE_URL.$accepterId.'/'.$quest->getId().$this->JPG_EXTENSION;
        }

        $response[$this->QUESTS][] = array(
            $this->QUEST_ID => $quest->getQuest()->getId(),
            $this->NAME => $quest->getQuest()->getName(),
            $this->DESCRIPTION => $quest->getQuest()->getDescription(),
            $this->STATE => $this->TO_REVIEW,
            $this->TYPE => 1,
            $this->PROVE_IMAGE_1 => $senderImageURL,
            $this->PROVE_IMAGE_2 => $accepterImageURL,
            $this->REWARD => array(
                $this->GOLD => $this->GOLD_REVIEW_REWARD,
                $this->CRYSTALS => $this->CRYSTALS_REVIEW_REWARD,
                $this->SKILL_ID => $this->NO_SKILLS_REVIEW_REWARD
            )
        );

        $response[$this->STATUS] = $this->STATUS_OK;
        return $response;
    }


    /**
     * @param $userId
     * @param $result
     *
     * @return array
     */
    public function checkQuestDuelReviewResult($userId, $result)
    {
        global $kernel;
        $questOnReview = $this->getEm()
            ->getRepository('AppBundle:QuestDuelReview')
            ->findOneBy(
                array(
                    $this->REVIEWER => $userId,
                    $this->RESULT => null
                )
            );
        if (!$questOnReview) {
            return array($this->STATUS => $this->NO_QUEST_FOR_VOTING);
        }
        $questOnReview->setResult($result);
        $this->getEm()->persist($questOnReview);
        $this->getEm()->flush();
        $this->checkQuestWasConfirmed($questOnReview->getQuestDuelRequest());
        $kernel->getContainer()
            ->get('app.manager.account')
            ->sendMoneyReward(
                $userId,
                new QuestReward($this->GOLD_REVIEW_REWARD, $this->CRYSTALS_REVIEW_REWARD, null)
            );
        return array(
            $this->STATUS => $this->STATUS_OK
        );
    }

    /**
     * @param QuestDuelRequest $questDuelRequest
     *
     * @return bool
     */
    public function checkQuestWasConfirmed(QuestDuelRequest $questDuelRequest)
    {
        global $kernel;
        $reviews = $this
            ->getEm()
            ->getRepository('AppBundle:QuestDuelReview')
            ->findReviewResultByQuestDuelRequest($questDuelRequest->getId());
        $response = false;
        if (count($reviews) == $this->REVIEW_COUNT_NUMBER) {
            $response = true;
            $newState = $this->getNewStateOfConfirmedQuest($reviews);
            $wonUser = $questDuelRequest->getAccepter()->getId();
            if ($newState === $this->SENDER_WIN) {
                $wonUser = $questDuelRequest->getSender()->getId();
            }
            $kernel->getContainer()
                ->get('app.manager.account')
                ->sendMoneyReward($wonUser, $questDuelRequest->getQuest()->getReward());
            if ($questDuelRequest->getQuest()->getReward()->getSkillId() == 0) {
                $questDuelRequest->setGotReward(true);
            }
            $questDuelRequest->setState($newState);
            $this->getEm()->persist($questDuelRequest);
            $this->getEm()->flush();
        }
        return $response;
    }

    /**
     * @param $reviews
     *
     * @return mixed
     */
    private function getNewStateOfConfirmedQuest($reviews)
    {
        $senderCount = 0;
        $accepterCount = 0;
        foreach ($reviews as $review) {
            if ($review->getResult() === false) {
                $senderCount++;
            } else {
                $accepterCount++;
            }
        }
        if ($senderCount > $accepterCount) {
            $newState = $this->SENDER_WIN;
        } else {
            $newState = $this->ACCEPTER_WIN;
        }

        return $newState;
    }
}
