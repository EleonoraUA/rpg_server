<?php
/**
 * Class File FetchQuestDuelManager
 *
 * PHP version 5.5
 *
 * @package AppBundle\Manager
 */
namespace AppBundle\Manager;

use AppBundle\Entity\QuestDuelRequest;
use AppBundle\Entity\QuestReward;
use AppBundle\Manager\Traits\BattleTrait;
use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\QuestInfoFieldsTrait;
use AppBundle\Manager\Traits\QuestStatesTrait;
use AppBundle\Manager\Traits\SkillFieldsTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;

/**
 * Class FetchQuestDuelManager
 *
 * @package AppBundle\Manager
 */
class FetchQuestDuelManager extends Manager
{
    use QuestStatesTrait;
    use ErrorCodesTrait;
    use UserInfoFieldsTrait;
    use BattleTrait;
    use SkillFieldsTrait;
    use QuestInfoFieldsTrait;

    /**
     * Find all incoming duel quests
     *
     * @param $data
     *
     * @return array
     */
    public function getIncomingDuelQuests($data)
    {
        global $kernel;
        $validatedResponse = $kernel
            ->getContainer()
            ->get('app.manager.validator')
            ->getUserIdFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return $validatedResponse;
        }
        $userId = $validatedResponse[$this->USER_ID];
        $duelQuests = $this->getEm()
            ->getRepository('AppBundle:QuestDuelRequest')
            ->findIncomingDuelRequestsByUserId($userId);
        if (empty($duelQuests)) {
            return array(
                $this->STATUS => $this->STATUS_OK,
                $this->DUEL_QUESTS => array()
            );
        }
        $duelQuestsResponse = array();
        foreach ($duelQuests as $duelQuest) {
            $friendState = $this->findUserState($duelQuest, $userId);
            $friendId = $friendState[$this->USER]->getId();
            $friendName = $friendState[$this->USER]->getUsername();
            $duelQuestsResponse[] = array(
                $this->FRIEND_ID => $friendId,
                $this->USERNAME => $friendName,
                $this->QUEST_ID => $duelQuest->getQuest()->getId()
            );
        }
        return array(
            $this->STATUS => $this->STATUS_OK,
            $this->DUEL_QUESTS => $duelQuestsResponse
        );
    }

    /**
     * Find all in progress duel quests
     *
     * @param $data
     *
     * @return array
     */
    public function getInProgressDuelQuests($data)
    {
        global $kernel;
        $validatedResponse = $kernel
            ->getContainer()
            ->get('app.manager.validator')
            ->getUserIdFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return $validatedResponse;
        }
        $userId = $validatedResponse[$this->USER_ID];
        $duelQuests = $this->getEm()
            ->getRepository('AppBundle:QuestDuelRequest')
            ->findInProgressQuestDuelByUserId($userId);
        if (empty($duelQuests)) {
            return array(
                $this->STATUS => $this->STATUS_OK,
                $this->DUEL_QUESTS => array()
            );
        }
        $duelQuestsResponse = array();
        foreach ($duelQuests as $duelQuest) {
            $friendState = $this->findUserState($duelQuest, $userId);
            $friendId = $friendState[$this->USER]->getId();
            $friendName = $friendState[$this->USER]->getUsername();
            $quest = $duelQuest->getQuest();
            $state = $duelQuest->getState();
            $senderImageURL = "";
            $accepterImageURL = "";
            if (file_exists(dirname(dirname(dirname(dirname(__FILE__)))).'/web'.$this->PROOF_QUEST_DUEL_IMAGE_URL .
                $userId . '/' . $duelQuest->getId() . $this->JPG_EXTENSION)) {
                $senderImageURL = $this->PROOF_QUEST_DUEL_IMAGE_URL .
                    $userId . '/' . $duelQuest->getId() . $this->JPG_EXTENSION;
            }
            if ($state == $this->IS_DONE) {
                $senderImageURL = $this->PROOF_QUEST_DUEL_IMAGE_URL .
                    $userId . '/' . $duelQuest->getId() . $this->JPG_EXTENSION;
                $accepterImageURL = $this->PROOF_QUEST_DUEL_IMAGE_URL .
                    $friendId . '/' . $duelQuest->getId() . $this->JPG_EXTENSION;
            }
            $duelQuestsResponse[] = $this->createDuelQuestsResponse(
                false,
                $friendId,
                $friendName,
                $duelQuest->getDaysLeft(),
                $quest->getId(),
                $quest->getName(),
                $quest->getDescription(),
                $state,
                $senderImageURL,
                $accepterImageURL,
                0,
                $quest->getReward()
            );
        }
        return array(
            $this->STATUS => $this->STATUS_OK,
            $this->DUEL_QUESTS => $duelQuestsResponse
        );
    }

    /**
     * Find all confirmed duel quests
     *
     * @param $data
     *
     * @return array
     */
    public function getConfirmedDuelQuests($data)
    {
        global $kernel;
        $validatedResponse = $kernel
            ->getContainer()
            ->get('app.manager.validator')
            ->getUserIdFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return $validatedResponse;
        }

        $userId = $validatedResponse[$this->USER_ID];
        $duelQuests = $this->getEm()
            ->getRepository('AppBundle:QuestDuelRequest')
            ->findConfirmedQuestDuelByUserId($userId);
        if (empty($duelQuests)) {
            return array(
                $this->STATUS => $this->STATUS_OK,
                $this->DUEL_QUESTS => array()
            );
        }
        $duelQuestsResponse = array();
        foreach ($duelQuests as $duelQuest) {
            $friendState = $this->findUserState($duelQuest, $userId);
            $friendId = $friendState[$this->USER]->getId();
            $friendName = $friendState[$this->USER]->getUsername();
            $quest = $duelQuest->getQuest();
            $state = $duelQuest->getState();
            $did_win = false;
            if ($state == $this->ACCEPTER_WIN) {
                if ($friendState[$this->STATE] != $this->ACCEPTER) {
                    $did_win = true;
                }
            } else {
                if ($friendState[$this->STATE] == $this->ACCEPTER) {
                    $did_win = true;
                }
            }
            $senderImageURL = $this->PROOF_QUEST_DUEL_IMAGE_URL .
                $userId . '/' . $duelQuest->getId() . $this->JPG_EXTENSION;
            $accepterImageURL = $this->PROOF_QUEST_DUEL_IMAGE_URL .
                $friendId . '/' . $duelQuest->getId() . $this->JPG_EXTENSION;
            $duelQuestsResponse[] = $this->createDuelQuestsResponse(
                $did_win,
                $friendId,
                $friendName,
                $duelQuest->getDaysLeft(),
                $quest->getId(),
                $quest->getName(),
                $quest->getDescription(),
                3,
                $senderImageURL,
                $accepterImageURL,
                $duelQuest->hasGotReward(),
                $quest->getReward()
            );
        }
        return array(
            $this->STATUS => $this->STATUS_OK,
            $this->DUEL_QUESTS => $duelQuestsResponse
        );
    }

    /**
     * Find all confirmed duel quests
     *
     * @param $data
     *
     * @return array
     */
    public function getActiveDuelQuests($data)
    {
        global $kernel;
        $validatedResponse = $kernel
            ->getContainer()
            ->get('app.manager.validator')
            ->getUserIdFromData($data);
        if (!$validatedResponse[$this->STATUS] == $this->STATUS_OK) {
            return $validatedResponse;
        }
        $userId = $validatedResponse[$this->USER_ID];
        $countActiveDuels = 0;
        $countActiveDuels += $this->getEm()
            ->getRepository('AppBundle:QuestDuelRequest')
            ->countIncomingQuestDuelByUserId($userId);
        $inProgressDuels = $this->getEm()
            ->getRepository('AppBundle:QuestDuelRequest')
            ->findInProgressQuestDuelByUserId($userId);
        if (!empty($inProgressDuels)) {
            foreach ($inProgressDuels as $duelQuest) {
                if ($duelQuest->getState() == $this->IN_PROGRESS) {
                    if (!file_exists(dirname(dirname(dirname(dirname(__FILE__)))).'/web'.$this->PROOF_QUEST_DUEL_IMAGE_URL .
                            $userId . '/' . $duelQuest->getId() . $this->JPG_EXTENSION)) {
                        $countActiveDuels ++;
                    }
                }
            }
        }
        return array(
            $this->STATUS => $this->STATUS_OK,
            $this->ACTIVE_DUELS => $countActiveDuels
        );
    }

    /**
     * Find state of user. is it accepter or sender
     *
     * @param $quest
     * @param $userId
     *
     * @return array
     */
    private function findUserState(QuestDuelRequest $quest, $userId)
    {
        if ($quest->getSender()->getId() == $userId) {
            return array($this->USER => $quest->getAccepter(), $this->STATE => $this->ACCEPTER);
        } elseif ($quest->getAccepter()->getId() == $userId) {
            return array($this->USER => $quest->getSender(), $this->STATE => $this->SENDER);
        }
    }

    /**
     * @param $friendId
     * @param $friendName
     * @param $time
     * @param $questId
     * @param $name
     * @param $description
     * @param $state
     * @param $senderImageURL
     * @param $accepterImageURL
     * @param $gotReward
     * @param QuestReward $reward
     * @return array
     */
    private function createDuelQuestsResponse(
        $did_win,
        $friendId,
        $friendName,
        $time,
        $questId,
        $name,
        $description,
        $state,
        $senderImageURL,
        $accepterImageURL,
        $gotReward,
        QuestReward $reward
    ) {
    
        $quest = array(
            $this->DID_WIN => $did_win,
            $this->FRIEND_ID => $friendId,
            $this->USERNAME => $friendName,
            $this->DAYS_LEFT => $time,
            $this->QUEST_ID => $questId,
            $this->NAME => $name,
            $this->TYPE => 1,
            $this->DESCRIPTION => $description,
            $this->STATE => $state,
            $this->PROVE_IMAGE_1 => $senderImageURL,
            $this->PROVE_IMAGE_2 => $accepterImageURL,
            $this->GOT_REWARD => $gotReward,
            $this->REWARD => array(
                $this->GOLD => $reward->getGold(),
                $this->CRYSTALS => $reward->getCrystals(),
                $this->SKILL_ID => $reward->getSkillId()
            )
        );
        return $quest;
    }
}
