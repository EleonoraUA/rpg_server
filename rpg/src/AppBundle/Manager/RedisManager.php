<?php
/**
 * Class File RedisManager
 *
 * @package AppBundle\Manager
 */

namespace AppBundle\Manager;

use AppBundle\Entity\Personage;
use AppBundle\Manager\Traits\BattleTrait;
use AppBundle\Manager\Traits\EffectsTrait;
use AppBundle\Manager\Traits\SkillFieldsTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Redis;

/**
 * Class RedisManager
 * Manage with Redis operations
 *
 * @package AppBundle\Manager
 */

class RedisManager
{
    use BattleTrait;
    use UserInfoFieldsTrait;
    use SkillFieldsTrait;
    use EffectsTrait;

    const HOST = '127.0.0.1';
    const PORT = '6379';

    /**
     * @var Redis $redis
     *
     * @access private
     */
    private $redis;

    /**
     * RedisManager constructor.
     */
    public function __construct()
    {
        $this->redis = new Redis();
        $this->redis->connect(self::HOST, self::PORT);
    }

    /**
     * Get redis
     *
     * @return Redis
     */
    private function getRedis()
    {
        return $this->redis;
    }

    /**
     * Write battle information to redis
     *
     * @param string $token user's token
     * @param Personage $personage user's personage
     * @param string $opponentToken opponent's token
     * @param int $time time of the beginning of the battle
     * @param bool $currentTurn is it user's current turn
     * @param array $skills user's skills
     * @param string $battleType type of battle
     * @param int $stageId
     */
    public function writeBattleToRedis(
        $token,
        $personage,
        $opponentToken,
        $time,
        $currentTurn,
        $skills,
        $battleType,
        $stageId = 0
    ) {
        if ($old_opponent = $this->getOpponentToken($token)) {
            $this->deleteBattleInfo($token);
            $this->deleteBattleInfo($old_opponent);
        }
        $json_skills = array();
        foreach ($skills as $skill) {
            $array[$this->SKILL_ID] = $skill->getId();
            $array[$this->MULTIPLIER] = $skill->getMultiplier();
            $array[$this->COOLDOWN] = $skill->getCd();
            $array[$this->CURRENT_CD] = 0;
            $effects = $skill->getEffects();
            $effectsArray = array();
            foreach ($effects as $effect) {
                $effectArray[$this->EFFECT_ID] = $effect->getId();
                $effectArray[$this->NAME] = $effect->getName();
                $effectArray[$this->DURATION] = $effect->getDuration();
                $effectsArray[] = $effectArray;
            }
            $array[$this->EFFECTS] = $effectsArray;
            array_push($json_skills, $array);
        }
        $redis = $this->getRedis();
        $redis->hSet($token, $this->BATTLE_TYPE, $battleType);
        $redis->hSet($token, $this->OPPONENT, $opponentToken);
        $redis->hSet($token, $this->MAX_HP, $personage->getHp());
        $redis->hSet($token, $this->CURRENT_HP, $personage->getHp());
        $redis->hSet($token, $this->ATTACK, $personage->getAtk());
        $redis->hSet($token, $this->CURRENT_TURN, $currentTurn);
        $redis->hSet($token, $this->START_TIME, $time);
        $redis->hSet($token, $this->SKILLS, json_encode($json_skills));
        $redis->hSet($token, $this->EFFECTS, array());
        $this->setSkillsDamage($token);

        if ($stageId) {
            $redis->hSet($token, $this->STAGE_ID, $stageId);
        }
    }

    /**
     * Get information about battle for user with given token
     *
     * @param string $token user's token
     *
     * @return array
     */
    public function getBattleInfo($token)
    {
        $redis = $this->getRedis();
        $myHp = $redis->hGet($token, $this->CURRENT_HP);
        $currentTurn = $redis->hGet($token, $this->CURRENT_TURN);
        if ($currentTurn != 1) {
            $currentTurn = 0;
        }
        $opponentToken = $redis->hGet($token, $this->OPPONENT);
        $opponentHp = $redis->hGet($opponentToken, $this->CURRENT_HP);
        $opponentEffects = json_decode($redis->hGet($opponentToken, $this->EFFECTS), true);
        $opponentInfo[$this->CURRENT_HP] = $opponentHp;
        $opponentInfo[$this->EFFECTS] = $opponentEffects ? $opponentEffects : array();
        $skillsDamage = json_decode($redis->hGet($token, $this->SKILLS_DAMAGE), true);
        $skills = json_decode($redis->hGet($token, $this->SKILLS), true);
        $effects = json_decode($redis->hGet($token, $this->EFFECTS), true);
        $skillsState = array();
        for ($i = 0; $i < count($skills); $i++) {
            $skillsState[$i] = array(
                $this->SKILL_ID => $skills[$i][$this->SKILL_ID],
                $this->COOLDOWN => $skills[$i][$this->CURRENT_CD]
            );
        }
        return array(
            $this->SKILLS_CONDITION => $skillsState,
            $this->PLAYER_INFO => array(
                $this->CURRENT_HP => $myHp,
                $this->EFFECTS => $effects ? $effects : array()
            ),
            $this->OPPONENT_INFO => $opponentInfo,
            $this->SKILLS_DAMAGE => $skillsDamage,
            $this->CURRENT_TURN => $currentTurn
        );
    }

    /**
     * Get time when turn started
     *
     * @param string $token user's token
     *
     * @return int
     */
    public function getStartTime($token)
    {
        return $this->getRedis()->hGet($token, $this->START_TIME);
    }

    /**
     * Get amount of attack for user with given token
     *
     * @param string $token user's token
     *
     * @return int
     */
    public function getAttack($token)
    {
        return $this->getRedis()->hGet($token, $this->ATTACK);
    }

    /**
     * Get opponent's HP
     *
     * @param string $token user's token
     *
     * @return int
     */
    public function getOpponentHp($token)
    {
        $opponent = $this->getRedis()->hGet($token, $this->OPPONENT);
        return $this->getRedis()->hGet($opponent, $this->CURRENT_HP);
    }

    /**
     * Set opponent's HP
     *
     * @param string $token user's token
     *
     * @param int $opponents_hp new opponent's hp
     */
    public function setOpponentHp($token, $opponents_hp)
    {
        $opponent = $this->getRedis()->hGet($token, $this->OPPONENT);
        $this->getRedis()->hSet($opponent, $this->CURRENT_HP, $opponents_hp);
    }

    /**
     * Set new turn time
     *
     * @param string $token user's token
     */
    public function setTurnTime($token)
    {
        $time = time();
        $redis = $this->getRedis();
        $opponent = $redis->hGet($token, $this->OPPONENT);
        $redis->hSet($token, $this->START_TIME, $time);
        $redis->hSet($opponent, $this->START_TIME, $time);
    }

    /**
     * Change turns
     *
     * @param string $token user's token
     */
    public function setCurrentTurn($token)
    {
        $redis = $this->getRedis();
        $opponent = $redis->hGet($token, $this->OPPONENT);
        $redis->hSet($token, $this->CURRENT_TURN, false);
        $redis->hSet($opponent, $this->CURRENT_TURN, true);
    }

    /**
     * Get skills
     *
     * @param string $token user's token
     *
     * @return array
     */
    public function getSkills($token)
    {
        return json_decode($this->getRedis()->hGet($token, $this->SKILLS), true);
    }

    /**
     * Change skill cooldown after use
     *
     * @param string $token user's token
     *
     * @param int $skillId id of used skill
     */
    public function updateSkillCooldown($token, $skillId)
    {
        $skills = $this->getSkills($token);
        for ($i = 0; $i < count($skills); $i++) {
            if ($skills[$i][$this->SKILL_ID] === $skillId) {
                $skills[$i][$this->CURRENT_CD] = $skills[$i][$this->COOLDOWN];
                $this->getRedis()->hSet($token, $this->SKILLS, json_encode($skills));
                return;
            }
        }
    }

    /**
     * Get opponent token
     *
     * @param string $token user's token
     *
     * @return string
     */
    public function getOpponentToken($token)
    {
        return $this->getRedis()->hGet($token, $this->OPPONENT);
    }

    /**
     * Change skills cooldown for opponent
     *
     * @param string $token user's token
     */
    public function updateOpponentCooldown($token)
    {
        $opponent = $this->getRedis()->hGet($token, $this->OPPONENT);
        $skills = json_decode($this->getRedis()->hGet($opponent, $this->SKILLS), true);
        for ($i = 0; $i < count($skills); $i++) {
            if ($skills[$i][$this->CURRENT_CD] > 0) {
                $skills[$i][$this->CURRENT_CD] -= 1;
            }
        }
        $this->getRedis()->hSet($opponent, $this->SKILLS, json_encode($skills));
    }

    /**
     * Get current turn
     *
     * @param string $token user's token
     *
     * @return bool
     */
    public function isCurrentTurn($token)
    {
        return $this->getRedis()->hGet($token, $this->CURRENT_TURN) ? 1 : 0;
    }

    /**
     * Delete information about battle
     *
     * @param string $token user's token
     */
    public function deleteBattleInfo($token)
    {
        $this->getRedis()->del($token);
    }

    /**
     * Save information about skill's damage
     *
     * @param string $token user's token
     *
     * @param int $skillId id of used skill
     *
     * @param int $damage the amount of damage
     */
    public function setSkillsDamage($token, $skillId = 0, $damage = 0)
    {
        $redis = $this->getRedis();
        $opponent = $this->getOpponentToken($token);
        $skillDamage = array($this->SKILL_ID => $skillId, $this->DAMAGE => $damage);
        $redis->hSet($token, $this->SKILLS_DAMAGE, json_encode($skillDamage));
        $redis->hSet($opponent, $this->SKILLS_DAMAGE, json_encode($skillDamage));
    }

    /**
     * Add information about online user
     *
     * @param int $userId id of user
     *
     * @param string $token user's token
     */
    public function addOnlineUser($userId, $token)
    {
        $redis = $this->getRedis();
        $oldToken = $redis->hGet($this->USER_TOKENS, $userId);
        if ($oldToken) {
            $this->signoutUser($oldToken);
            $oldOpponent = $this->getOpponentToken($oldToken);
            $this->deleteBattleInfo($oldToken);
            $this->deleteBattleInfo($oldOpponent);
        }
        $redis->hSet($this->ONLINE_USERS, $token, $userId);
        $redis->hSet($this->USER_TOKENS, $userId, $token);
    }

    /**
     * Get user id by token
     *
     * @param string $token user's token
     *
     * @return int
     */
    public function getUserIdByToken($token)
    {
        return $this->getRedis()->hGet($this->ONLINE_USERS, $token);
    }

    /**
     * Delete information about user when logout
     *
     * @param string $token user's token
     */
    public function signoutUser($token)
    {
        $redis = $this->getRedis();
        $userId = $this->getUserIdByToken($token);
        $redis->hDel($this->ONLINE_USERS, $token);
        $redis->hDel($this->USER_TOKENS, $userId);
    }

    /**
     * Get max HP of personage
     *
     * @param string $token user's token
     *
     * @return int
     */
    public function getMaxHp($token)
    {
        return $this->getRedis()->hGet($token, $this->MAX_HP);
    }

    /**
     * Get current HP of personage
     *
     * @param string $token user's token
     *
     * @return int
     */
    public function getHp($token)
    {
        return $this->getRedis()->hGet($token, $this->CURRENT_HP);
    }

    /**
     * Set current HP of personage
     *
     * @param string $token user's token
     *
     * @param int $current_hp new personage's hp
     */
    public function setHp($token, $current_hp)
    {
        $this->getRedis()->hSet($token, $this->CURRENT_HP, $current_hp);
    }

    /**
     * Gets id of the stage
     *
     * @param string $token user's token
     *
     * @return int
     */
    public function getStageId($token)
    {
        return $this->getRedis()->hGet($token, $this->STAGE_ID);
    }

    /**
     * Gets type of the battle
     *
     * @param string $token
     *
     * @return string
     */
    public function getBattleType($token)
    {
        return $this->getRedis()->hGet($token, $this->BATTLE_TYPE);
    }

    /**
     * Gets effects of the personage
     *
     * @param string $token
     *
     * @return array
     */
    public function getEffects($token)
    {
        return json_decode($this->getRedis()->hGet($token, $this->EFFECTS), true);
    }

    /**
     * Adds effect on personage or opponent
     *
     * @param string $token
     *
     * @param array $effect
     */
    public function addEffect($token, $effect)
    {
        if ($effect[$this->EFFECT_ID] == $this->INCREASE_ATTACK_EFFECT ||
            $effect[$this->EFFECT_ID] == $this->INCREASE_DEFENCE_EFFECT ||
            $effect[$this->EFFECT_ID] == $this->HEALING_EFFECT ||
            $effect[$this->EFFECT_ID] == $this->PROTECTION_EFFECT) {
            $target = $token;
        } elseif ($effect[$this->EFFECT_ID] == $this->ATTACK_BREAK_EFFECT ||
            $effect[$this->EFFECT_ID] == $this->DEFENCE_BREAK_EFFECT ||
            $effect[$this->EFFECT_ID] == $this->CONTINUOUS_DAMAGE_EFFECT ||
            $effect[$this->EFFECT_ID] == $this->STUN_EFFECT) {
            $target = $this->getOpponentToken($token);
        }
        $effects = $this->getEffects($target);
        if (!$effects) {
            $effects = array();
        }
        if ($target != $token && $this->isProtected($target)) {
            return;
        }
        if (count($effects) < $this->MAX_EFFECTS_NUMBER) {
            if ($effect[$this->EFFECT_ID] == $this->CONTINUOUS_DAMAGE_EFFECT) {
                array_push($effects, $effect);
            } else {
                $effectAlreadyExist = false;
                foreach ($effects as $e) {
                    if ($e[$this->EFFECT_ID] == $effect[$this->EFFECT_ID]) {
                        $e[$this->DURATION] = $effect[$this->DURATION];
                        $effectAlreadyExist = true;
                        break;
                    }
                }
                if (!$effectAlreadyExist) {
                    array_push($effects, $effect);
                }
            }
            $this->getRedis()->hSet($target, $this->EFFECTS, json_encode($effects));
        }
    }

    /**
     * Updates duration of the effects on the personage
     *
     * @param string $token
     */
    public function updateEffectsDuration($token)
    {
        $oldEffects = $this->getEffects($token);
        if (!$oldEffects) {
            return;
        }
        $effects = array();
        foreach ($oldEffects as $effect) {
            if ($effect[$this->EFFECT_ID] != $this->CONTINUOUS_DAMAGE_EFFECT &&
                $effect[$this->EFFECT_ID] != $this->PROTECTION_EFFECT &&
                $effect[$this->EFFECT_ID] != $this->HEALING_EFFECT &&
                $effect[$this->DURATION] > 0) {
                $effect[$this->DURATION] -= 1;
                if ($effect[$this->DURATION]) {
                    $effects[] = $effect;
                }
            } else {
                $effects[] = $effect;
            }
        }
        $this->getRedis()->hSet($token, $this->EFFECTS, json_encode($effects));
    }

    /**
     * Updates DOT,HOT effects
     *
     * @param string $token
     */
    public function updateTickEffectsDuration($token)
    {
        $oldEffects = $this->getEffects($token);
        if (!$oldEffects) {
            return;
        }
        $effects = array();
        foreach ($oldEffects as $effect) {
            if ($effect[$this->EFFECT_ID] == $this->CONTINUOUS_DAMAGE_EFFECT ||
                $effect[$this->EFFECT_ID] == $this->PROTECTION_EFFECT ||
                $effect[$this->EFFECT_ID] == $this->HEALING_EFFECT &&
                $effect[$this->DURATION] > 0) {
                $effect[$this->DURATION] -= 1;
                if ($effect[$this->DURATION]) {
                    $effects[] = $effect;
                }
            } else {
                $effects[] = $effect;
            }
        }
        $this->getRedis()->hSet($token, $this->EFFECTS, json_encode($effects));
    }

    /**
     * Returns is user online or not by userId
     *
     * @param int $userId id of user
     *
     * @return bool
     */
    public function isOnlineUserByUserId($userId)
    {
        $redis = $this->getRedis();
        $token = $redis->hGet($this->USER_TOKENS, $userId);
        $isOnline = $redis->get('online:'.$token);
        if ($isOnline === false || date('YmdHis') > $isOnline) {
            return false;
        }
        return true;
    }

    /**
     * Checks if personage has Protection effect
     *
     * @param string $token
     *
     * @return bool
     */
    public function isProtected($token)
    {
        $effects = $this->getEffects($token);
        if (!$effects) {
            return false;
        }
        foreach ($effects as $effect) {
            if ($effect[$this->EFFECT_ID] == $this->PROTECTION_EFFECT) {
                return true;
            }
        }
        return false;
    }

    /**
     * Delete debuffs from personage
     *
     * @param string $token
     */
    public function deleteDebuffs($token)
    {
        $effects = $this->getEffects($token);
        if (!$effects) {
            return;
        }
        $newEffects = array();
        foreach ($effects as $effect) {
            if ($effect[$this->EFFECT_ID] == $this->INCREASE_DEFENCE_EFFECT ||
                $effect[$this->EFFECT_ID] == $this->INCREASE_ATTACK_EFFECT ||
                $effect[$this->EFFECT_ID] == $this->HEALING_EFFECT ||
                $effect[$this->EFFECT_ID] == $this->PROTECTION_EFFECT) {
                $newEffects[] = $effect;
            }
        }
        $this->getRedis()->hSet($token, $this->EFFECTS, json_encode($newEffects));
    }

    /**
     * Removes opponent from stun
     *
     * @param string $token
     */
    public function unstunOpponent($token)
    {
        $opponent = $this->getOpponentToken($token);
        $effects = $this->getEffects($opponent);
        if (!$effects) {
            return;
        }
        $newEffects = array();
        foreach ($effects as $effect) {
            if ($effect[$this->EFFECT_ID] != $this->STUN_EFFECT) {
                $newEffects[] = $effect;
            }
        }
        $this->getRedis()->hSet($opponent, $this->EFFECTS, json_encode($newEffects));
    }
}
