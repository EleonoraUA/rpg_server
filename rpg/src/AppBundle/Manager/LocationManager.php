<?php
/**
 * Class File LocationManager
 *
 * @package AppBundle\Manager
 */

namespace AppBundle\Manager;

use AppBundle\Entity\ClearingStage;
use AppBundle\Entity\Monster;
use AppBundle\Entity\Personage;
use AppBundle\Entity\Stage;
use AppBundle\Manager\Traits\BattleTrait;
use AppBundle\Manager\Traits\QuestInfoFieldsTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;

/**
 * Class LocationManager
 * Manage with battle processes
 *
 * @package AppBundle\Manager
 */
class LocationManager extends Manager
{
    use UserInfoFieldsTrait;
    use BattleTrait;
    use QuestInfoFieldsTrait;

    /**
     * @return RedisManager|object
     */
    private function getRedisManager()
    {
        global $kernel;
        return $kernel->getContainer()->get('app.manager.redis');
    }

    /**
     * @return AccountManager|object
     */
    private function getAccountManager()
    {
        global $kernel;
        return $kernel->getContainer()->get('app.manager.account');
    }

    /**
     * Select monster from stage
     *
     * @param Stage $stage
     *
     * @return Monster $monster
     */
    public function generateMonsterForStage($stage)
    {
        $monsters = $stage->getMonsters();
        return $monsters[rand(0, count($monsters) - 1)];
    }

    /**
     * Get stage by it's id
     *
     * @param $stageId
     *
     * @return null|Stage
     */
    public function getStageById($stageId)
    {
        return $this->getEm()->getRepository('AppBundle:Stage')->find($stageId);
    }

    /**
     * Get ids of accessible locations
     *
     * @param Personage $personage
     *
     * @return array
     */
    public function getAccessibleLocations($personage)
    {
        $locations = $this->getEm()->getRepository('AppBundle:Location')->findAccessibleLocations($personage);
        $locIds = array();
        foreach ($locations as $location) {
            $locIds[] = $location->getId();
        }
        return $locIds;
    }

    /**
     * Get information about stages on location
     *
     * @param Personage $personage
     *
     * @param int $locationId
     *
     * @return array
     */
    public function getInfoAboutLocation($personage, $locationId)
    {
        $stages = $this->getEm()->getRepository('AppBundle:Stage')->findBy(array($this->LOCATION => $locationId));
        if (!$stages) {
            return array();
        }
        $clearingStages =  $this->getEm()->getRepository('AppBundle:ClearingStage')->findByPersonage($personage);
        $clearingStagesInfo = array();
        if (count($clearingStages) > 0) {
            foreach ($clearingStages as $clearingStage) {
                $clearingStagesInfo[$clearingStage->getStage()->getId()] = $clearingStage->getIsCleared();
            }
        }
        $stagesInfo = array();
        foreach ($stages as $stage) {
            $info[$this->BATTLE_PLACE_ID] = $stage->getId();
            if (isset($clearingStagesInfo[$stage->getId()])) {
                $info[$this->STATE] = $clearingStagesInfo[$stage->getId()] ? 1 : 0;
            } else {
                $info[$this->STATE] = $this->IS_NOT_AVAILABLE;
            }
            $stagesInfo[] = $info;
        }
        return $stagesInfo;
    }
    
    /**
     * Get stage's id
     *
     * @param string $token user's token
     *
     * @return int
     */
    public function getStageId($token)
    {
        return $this->getRedisManager()->getStageId($token);
    }

    /**
     * Set stage as cleared
     *
     * @param string $token
     */
    public function clearStage($token)
    {
        $personage = $this->getAccountManager()->getPersonageByUserToken($token);
        $stageId = $this->getRedisManager()->getStageId($token);
        $stage = $this->getEm()->getRepository('AppBundle:Stage')->find($stageId);
        $clearingStage = $this->getEm()->getRepository('AppBundle:ClearingStage')->findOneBy(array(
            $this->STAGE => $stage,
            $this->PERSONAGE => $personage
        ));
        $clearingStage->setIsCleared(true);
        $nextStage = $this->getEm()->getRepository('AppBundle:Stage')->find($stageId + 1);
        if ($nextStage) {
            $nextClearingStage = new ClearingStage($nextStage, $personage, false);
            $this->getEm()->persist($nextClearingStage);
        }
        $this->getEm()->flush();
    }

    /**
     * Checks if stage is enabled
     *
     * @param Stage $stage
     *
     * @param Personage $personage
     *
     * @return bool
     */
    public function stageIsEnabled($stage, $personage)
    {
        $clearingStage = $this->getEm()->getRepository('AppBundle:ClearingStage')->findOneBy(array(
            $this->STAGE => $stage,
            $this->PERSONAGE => $personage
        ));
        if (!$clearingStage) {
            return false;
        }
        return true;
    }

    /**
     * Checks if location with such id exists
     *
     * @param int $locationId
     *
     * @return bool
     */
    public function locationExists($locationId)
    {
        $location = $this->getEm()->getRepository('AppBundle:Location')->find($locationId);
        return $location ? true : false;
    }
}
