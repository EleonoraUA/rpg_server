<?php
/**
 * Class File BattleResponseManager
 *
 * @package AppBundle\Manager
 */

namespace AppBundle\Manager;

use AppBundle\Entity\Personage;
use AppBundle\Entity\Stage;
use AppBundle\Manager\Traits\BattleTrait;
use AppBundle\Manager\Traits\ErrorCodesTrait;
use AppBundle\Manager\Traits\RequestResponseTypesTrait;
use AppBundle\Manager\Traits\SkillFieldsTrait;
use AppBundle\Manager\Traits\UserInfoFieldsTrait;
use Symfony\Component\HttpFoundation\JsonResponse;

const BATTLE_TIME = 30;

/**
 * Class BattleResponseManager
 * Manage with battle processes
 *
 * @package AppBundle\Manager
 */
class BattleResponseManager extends Manager
{
    use ErrorCodesTrait;
    use RequestResponseTypesTrait;
    use UserInfoFieldsTrait;
    use BattleTrait;

    /**
     * @return RedisManager|object
     */
    private function getRedisManager()
    {
        global $kernel;
        return $kernel->getContainer()->get('app.manager.redis');
    }

    /**
     * @return AccountManager|object
     */
    private function getAccountManager()
    {
        global $kernel;
        return $kernel->getContainer()->get('app.manager.account');
    }

    /**
     * @return SkillAPIManager|object
     */
    private function getSkillManager()
    {
        global $kernel;
        return $kernel->getContainer()->get('app.manager.skillapi');
    }

    /**
     * @return \FOS\UserBundle\Util\TokenGenerator|object
     */
    private function getTokenGenerator()
    {
        global $kernel;
        return $kernel->getContainer()->get('fos_user.util.token_generator');
    }

    /**
     * @return BattleManager|object
     */
    private function getBattleManager()
    {
        global $kernel;
        return $kernel->getContainer()->get('app.manager.battle');
    }

    /**
     * @return LocationManager|object
     */
    private function getLocationManager()
    {
        global $kernel;
        return $kernel->getContainer()->get('app.manager.location');
    }

    /**
     * Checks battle init request on error
     *
     * @param string $token
     *
     * @param Personage $personage
     *
     * @param Stage $stage
     *
     * @return JsonResponse|null
     */
    public function battleInitError($token, $personage, $stage)
    {
        if ($this->getBattleManager()->doesBattleExist($token) &&
            !$this->getBattleManager()->isBattleType($token, $this->MONSTER)
        ) {
            return new JsonResponse(
                array(
                    $this->STATUS => $this->WRONG_BATTLE_TYPE,
                    $this->TYPE => $this->ADVENTURE_BATTLE_INIT
                )
            );
        }
        if (!$this->getLocationManager()->stageIsEnabled($stage, $personage)) {
            return new JsonResponse(
                array(
                    $this->STATUS => $this->STAGE_IS_NOT_ENABLED,
                    $this->TYPE => $this->ADVENTURE_BATTLE_INIT
                )
            );
        }
        return null;
    }

    /**
     * Creates response for monster battle
     *
     * @param string $token
     *
     * @param Personage $personage
     *
     * @param Stage $stage
     *
     * @return JsonResponse
     */
    public function generateMonsterBattle($token, $personage, $stage)
    {
        $monster = $this->getLocationManager()->generateMonsterForStage($stage);
        $monstersSkills = $monster->getSkills();
        $monstersToken = $this->getTokenGenerator()->generateToken();

        $time = time();
        $skills = $this->getSkillManager()->getSelectedSkillsOfPersonage($personage);

        $this->getRedisManager()->writeBattleToRedis(
            $token,
            $personage,
            $monstersToken,
            $time,
            true,
            $skills,
            $this->MONSTER,
            $stage->getId()
        );
        $this->getRedisManager()->writeBattleToRedis(
            $monstersToken,
            $monster,
            $token,
            $time,
            false,
            $monstersSkills,
            $this->MONSTER,
            $stage->getId()
        );

        $response = $this->getInitResponse($token, $monster->getName(), $time, $this->ADVENTURE_BATTLE_INIT);
        $response[$this->PLAYER_INFO][$this->LVL] = $personage->getLvl();
        $response[$this->OPPONENT_INFO][$this->LVL] = $monster->getLvl();
        return new JsonResponse($response);
    }

    /**
     * Gets init response
     *
     * @param string $token user's token
     *
     * @param string $opponentName opponent's name
     *
     * @param int $time time of the beginning of the battle
     *
     * @return array
     */
    public function getInitResponse($token, $opponentName, $time, $type)
    {
        $redisManager = $this->getRedisManager();
        return array
        (
            $this->PLAYER_INFO => array(
                $this->CURRENT_HP => (int)$redisManager->getHp($token),
                $this->MAX_HP => (int)$redisManager->getHp($token)
            ),
            $this->STATUS => $this->STATUS_OK,
            $this->TYPE => $type,
            $this->OPPONENT_INFO => array(
                $this->OPPONENT_NAME => $opponentName,
                $this->CURRENT_HP => (int)$redisManager->getOpponentHp($token),
                $this->MAX_HP => (int)$redisManager->getOpponentHp($token)
            ),
            $this->IS_CURRENT_TURN => (int)$redisManager->isCurrentTurn($token),
            $this->TIME => $time
        );
    }

    /**
     * Creates response for arena battle
     *
     * @param string $token1
     *
     * @param array $skills1
     *
     * @param string $token2
     *
     * @param array $skills2
     *
     * @return JsonResponse
     */
    public function createArenaBattle($token1, $skills1, $token2, $skills2)
    {
        $personage1 = $this->getAccountManager()->getPersonageByUserToken($token1);
        $personage2 = $this->getAccountManager()->getPersonageByUserToken($token2);

        $skills1 = $this->getSkillManager()->getArenaSkillsForPersonage($skills1);
        $skills2 = $this->getSkillManager()->getArenaSkillsForPersonage($skills2);

        $time = time();
        $turn = false;
        $currentPlayer = $token2;
        if (rand(0, 1)) {
            $turn = true;
            $currentPlayer = $token1;
        }
        $this->getRedisManager()->writeBattleToRedis(
            $token1,
            $personage1,
            $token2,
            $time,
            $turn,
            $skills1,
            $this->ARENA
        );

        $this->getRedisManager()->writeBattleToRedis(
            $token2,
            $personage2,
            $token1,
            $time,
            !$turn,
            $skills2,
            $this->ARENA
        );
        $initResponse1 = $this->getInitResponse($token1, $personage2->getName(), $time, $this->ARENA_BATTLE_INIT);
        $initResponse2 = $this->getInitResponse($token2, $personage1->getName(), $time, $this->ARENA_BATTLE_INIT);

        $response[$this->STATUS] = $this->STATUS_OK;
        $response[$this->CURRENT_TURN] = $currentPlayer;

        $response[$token1] = $initResponse1;
        $response[$token1][$this->PLAYER_INFO][$this->LVL] = $personage1->getLvl();
        $response[$token1][$this->OPPONENT_INFO][$this->LVL] = $personage2->getLvl();
        $response[$token1][$this->OPPONENT_INFO][$this->CLASS_ID] = $personage2->getClass()->getId();

        $response[$token2][$this->PLAYER_INFO][$this->LVL] = $personage2->getLvl();
        $response[$token2] = $initResponse2;
        $response[$token2][$this->OPPONENT_INFO][$this->LVL] = $personage1->getLvl();
        $response[$token2][$this->OPPONENT_INFO][$this->CLASS_ID] = $personage1->getClass()->getId();

        return new JsonResponse($response);
    }

    /**
     * Creates response for tournament battle
     *
     * @param string $token1
     *
     * @param string $token2
     *
     * @return JsonResponse
     */
    public function createTournamentBattle($token1, $token2)
    {
        $personage1 = $this->getAccountManager()->getPersonageByUserToken($token1);
        $personage2 = $this->getAccountManager()->getPersonageByUserToken($token2);

        if (!$personage1 || !$personage2) {
            return new JsonResponse(array(
                $this->STATUS => $this->USER_DOES_NOT_EXIST,
                $this->TYPE => $this->TOURNAMENT_BATTLE_INIT
            ));
        }
        $skills1 = $this->getSkillManager()->getSelectedSkillsOfPersonage($personage1);
        $skills2 = $this->getSkillManager()->getSelectedSkillsOfPersonage($personage2);

        $time = time();
        $turn = false;
        $currentPlayer = $token2;

        if (rand(0, 1)) {
            $turn = true;
            $currentPlayer = $token1;
        }

        $this->getRedisManager()->writeBattleToRedis(
            $token1,
            $personage1,
            $token2,
            $time,
            $turn,
            $skills1,
            $this->TOURNAMENT
        );

        $this->getRedisManager()->writeBattleToRedis(
            $token2,
            $personage2,
            $token1,
            $time,
            !$turn,
            $skills2,
            $this->TOURNAMENT
        );

        $initResponse1 = $this->getInitResponse($token1, $personage2->getName(), $time, $this->TOURNAMENT_BATTLE_INIT);
        $initResponse2 = $this->getInitResponse($token2, $personage1->getName(), $time, $this->TOURNAMENT_BATTLE_INIT);

        $tournamentInfo1 = $this
            ->getEm()
            ->getRepository('AppBundle:Tournament')
            ->findOneBy(array($this->PERSONAGE => $personage1));
        $tournamentInfo2 = $this
            ->getEm()
            ->getRepository('AppBundle:Tournament')
            ->findOneBy(array($this->PERSONAGE => $personage2));

        $response[$this->STATUS] = $this->STATUS_OK;
        $response[$this->CURRENT_TURN] = $currentPlayer;

        $response[$token1] = $initResponse1;
        $response[$token1][$this->PLAYER_INFO][$this->LVL] = $personage1->getLvl();
        $response[$token1][$this->OPPONENT_INFO][$this->LVL] = $personage2->getLvl();
        $response[$token1][$this->PLAYER_INFO][$this->WIN_COUNT] = $tournamentInfo1 ? $tournamentInfo1
            ->getWinCount() : 0;
        $response[$token1][$this->OPPONENT_INFO][$this->CLASS_ID] = $personage2->getClass()->getId();

        $response[$token2] = $initResponse2;
        $response[$token2][$this->PLAYER_INFO][$this->LVL] = $personage2->getLvl();
        $response[$token2][$this->OPPONENT_INFO][$this->LVL] = $personage1->getLvl();
        $response[$token2][$this->PLAYER_INFO][$this->WIN_COUNT] = $tournamentInfo2 ? $tournamentInfo2
            ->getWinCount() : 0;
        $response[$token2][$this->OPPONENT_INFO][$this->CLASS_ID] = $personage1->getClass()->getId();

        return new JsonResponse($response);
    }

    /**
     * Gets battle condition response
     *
     * @param string $token
     *
     * @return JsonResponse
     */
    public function battleConditionResponse($token)
    {
        $response = $this->getBattleCondition($token);
        $stageId = $this->getRedisManager()->getStageId($token);
        if ($response[$this->DID_WIN] === 1) {
            $gold = rand($this->MIN_GOLD_REWARD, $this->MAX_GOLD_REWARD);
            $exp = $this->getAccountManager()->getExpReward($stageId);
            $reward = array($this->GOLD => $gold, $this->EXP => $exp);
            $response[$this->REWARD] = $reward;
            $this->getLocationManager()->clearStage($token);
            $this->getAccountManager()->sendReward($token, $reward);
        } elseif ($response[$this->DID_WIN] === 0) {
            $exp = (int)$this->getAccountManager()->getExpReward($stageId)/2;
            $this->getAccountManager()->setExperienceToPersonage($token, $exp);
            $reward = array($this->EXP => $exp);
            $response[$this->REWARD] = $reward;
            $this->getAccountManager()->sendReward($token, $reward);
        }

        if ($this->getBattleManager()->battleIsOver($token)) {
            $this->getBattleManager()->endBattle($token);
        }

        $response[$this->TYPE] = $this->ADVENTURE_BATTLE_CONDITION;
        return new JsonResponse($response);
    }

    /**
     * Gets PVP condition response
     *
     * @param string $token
     *
     * @param string $battleType
     *
     * @return JsonResponse
     */
    public function getPvpConditionResponse($token, $battleType)
    {
        $opponentToken = $this->getRedisManager()->getOpponentToken($token);
        $accountManager = $this->getAccountManager();
        $reward1 = array($this->GOLD => 0);
        $reward2 = array($this->GOLD => 0);
        $gold = rand($this->MIN_GOLD_REWARD, $this->MAX_GOLD_REWARD);

        if ($this->getBattleManager()->didWin($token)) {
            if ($battleType == $this->ARENA) {
                $reward1 = array($this->GOLD => $gold, $this->CRYSTALS => $this->ARENA_PRICE);
            } elseif ($battleType == $this->TOURNAMENT) {
                $reward1 = array($this->GOLD => $gold);
                $accountManager->personageWonTournamentBattle($token);
                $accountManager->personageLoseTournamentBattle($opponentToken);
            }
            $accountManager->sendReward($token, $reward1);
        } elseif ($this->getBattleManager()->didWin($opponentToken)) {
            if ($battleType == $this->ARENA) {
                $reward2 = array($this->GOLD => $gold, $this->CRYSTALS => $this->ARENA_PRICE);
            } elseif ($battleType == $this->TOURNAMENT) {
                $reward2 = array($this->GOLD => $gold);
                $accountManager->personageWonTournamentBattle($opponentToken);
                $accountManager->personageLoseTournamentBattle($token);
            }
            $accountManager->sendReward($opponentToken, $reward2);
        }
        $battleCondition1 = $this->getBattleCondition($token, $reward1);
        $battleCondition2 = $this->getBattleCondition($opponentToken, $reward2);

        if ($this->getBattleManager()->battleIsOver($token)) {
            $this->getBattleManager()->endBattle($token);
        }

        $response[$token] = $battleCondition1;
        $response[$opponentToken] = $battleCondition2;
        if ($response[$token][$this->CURRENT_TURN]) {
            $response[$this->CURRENT_TURN] = $token;
        } else {
            $response[$this->CURRENT_TURN] = $opponentToken;
        }
        $response[$this->STATUS] = $this->STATUS_OK;
        $response[$token][$this->TYPE] = $this->PVP_BATTLE_CONDITION;
        $response[$opponentToken][$this->TYPE] = $this->PVP_BATTLE_CONDITION;
        return new JsonResponse($response);
    }

    /**
     * Gets response for skill action
     *
     * @param string $token
     *
     * @param int $skillId
     *
     * @return JsonResponse
     */
    public function skillActionResponse($token, $skillId)
    {
        $this->getBattleManager()->resetSkillsDamage($token);

        $error = $this->checkTurnError($token, $skillId);
        if ($error) {
            return $error;
        }
        $battle_type = $this->getBattleManager()->getBattleType($token);
        $this->getBattleManager()->performUsingSkill($token, $skillId);
        $this->getBattleManager()->endTurn($token);

        return new JsonResponse(array(
            $this->STATUS => $this->STATUS_OK,
            $this->TYPE => $this->SKILL_ACTION,
            $this->BATTLE_TYPE => $battle_type
        ));
    }

    /**
     * Gets response for unexpected close request
     *
     * @param string $token
     *
     * @return JsonResponse
     */
    public function unexpectedCloseResponse($token)
    {
        $gold = rand($this->MIN_GOLD_REWARD, $this->MAX_GOLD_REWARD);
        $reward = array($this->GOLD => $gold);
        $battleCondition = array();
        if ($this->getBattleManager()->isBattleType($token, $this->ARENA)) {
            $reward[$this->CRYSTALS] = $this->ARENA_PRICE;
            $battleCondition = $this->pvpQuit($token, $reward);
        } elseif ($this->getBattleManager()->isBattleType($token, $this->TOURNAMENT)) {
            $battleCondition = $this->pvpQuit($token, $reward);
            $this->getAccountManager()->personageWonTournamentBattle($token);
            $this->getAccountManager()->personageLoseTournamentBattle($battleCondition[$this->OPPONENT]);
        }
        $this->getAccountManager()->sendReward($battleCondition[$this->OPPONENT], $reward);
        $this->getBattleManager()->endBattle($token);
        return new JsonResponse($battleCondition);
    }

    /**
     * Check if skill can be used now
     *
     * @param string $token user's token
     *
     * @param int $skill_id id of the used skill
     *
     * @return JsonResponse|null
     */
    public function checkTurnError($token, $skill_id)
    {
        if (!$this->getRedisManager()->isCurrentTurn($token)) {
            return new JsonResponse(
                array(
                    $this->STATUS => $this->NOT_YOUR_TURN
                )
            );
        }
        $skill = $this->getBattleManager()->getSkillById($token, $skill_id);
        if (!$skill) {
            return new JsonResponse(
                array(
                    $this->STATUS => $this->SKILL_IS_ON_COOLDOWN_OR_DOES_NOT_EXISTS
                )
            );
        }
        $startTime = $this->getRedisManager()->getStartTime($token);
        $time = time();
        if ($time - $startTime > BATTLE_TIME) {
            return new JsonResponse(
                array(
                    $this->STATUS => $this->RUN_OUT_OF_TIME
                )
            );
        }
        return null;
    }

    /**
     * Get battle condition for user with given token
     *
     * @param string $token user's token
     *
     * @param array $reward information about reward
     *
     * @return array
     */
    private function getBattleCondition($token, $reward = array())
    {
        $battleCondition = $this->getRedisManager()->getBattleInfo($token);
        $response = $battleCondition;
        $response[$this->STATUS] = $this->STATUS_OK;
        $response[$this->DID_WIN] = -1;
        if ($battleCondition[$this->OPPONENT_INFO][$this->CURRENT_HP] == 0) {
            $response[$this->DID_WIN] = 1;
            $response[$this->REWARD] = $reward;
        }
        if ($battleCondition[$this->PLAYER_INFO][$this->CURRENT_HP] == 0) {
            $response[$this->DID_WIN] = 0;
            $response[$this->REWARD] = $reward;
        }
        return $response;
    }

    /**
     * Set win to an opponent when user quit pvp battle
     *
     * @param string $token
     *
     * @param array $reward
     *
     * @return mixed
     */
    public function pvpQuit($token, $reward)
    {
        $opponent = $this->getRedisManager()->getOpponentToken($token);
        $battleCondition[$this->BATTLE_CONDITION_RESPONSE] = $this->getBattleCondition($opponent);
        $battleCondition[$this->BATTLE_CONDITION_RESPONSE][$this->REWARD] = $reward;
        $battleCondition[$this->OPPONENT] = $opponent;
        $battleCondition[$this->BATTLE_CONDITION_RESPONSE][$this->DID_WIN] = 1;
        $battleCondition[$this->BATTLE_CONDITION_RESPONSE][$this->TYPE] = $this->PVP_BATTLE_CONDITION;
        return $battleCondition;
    }
}
