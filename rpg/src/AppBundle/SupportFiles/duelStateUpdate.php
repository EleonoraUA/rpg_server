<?php

require dirname(dirname(dirname(dirname(__FILE__)))).'/vendor/autoload.php';

use Symfony\Component\Console\Application;

$application = new Application();

$application->add(new \AppBundle\Command\UpdateDuelCommand());

$application->run();
