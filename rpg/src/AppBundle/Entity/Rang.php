<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rang
 *
 * @ORM\Table(name="rang")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RangRepository")
 */
class Rang
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="lvl", type="integer", unique=true)
     */
    private $lvl;

    /**
     * @var int
     *
     * @ORM\Column(name="max_win_count", type="integer")
     */
    private $maxWinCount;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     *
     * @return Rang
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return int
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set maxWinCount
     *
     * @param integer $maxWinCount
     *
     * @return Rang
     */
    public function setMaxWinCount($maxWinCount)
    {
        $this->maxWinCount = $maxWinCount;

        return $this;
    }

    /**
     * Get maxWinCount
     *
     * @return int
     */
    public function getMaxWinCount()
    {
        return $this->maxWinCount;
    }
}
