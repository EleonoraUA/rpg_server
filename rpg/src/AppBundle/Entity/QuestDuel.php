<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QuestDuel
 *
 * @ORM\Table(name="quest_duel")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestDuelRepository")
 */
class QuestDuel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Quest
     * @ORM\OneToOne(targetEntity="Quest")
     * @ORM\JoinColumn(name="quest_id", referencedColumnName="id")
     */
    private $quest;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quest
     *
     * @param Quest $quest
     *
     * @return Quest
     */
    public function setQuest($quest)
    {
        $this->quest = $quest;

        return $this;
    }

    /**
     * Get quest
     *
     * @return Quest
     */
    public function getQuest()
    {
        return $this->quest;
    }
}
