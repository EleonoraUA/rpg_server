<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClearingStage
 *
 * @ORM\Table(name="clearing_stage")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClearingStageRepository")
 */
class ClearingStage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Personage
     *
     * @ORM\ManyToOne(targetEntity="Personage")
     * @ORM\JoinColumn(name="char_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $personage;

    /**
     * @var Stage
     *
     * @ORM\ManyToOne(targetEntity="Stage")
     * @ORM\JoinColumn(name="stage_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $stage;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_cleared", type="boolean")
     */
    private $isCleared;

    /**
     * Constructor
     *
     * @param Stage $stage
     *
     * @param Personage $personage
     *
     * @param bool $isCleared
     */
    public function __construct($stage, $personage, $isCleared)
    {
        $this->personage = $personage;
        $this->stage = $stage;
        $this->isCleared = $isCleared;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isCleared
     *
     * @param boolean $isCleared
     *
     * @return ClearingStage
     */
    public function setIsCleared($isCleared)
    {
        $this->isCleared = $isCleared;

        return $this;
    }

    /**
     * Get isCleared
     *
     * @return boolean
     */
    public function getIsCleared()
    {
        return $this->isCleared;
    }

    /**
     * Set personage
     *
     * @param \AppBundle\Entity\Personage $personage
     *
     * @return ClearingStage
     */
    public function setPersonage(\AppBundle\Entity\Personage $personage = null)
    {
        $this->personage = $personage;

        return $this;
    }

    /**
     * Get personage
     *
     * @return \AppBundle\Entity\Personage
     */
    public function getPersonage()
    {
        return $this->personage;
    }

    /**
     * Set stage
     *
     * @param \AppBundle\Entity\Stage $stage
     *
     * @return ClearingStage
     */
    public function setStage(\AppBundle\Entity\Stage $stage = null)
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * Get stage
     *
     * @return \AppBundle\Entity\Stage
     */
    public function getStage()
    {
        return $this->stage;
    }
}
