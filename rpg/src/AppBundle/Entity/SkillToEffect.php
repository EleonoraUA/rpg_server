<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SkillToEffect
 *
 * @ORM\Table(name="skill_to_effect")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SkillToEffectRepository")
 */
class SkillToEffect
{
    /**
    * @var int
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Skill")
     * @ORM\JoinColumn(name="skill_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $skill;

    /**
     * @ORM\ManyToOne(targetEntity="SkillEffect")
     * @ORM\JoinColumn(name="effect_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $effect;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set skill
     *
     * @param \AppBundle\Entity\Personage $skill
     *
     * @return SkillToEffect
     */
    public function setSkill(\AppBundle\Entity\Personage $skill = null)
    {
        $this->skill = $skill;

        return $this;
    }

    /**
     * Get skill
     *
     * @return \AppBundle\Entity\Personage
     */
    public function getSkill()
    {
        return $this->skill;
    }

    /**
     * Set effect
     *
     * @param \AppBundle\Entity\Skill $effect
     *
     * @return SkillToEffect
     */
    public function setEffect(\AppBundle\Entity\Skill $effect = null)
    {
        $this->effect = $effect;

        return $this;
    }

    /**
     * Get effect
     *
     * @return \AppBundle\Entity\Skill
     */
    public function getEffect()
    {
        return $this->effect;
    }
}
