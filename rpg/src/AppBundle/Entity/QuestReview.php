<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Traits\QuestReviewEntityTrait;

/**
 * QuestReview
 *
 * @ORM\Table(name="quest_review")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestReviewRepository")
 */
class QuestReview
{
    use QuestReviewEntityTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserToQuest")
     * @ORM\JoinColumn(name="user_to_quest", referencedColumnName="id")
     */
    private $userToQuest;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userToQuest
     *
     * @param UserToQuest $userToQuest
     *
     * @return QuestReview
     */
    public function setUserToQuest($userToQuest)
    {
        $this->userToQuest = $userToQuest;

        return $this;
    }

    /**
     * Get userToQuest
     *
     * @return UserToQuest
     */
    public function getUserToQuest()
    {
        return $this->userToQuest;
    }
}
