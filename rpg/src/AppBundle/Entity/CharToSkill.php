<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CharToSkill
 *
 * @ORM\Table(name="char_to_skill")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CharToSkillRepository")
 */
class CharToSkill
{
    /**
    * @var int
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Personage")
     * @ORM\JoinColumn(name="char_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $char;

    /**
     * @ORM\ManyToOne(targetEntity="Skill")
     * @ORM\JoinColumn(name="skill_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $skill;

    /**
     * @ORM\Column(name="isSelected", type="boolean")
     */
    private $isSelected;

    public function __construct($character, $skill, $isSelected = false)
    {
        $this->setChar($character);
        $this->setSkill($skill);
        $this->setIsSelected($isSelected);
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isSelected
     *
     * @param boolean $isSelected
     *
     * @return CharToSkill
     */
    public function setIsSelected($isSelected)
    {
        $this->isSelected = $isSelected;

        return $this;
    }

    /**
     * Get isSelected
     *
     * @return boolean
     */
    public function getIsSelected()
    {
        return $this->isSelected;
    }

    /**
     * Set char
     *
     * @param \AppBundle\Entity\Personage $char
     *
     * @return CharToSkill
     */
    public function setChar(\AppBundle\Entity\Personage $char = null)
    {
        $this->char = $char;

        return $this;
    }

    /**
     * Get char
     *
     * @return \AppBundle\Entity\Personage
     */
    public function getChar()
    {
        return $this->char;
    }

    /**
     * Set skill
     *
     * @param \AppBundle\Entity\Skill $skill
     *
     * @return CharToSkill
     */
    public function setSkill(\AppBundle\Entity\Skill $skill = null)
    {
        $this->skill = $skill;

        return $this;
    }

    /**
     * Get skill
     *
     * @return \AppBundle\Entity\Skill
     */
    public function getSkill()
    {
        return $this->skill;
    }
}
