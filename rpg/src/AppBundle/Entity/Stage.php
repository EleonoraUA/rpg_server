<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stage
 *
 * @ORM\Table(name="stage")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StageRepository")
 */
class Stage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=40)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_boss", type="boolean")
     */
    private $isBoss;

    /**
     * @ORM\ManyToMany(targetEntity="Monster")
     * @ORM\JoinTable(name="stage_to_monster",
     *      joinColumns={@ORM\JoinColumn(name="stage_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="monster_id", referencedColumnName="id")}
     *      )
     */
    private $monsters;

    /**
     * @var int
     *
     * @ORM\Column(name="min_exp", type="integer")
     */
    private $minExp;

    /**
     * @var int
     *
     * @ORM\Column(name="max_exp", type="integer")
     */
    private $maxExp;

    /**
     * @ORM\ManyToOne(targetEntity="Location")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $location;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Stage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isBoss
     *
     * @param boolean $isBoss
     *
     * @return Stage
     */
    public function setIsBoss($isBoss)
    {
        $this->isBoss = $isBoss;

        return $this;
    }

    /**
     * Get isBoss
     *
     * @return boolean
     */
    public function getIsBoss()
    {
        return $this->isBoss;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->monsters = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add monster
     *
     * @param \AppBundle\Entity\Monster $monster
     *
     * @return Stage
     */
    public function addMonster(\AppBundle\Entity\Monster $monster)
    {
        $this->monsters[] = $monster;

        return $this;
    }

    /**
     * Remove monster
     *
     * @param \AppBundle\Entity\Monster $monster
     */
    public function removeMonster(\AppBundle\Entity\Monster $monster)
    {
        $this->monsters->removeElement($monster);
    }

    /**
     * Get monsters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonsters()
    {
        return $this->monsters;
    }

    /**
     * Set minExp
     *
     * @param integer $minExp
     *
     * @return Stage
     */
    public function setMinExp($minExp)
    {
        $this->minExp = $minExp;

        return $this;
    }

    /**
     * Get minExp
     *
     * @return integer
     */
    public function getMinExp()
    {
        return $this->minExp;
    }

    /**
     * Set maxExp
     *
     * @param integer $maxExp
     *
     * @return Stage
     */
    public function setMaxExp($maxExp)
    {
        $this->maxExp = $maxExp;

        return $this;
    }

    /**
     * Get maxExp
     *
     * @return integer
     */
    public function getMaxExp()
    {
        return $this->maxExp;
    }

    /**
     * Set location
     *
     * @param \AppBundle\Entity\Location $location
     *
     * @return Stage
     */
    public function setLocation(\AppBundle\Entity\Location $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \AppBundle\Entity\Location
     */
    public function getLocation()
    {
        return $this->location;
    }
}
