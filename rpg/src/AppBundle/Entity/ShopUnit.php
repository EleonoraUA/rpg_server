<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Doctrine\ORM\Mapping as ORM;

/**
 * ShopUnit
 *
 * @ORM\Table(name="shop_unit")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ShopUnitRepository")
 */
class ShopUnit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=25)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=20)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="count", type="integer")
     */
    private $count;

    /**
     * @var string
     *
     * @ORM\Column(name="price_type", type="string", length=20)
     */
    private $priceType;

    /**
     * @var int
     *
     * @ORM\Column(name="price_count", type="integer")
     */
    private $priceCount;

    /**
     * @ORM\ManyToOne(targetEntity="Skill")
     * @ORM\JoinColumn(name="skill_id", referencedColumnName="id")
     */
    private $skill;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_one_time", type="boolean")
     */
    private $isOneTime;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ShopUnit
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ShopUnit
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set count
     *
     * @param int $count
     *
     * @return ShopUnit
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set priceType
     *
     * @param string $priceType
     *
     * @return ShopUnit
     */
    public function setPriceType($priceType)
    {
        $this->priceType = $priceType;

        return $this;
    }

    /**
     * Get priceType
     *
     * @return string
     */
    public function getPriceType()
    {
        return $this->priceType;
    }

    /**
     * Set priceCount
     *
     * @param int $priceCount
     *
     * @return ShopUnit
     */
    public function setPriceCount($priceCount)
    {
        $this->priceCount = $priceCount;

        return $this;
    }

    /**
     * Get priceCount
     *
     * @return \int
     */
    public function getPriceCount()
    {
        return $this->priceCount;
    }

    /**
     * Set skill
     *
     * @param \AppBundle\Entity\Skill $skill
     *
     * @return ShopUnit
     */
    public function setSkillId(\AppBundle\Entity\Skill $skill = null)
    {
        $this->skill = $skill;

        return $this;
    }

    /**
     * Get skill
     *
     * @return \AppBundle\Entity\Skill
     */
    public function getSkill()
    {
        return $this->skill;
    }

    /**
     * Set isOneTime
     *
     * @param boolean $isOneTime
     *
     * @return ShopUnit
     */
    public function setIsOneTime($isOneTime)
    {
        $this->isOneTime = $isOneTime;

        return $this;
    }

    /**
     * Get isOneTime
     *
     * @return boolean
     */
    public function getIsOneTime()
    {
        return $this->isOneTime;
    }

    /**
     * Set skill
     *
     * @param \AppBundle\Entity\Skill $skill
     *
     * @return ShopUnit
     */
    public function setSkill(\AppBundle\Entity\Skill $skill = null)
    {
        $this->skill = $skill;

        return $this;
    }
}
