<?php
namespace AppBundle\Entity\Traits;

use AppBundle\Entity\Quest;
use Doctrine\ORM\Mapping as ORM;

trait QuestRequestEntityTrait
{
    /**
     * @ORM\ManyToOne(targetEntity="Quest")
     * @ORM\JoinColumn(name="quest_id", referencedColumnName="id")
     */
    private $quest;

    /**
     * @var int
     *
     * @ORM\Column(name="state", type="integer")
     */
    private $state;

    /**
     * @var int
     *
     * @ORM\Column(name="review_count", type="integer")
     */
    private $reviewCount;

    /**
     * @var bool
     *
     * @ORM\Column(name="got_reward", type="boolean")
     */
    private $gotReward;

    /**
     * Set quest
     *
     * @param Quest $quest
     */
    public function setQuest($quest)
    {
        $this->quest = $quest;
    }

    /**
     * Get quest
     *
     * @return Quest
     */
    public function getQuest()
    {
        return $this->quest;
    }

    /**
     * Set state
     *
     * @param integer $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * Get state
     *
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }


    /**
     * Get rewiewCount
     *
     * @return int
     */
    public function getReviewCount()
    {
        return $this->reviewCount;
    }

    /**
     * Set rewiewCount
     *
     * @param int $reviewCount
     */
    public function setReviewCount($reviewCount)
    {
        $this->reviewCount = $reviewCount;
    }

    /**
     * @return boolean
     */
    public function hasGotReward()
    {
        return $this->gotReward;
    }

    /**
     * @param boolean $gotReward
     */
    public function setGotReward($gotReward)
    {
        $this->gotReward = $gotReward;
    }

    public function updateReviewCount()
    {
        $this->reviewCount += 1;
    }
}
