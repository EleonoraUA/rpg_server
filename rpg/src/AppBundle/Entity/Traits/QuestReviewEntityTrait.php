<?php

namespace AppBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait QuestReviewEntityTrait
{
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="reviewer", referencedColumnName="id")
     */
    private $reviewer;

    /**
     * @var bool
     *
     * @ORM\Column(name="result", type="boolean", nullable=true)
     */
    private $result;

    /**
     * Set reviewer
     *
     * @return
     */
    public function setReviewer($reviewer)
    {
        $this->reviewer = $reviewer;

        return $this;
    }

    /**
     * Get reviewer
     */
    public function getReviewer()
    {
        return $this->reviewer;
    }

    /**
     * Set result
     *
     * @param boolean $result
     *
     * @return bool
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return bool
     */
    public function getResult()
    {
        return $this->result;
    }
}
