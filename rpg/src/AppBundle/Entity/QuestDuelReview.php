<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Traits\QuestReviewEntityTrait;

/**
 * QuestDuelReview
 *
 * @ORM\Table(name="quest_duel_review")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestDuelReviewRepository")
 */
class QuestDuelReview
{
    use QuestReviewEntityTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="QuestDuelRequest")
     * @ORM\JoinColumn(name="quest_duel_request", referencedColumnName="id")
     */
    private $questDuelRequest;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set questDuelRequest
     *
     * @param QuestDuelRequest $questDuelRequest
     *
     * @return QuestDuelRequest
     */
    public function setQuestDuelRequest($questDuelRequest)
    {
        $this->questDuelRequest = $questDuelRequest;

        return $this;
    }

    /**
     * Get questDuelRequest
     *
     * @return QuestDuelRequest
     */
    public function getQuestDuelRequest()
    {
        return $this->questDuelRequest;
    }
}
