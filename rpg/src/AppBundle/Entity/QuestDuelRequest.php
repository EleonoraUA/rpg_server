<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \AppBundle\Entity\Traits\QuestRequestEntityTrait;

/**
 * QuestDuelRequest
 *
 * @ORM\Table(name="quest_duel_request", uniqueConstraints={@ORM\UniqueConstraint(name="duel_quest_unique",
 *     columns={"sender_id", "accepter_id", "quest_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestDuelRequestRepository")
 */
class QuestDuelRequest
{
    use QuestRequestEntityTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="sender_id", referencedColumnName="id")
     */
    private $sender;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="accepter_id", referencedColumnName="id")
     */
    private $accepter;

    /**
     * @var string
     *
     * @ORM\Column(name="accepted_at", type="datetime", nullable=true)
     */
    private $acceptedAt;

    /**
     * @var int
     *
     * @ORM\Column(name="days_left", type="integer", nullable=true)
     */
    private $daysLeft;

    /**
     * QuestDuelRequest constructor.
     * @param $sender
     * @param $accepter
     * @param $quest
     */
    public function __construct($quest, $sender, $accepter)
    {
        $this->sender = $sender;
        $this->accepter = $accepter;
        $this->quest = $quest;
        $this->state = 0;
        $this->acceptedAt = null;
        $this->daysLeft = null;
        $this->reviewCount = 0;
        $this->gotReward = false;
    }

    /**
     * @return int
     */
    public function getDaysLeft()
    {
        return $this->daysLeft;
    }

    /**
     * @param int $daysLeft
     */
    public function setDaysLeft($daysLeft)
    {
        $this->daysLeft = $daysLeft;
    }

    /**
     * @return string
     */
    public function getAcceptedAt()
    {
        return $this->acceptedAt;
    }

    /**
     * @param string $acceptedAt
     */
    public function setAcceptedAt($acceptedAt)
    {
        $this->acceptedAt = $acceptedAt;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sender
     *
     * @param User $sender
     *
     * @return QuestDuelRequest
     */
    public function setSender($sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set accepter
     *
     * @param User $accepter
     *
     * @return QuestDuelRequest
     */
    public function setAccepter($accepter)
    {
        $this->accepter = $accepter;

        return $this;
    }

    /**
     * Get accepter
     *
     * @return User
     */
    public function getAccepter()
    {
        return $this->accepter;
    }
}
