<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\Extension\Core\DataTransformer\PercentToLocalizedStringTransformer;

/**
 * Purchase
 *
 * @ORM\Table(name="purchase")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PurchaseRepository")
 */
class Purchase
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Personage")
     * @ORM\JoinColumn(name="char_id", referencedColumnName="id")
     */
    private $personage;

    /**
     * @ORM\ManyToOne(targetEntity="ShopUnit")
     * @ORM\JoinColumn(name="unit_id", referencedColumnName="id")
     */
    private $shopUnit;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * Set personage
     *
     * @param \AppBundle\Entity\Personage $personage
     *
     * @return Purchase
     */
    public function setPersonage(\AppBundle\Entity\Personage $personage = null)
    {
        $this->personage = $personage;

        return $this;
    }

    /**
     * Get personage
     *
     * @return \AppBundle\Entity\Personage
     */
    public function getPersonage()
    {
        return $this->personage;
    }

    /**
     * Set shopUnit
     *
     * @param \AppBundle\Entity\ShopUnit $shopUnit
     *
     * @return Purchase
     */
    public function setShopUnit(\AppBundle\Entity\ShopUnit $shopUnit = null)
    {
        $this->shopUnit = $shopUnit;

        return $this;
    }

    /**
     * Get shopUnit
     *
     * @return \AppBundle\Entity\ShopUnit
     */
    public function getShopUnit()
    {
        return $this->shopUnit;
    }

    /**
     * Purchase constructor.
     * @param Personage $personage
     * @param ShopUnit $unit
     */
    public function __construct($personage, $unit)
    {
        $this->personage = $personage;
        $this->shopUnit = $unit;
    }
}
