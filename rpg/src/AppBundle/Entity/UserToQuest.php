<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \AppBundle\Entity\Traits\QuestRequestEntityTrait;

/**
 * UserToQuest
 *
 * @ORM\Table(
 *     name="user_to_quest",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="user_to_quest_unique",
 *     columns={"user_id", "state", "quest_id"})}
 *     )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserToQuestRepository")
 */
class UserToQuest
{
    use QuestRequestEntityTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @return UserToQuest
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return UserToQuest
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }
}
