<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Doctrine\ORM\Mapping as ORM;

/**
 * Account
 *
 * @ORM\Table(name="account")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AccountRepository")
 */
class Account
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="gold", type="integer", options={"default" : 0})
     */
    private $gold = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="crystals", type="integer", options={"default" : 0})
     */
    private $crystals = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="avatar_id", type="integer", options={"default" : 1})
     */
    private $avatarId = 1;
    
    /**
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gold
     *
     * @param integer $gold
     *
     * @return Account
     */
    public function setGold($gold)
    {
        $this->gold = $gold;

        return $this;
    }

    /**
     * Get avatarId
     *
     * @return int
     */
    public function getAvatarId()
    {
        return $this->avatarId;
    }

    /**
     * Set avatarId
     *
     * @param int $avatarId
     *
     * @return Account
     */
    public function setAvatarId($avatarId)
    {
        $this->avatarId = $avatarId;
        return $this;
    }

    /**
     * Get gold
     *
     * @return int
     */
    public function getGold()
    {
        return $this->gold;
    }

    /**
     * Set crystals
     *
     * @param integer $crystals
     *
     * @return Account
     */
    public function setCrystals($crystals)
    {
        $this->crystals = $crystals;

        return $this;
    }

    /**
     * Get crystals
     *
     * @return int
     */
    public function getCrystals()
    {
        return $this->crystals;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Account
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function __construct(User $user)
    {
        $this->setUser($user);
        return $this;
    }
}
