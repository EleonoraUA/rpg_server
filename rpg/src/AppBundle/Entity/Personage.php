<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Personage
 *
 * @ORM\Table(name="personage")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonageRepository")
 */
class Personage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=15)
     *
     * @Assert\Type("string")
     * @Assert\Length(min = 4, max = 15,
     *      minMessage = 45,
     *      maxMessage = 46,
     *      groups = {"registration"})
     * @Assert\Regex(pattern="/^[a-zA-Z][a-zA-Z0-9._]+$/", message=48, groups={"registration"})
     *
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="hp", type="integer")
     */
    private $hp;

    /**
     * @var int
     *
     * @ORM\Column(name="atk", type="integer")
     */
    private $atk;

    /**
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $account;

    /**
     * @ORM\ManyToOne(targetEntity="CharacterClass")
     * @ORM\JoinColumn(name="class_id", referencedColumnName="id")
     */
    private $class;

    /**
     * @var int
     *
     * @ORM\Column(name="lvl", type="integer", options={"default" : 1})
     */
    private $lvl = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="exp", type="integer", options={"default" : 0})
     */
    private $exp = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="bag_size", type="integer")
     */
    private $bagSize = 8;
    
    /**
     * @var int
     *
     * @ORM\Column(name="active_skills_bag_size", type="integer")
     */
    private $activeSkillsBagSize = 3;

    /**
     * @return int
     */
    public function getBagSize()
    {
        return $this->bagSize;
    }

    /**
     * @param int $bagSize
     */
    public function setBagSize($bagSize)
    {
        $this->bagSize = $bagSize;
    }

    /**
     * @return int
     */
    public function getActiveSkillsBagSize()
    {
        return $this->activeSkillsBagSize;
    }

    /**
     * @param int $activeSkillsBagSize
     */
    public function setActiveSkillsBagSize($activeSkillsBagSize)
    {
        $this->activeSkillsBagSize = $activeSkillsBagSize;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Personage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set hp
     *
     * @param integer $hp
     *
     * @return Personage
     */
    public function setHp($hp)
    {
        $this->hp = $hp;

        return $this;
    }

    /**
     * Get hp
     *
     * @return int
     */
    public function getHp()
    {
        return $this->hp;
    }

    /**
     * Set atk
     *
     * @param integer $atk
     *
     * @return Personage
     */
    public function setAtk($atk)
    {
        $this->atk = $atk;

        return $this;
    }

    /**
     * Get atk
     *
     * @return int
     */
    public function getAtk()
    {
        return $this->atk;
    }

    /**
     * Set account
     *
     * @param \AppBundle\Entity\Account $account
     *
     * @return Personage
     */
    public function setAccount(\AppBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \AppBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    public function __construct(Account $account, $name, $class)
    {
        $this->setAccount($account);
        $this->setName($name);
        $this->setClass($class);
        return $this;
    }

    /**
     * Set class
     *
     * @param \AppBundle\Entity\CharacterClass $class
     *
     * @return Personage
     */
    public function setClass(\AppBundle\Entity\CharacterClass $class = null)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get class
     *
     * @return \AppBundle\Entity\CharacterClass
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     *
     * @return Personage
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set exp
     *
     * @param integer $exp
     *
     * @return Personage
     */
    public function setExp($exp)
    {
        $this->exp = $exp;

        return $this;
    }

    /**
     * Get exp
     *
     * @return integer
     */
    public function getExp()
    {
        return $this->exp;
    }
}
