<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tournament
 *
 * @ORM\Table(name="tournament")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TournamentRepository")
 */
class Tournament
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Personage")
     * @ORM\JoinColumn(name="char_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $personage;

    /**
     * @var int
     *
     * @ORM\Column(name="win_count", type="integer")
     */
    private $winCount;

    /**
     * @var int
     *
     * @ORM\Column(name="total_amount", type="integer")
     */
    private $totalAmount;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Rang")
     * @ORM\JoinColumn(name="best_rang", referencedColumnName="id")
     */
    private $bestRang;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set winCount
     *
     * @param integer $winCount
     *
     * @return Tournament
     */
    public function setWinCount($winCount)
    {
        $this->winCount = $winCount;

        return $this;
    }

    /**
     * Get winCount
     *
     * @return integer
     */
    public function getWinCount()
    {
        return $this->winCount;
    }

    /**
     * Set totalAmount
     *
     * @param integer $totalAmount
     *
     * @return Tournament
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    /**
     * Get totalAmount
     *
     * @return integer
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * Set personage
     *
     * @param \AppBundle\Entity\Personage $personage
     *
     * @return Tournament
     */
    public function setPersonage(\AppBundle\Entity\Personage $personage = null)
    {
        $this->personage = $personage;

        return $this;
    }

    /**
     * Get personage
     *
     * @return \AppBundle\Entity\Personage
     */
    public function getPersonage()
    {
        return $this->personage;
    }

    /**
     * Set bestRang
     *
     * @param \AppBundle\Entity\Rang $bestRang
     *
     * @return Tournament
     */
    public function setBestRang(\AppBundle\Entity\Rang $bestRang = null)
    {
        $this->bestRang = $bestRang;

        return $this;
    }

    /**
     * Get bestRang
     *
     * @return \AppBundle\Entity\Rang
     */
    public function getBestRang()
    {
        return $this->bestRang;
    }
}
