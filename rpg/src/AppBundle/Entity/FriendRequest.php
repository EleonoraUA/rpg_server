<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FriendRequest
 *
 * @ORM\Table(name="friend_request",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="friend_request_unique",
 *     columns={"sender", "accepter"})}
 *     )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FriendRequestRepository")
 */
class FriendRequest
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="sender", referencedColumnName="id")
     */
    private $sender;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="accepter", referencedColumnName="id")
     */
    private $accepter;

    /**
     * @var int
     *
     * @ORM\Column(name="state", type="integer")
     */
    private $state;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sender
     *
     * @param User $sender
     *
     * @return FriendRequest
     */
    public function setSender($sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set accepter
     *
     * @param User $accepter
     *
     * @return FriendRequest
     */
    public function setAccepter($accepter)
    {
        $this->accepter = $accepter;

        return $this;
    }

    /**
     * Get accepter
     *
     * @return User
     */
    public function getAccepter()
    {
        return $this->accepter;
    }

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return FriendRequest
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }
}
