<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reward
 *
 * @ORM\Table(name="quest_reward")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RewardRepository")
 */
class QuestReward
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="gold", type="integer")
     */
    private $gold;

    /**
     * @var int
     *
     * @ORM\Column(name="crystals", type="integer")
     */
    private $crystals;

    /**
     * @var int
     *
     * @ORM\Column(name="skill_id", type="integer", nullable=true)
     */
    private $skillId;

    /**
     * QuestReward constructor.
     * @param int $gold
     * @param int $crystals
     * @param int $skillId
     */
    public function __construct($gold, $crystals, $skillId)
    {
        $this->gold = $gold;
        $this->crystals = $crystals;
        $this->skillId = $skillId;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gold
     *
     * @param integer $gold
     *
     * @return QuestReward
     */
    public function setGold($gold)
    {
        $this->gold = $gold;

        return $this;
    }

    /**
     * Get gold
     *
     * @return int
     */
    public function getGold()
    {
        return $this->gold;
    }

    /**
     * Set crystals
     *
     * @param integer $crystals
     *
     * @return QuestReward
     */
    public function setCrystals($crystals)
    {
        $this->crystals = $crystals;

        return $this;
    }

    /**
     * Get crystals
     *
     * @return int
     */
    public function getCrystals()
    {
        return $this->crystals;
    }

    /**
     * Set skillId
     *
     * @param integer $skillId
     *
     * @return QuestReward
     */
    public function setSkillId($skillId)
    {
        $this->skillId = $skillId;

        return $this;
    }

    /**
     * Get skillId
     *
     * @return int
     */
    public function getSkillId()
    {
        return $this->skillId;
    }
}
