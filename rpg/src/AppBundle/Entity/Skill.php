<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Skill
 *
 * @ORM\Table(name="skill")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SkillRepository")
 */
class Skill
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=20, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="multiplier", type="float")
     */
    private $multiplier;

    /**
     * @var int
     *
     * @ORM\Column(name="cd", type="integer")
     */
    private $cd;

    /**
     * @var array
     */
    private $effects;

    /**
     * @var int
     *
     * @ORM\Column(name="required_lvl", type="integer")
     */
    private $required_lvl;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Skill
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Skill
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set multiplier
     *
     * @param float $multiplier
     *
     * @return Skill
     */
    public function setMultiplier($multiplier)
    {
        $this->multiplier = $multiplier;

        return $this;
    }

    /**
     * Get multiplier
     *
     * @return float
     */
    public function getMultiplier()
    {
        return $this->multiplier;
    }

    /**
     * Set cd
     *
     * @param integer $cd
     *
     * @return Skill
     */
    public function setCd($cd)
    {
        $this->cd = $cd;

        return $this;
    }

    /**
     * Get cd
     *
     * @return int
     */
    public function getCd()
    {
        return $this->cd;
    }

    /**
     * Set requiredLvl
     *
     * @param integer $requiredLvl
     *
     * @return Skill
     */
    public function setRequiredLvl($requiredLvl)
    {
        $this->required_lvl = $requiredLvl;

        return $this;
    }

    /**
     * Get requiredLvl
     *
     * @return integer
     */
    public function getRequiredLvl()
    {
        return $this->required_lvl;
    }

    public function getEffects()
    {
        global $kernel;
        return $kernel->getContainer()->get('app.manager.skillapi')->getEffectsForSkill($this->getId());
    }

    public function __construct()
    {
        $this->effects = array();
    }
}
