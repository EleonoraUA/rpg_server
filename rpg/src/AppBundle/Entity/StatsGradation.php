<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Doctrine\ORM\Mapping as ORM;

/**
 * StatsGradation
 *
 * @ORM\Table(name="stats_gradation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExperienceRepository")
 */
class StatsGradation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="lvl", type="integer")
     */
    private $lvl;

    /**
     * @var int
     *
     * @ORM\Column(name="atk", type="integer")
     */
    private $atk;

    /**
     * @var int
     *
     * @ORM\Column(name="hp", type="integer")
     */
    private $hp;

    /**
     * @ORM\ManyToOne(targetEntity="CharacterClass")
     * @ORM\JoinColumn(name="class_id", referencedColumnName="id")
     */
    private $class;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     *
     * @return StatsGradation
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl()
    {
        return $this->lvl;
    }


    /**
     * Set atk
     *
     * @param integer $atk
     *
     * @return StatsGradation
     */
    public function setAtk($atk)
    {
        $this->atk = $atk;

        return $this;
    }

    /**
     * Get atk
     *
     * @return integer
     */
    public function getAtk()
    {
        return $this->atk;
    }

    /**
     * Set hp
     *
     * @param integer $hp
     *
     * @return StatsGradation
     */
    public function setHp($hp)
    {
        $this->hp = $hp;

        return $this;
    }

    /**
     * Get hp
     *
     * @return integer
     */
    public function getHp()
    {
        return $this->hp;
    }

    /**
     * Set class
     *
     * @param \AppBundle\Entity\CharacterClass $class
     *
     * @return StatsGradation
     */
    public function setClass(\AppBundle\Entity\CharacterClass $class = null)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get class
     *
     * @return \AppBundle\Entity\CharacterClass
     */
    public function getClass()
    {
        return $this->class;
    }
}
