## Протокол (Клиент-Сервер)
### ER diagram
![Alt text](https://gitlab.com/EleonoraUA/rpg_server/raw/3b3cedc387ae53e35bd4e131fe3fbc3f72a40533/er.png "ER DIAGRAM")
# RPG
###### Формат:JSON
-----------------------------------
#### Login/Logout/Register API
-----------------------------------
### Вход (/login)   
#### POST
##### USER_LOGIN
Клиент отправляет запрос на вход
```
{
  "email": string,
  "password" : string
}
```
Сервер возвращает инфу об юзере
```
{
  "status": int,
  "token": *,
  "username": string,
  "characters":[
        {
            "char_id": int,
            "name": string,
            "class_id": int,
            "avatar_id": int,
            "skills": [
                int
            ]
        }
    ]
}
```
-----------------------------------
### Выход (/signout)
#### POST
##### USER_LOGOUT
Клиент отправляет запрос на выход
```
{
  "token": *
}
```
Сервер возвращает:
```
{
    "status": int
}
```
-----------------------------------
### Проверка существования токена (/token_exists)
#### POST
##### TOKEN_EXISTS
Клиент отправляет запрос на проверку токена
```
{
  "token": *
}
```
Сервер возвращает:
```
{
    "status": int
}
```
-----------------------------------
### Регистрация пользователя (/register)
#### POST
##### USER_REGISTER
Клиент отправляет запрос на регистрацию
```
{
  "username": string,
  "email": string,
  "password" : string,
  "character":{
        "name": string,
        "class_id": int,
        "avatar_id": int
    }
}
```
Сервер возвращает статус успешно или не успешно пользователь зарегестрирован
```
{
    "status": int
}
```
-----------------------------------
### Принужденный выход (/forcequit)
#### POST
##### USER_LOGOUT
Клиент отправляет токен
```
{
  "token": *
}
```
Сервер возвращает статус
```
{
    "status": int
}
```
-----------------------------------
### Получение ресурсов (золото, кристаллы) (/resources)
#### POST
##### RESOURCES_REQUEST
Клиент отправляет запрос на получение ресурсов
```
{
  "token": *
}
```
Сервер возвращает ресурсы юзера
```
{
    "status": int,
    "resources":{
        "gold": int,
        "crystals":int
    }
}
```
-----------------------------------
#### CHARACTER API
-----------------------------------
### Получение статистики юзера (уровень, опыт) (/char_profile)
#### POST
##### CHAR_EXPERIENCE_REQUEST
Клиент отправляет запрос на получение 
```
{
  "token": *,
  "char_id": int
}
```
Сервер возвращает
```
{
    "status": int,
    "current_level": int,
    "max_exp": int,
    "current_exp": int,
    "attack": int,
    "hp": int,
    "bag_size": int,
    "active_skills_bag_size": int,
    "skills": [
        {
            "skill_id": int,
            "is_selected": bool
        }
    ]
}
```
### Изменение аватара пользователя(/change_avatar)
#### POST
##### CHANGE_AVATAR_REQUEST
Клиент отправляет запрос на смену аватара
```
{
  "token": *,
  "avatar_id": int
}
```
Сервер возвращает
```
{
    "status": int
}
```
-----------------------------------
#### SKILLS API
-----------------------------------
### Скиллы персонажа (/skills)
#### POST
##### SKILLS_REQUEST
Запрос на скиллы персонажа
```
{
    "token": *,
    "char_id": int
}
```
Сервер возвращает скиллы персонажа
```
{
    "status": int,
    "skills": [
        {
            "skill_id": int
            "is_selected": bool
        }
    ]
}
```
### Скиллы (/skill/{skill_id})
#### GET
##### SKILL_REQUEST
Сервер возвращает конкретный скилл по skill_id
```
{ 
    "status": int,
    "skill": {
            "name": string,
            "description": string,
            "multiplier": float
        }
}
```
### Выбранные скиллы персонажа (/select_skills)
#### POST
##### SELECT_SKILLS_REQUEST
Запрос на выбор скиллов персонажа
```
{
    "token": *,
    "char_id": int,
    "skills": [ 
        int 
    ]
}
```
Сервер возвращает статус
```
{
    "status": int
}
```
-----------------------------------
#### CLASS API
-----------------------------------
### Классы (/classes)
#### GET
##### CLASSES_REQUEST
Сервер возвращает все классы
```
{
    "status": int,
    "classes": [
        {
            "class_id": int
        }
    ]
}
```
### Класс персонажа (/class/{class_id})
#### GET
##### CLASS_REQUEST
Сервер возвращает конкретный класс по class_id
```
{
    "status": int,
    "class": {
        "name": string,
        "description": string
    }
}
```
-----------------------------------
#### LOCATION API
-----------------------------------
### Коды состояний 
```
IS_CLEARED = 0;
IS_AVAILABLE = 1;
IS_NOT_AVAILABLE = 2;

```
-----------------------------------
### Локации доступные для пользователя(/locations)
#### POST
##### LOCATIONS_REQUEST
Запрос на доступные локации
```
{
    "token": *
}
```
Сервер возвращает доступные локации
```
{
    "status": int,
    "locations": [int]
}
```
-----------------------------------
### Получить информацию о локации(/location_info)
#### POST
##### LOCATION_INFO_REQUEST
Запрос на получение информации 
```
{
    "token": *,
    "location_id": int
}
```
Сервер возвращает 
```
{
    "status": int,
    "location_info": [
        {
            "battle_place_id": int,
            "state": int
        }
    ]
}
```
-----------------------------------
#### QUESTS API
-----------------------------------
### Коды состояний квестов
```
EMPTY_STATE = 0;
IN_PROGRESS = 1;
IS_DONE = 2;
REVIEWED_TRUE = 3;
REVIEWED_FALSE = 4;
SKIPPED = 5;
```
-----------------------------------
### Квесты доступные для выполения (/quests)
#### POST
##### QUESTS_REQUEST
Запрос на доступные для выполнения квесты
```
{
    "token": *
}
```
Сервер возвращает юзеру доступные для выполнения квесты
```
{
    "status": int,
    "quests": [
        {
            "quest_id": int,
            "name": string,
            "description": string,
            "lvl": int,
            "state": int,
            "prove_image_1": string,
            "reward": {
                    "gold": int,
                    "crystals":int,
                    "skill_id": int
            }
        }
    ]
}
```
-----------------------------------
### Проверка квестов других пользователей (/review_quests)
#### POST
##### REVIEW_QUESTS_REQUEST
Запрос на доступные для проверки квесты
```
{
    "token": *
}
```
Сервер возвращает юзеру все доступные для проверки квесты
```
{
    "status": int,
    "quests": [
        {
            "quest_id": int,
            "name": string,
            "description": string,
            "state": int,
            "prove_image_1": string,
            "reward": {
                    "gold": int,
                    "crystals":int,
                    "skill_id": int
            }
        }
    ]
}
```
-----------------------------------
### Принять квест (/accept_quest)
#### POST
##### TAKE_QUEST
Запрос на принятие квеста
```
{
    "token": *,
    "quest_id": int
}
```
Сервер возвращает юзеру статус ОК или код ошибки
```
{
    "status": int
}
```
-----------------------------------
### Отказ от квеста (/skip_quest)
#### POST
##### TAKE_QUEST
Запрос на отказ от квеста
```
{
    "token": *,
    "quest_id": int
}
```
Сервер возвращает юзеру статус ОК или код ошибки
```
{
    "status": int
}
```
-----------------------------------
### Квесты в процессе выполнения (/in_progress_quests)
#### POST
##### INPROGRESS_QUESTS_REQUEST
Запрос на квесты, которые в процессе выполнения
```
{
    "token": *
}
```
Сервер возвращает юзеру все квесты, которые в процессе выполнения
```
{
    "status": int,
    "quests": [
        {
            "quest_id": int,
            "name": string,
            "description": string,
            "prove_image_1": string,
            "lvl": int,
            "reward":{
                    "gold": int,
                    "crystals":int
                    "skill_id": int
            }
            "state": int
        }
    ]
}
```
-----------------------------------
### Сделанные квесты (/confirmed_quests)
#### POST
##### CONFIRMED_QUESTS
Запрос на сделанные и проверенные квесты
```
{
    "token": *
}
```
Сервер возвращает юзеру сделанные и проверенные квесты
```
{
    "status": int,
    "quests": [
        {
            "quest_id": int,
            "name": string,
            "description": string,
            "prove_image_1": string,
            "got_reward": bool,
            "reward":{
                    "gold": int,
                    "crystals":int
                    "skill_id": int
            }
        }
    ]
}
```
----------------------------------
### Начисление скилла за квест (/get_quest_reward)
#### POST
##### GET_QUEST_REWARD
```
{
    "token": *,
    "quest_id": int
}
```
Сервер возвращает:
```
{
    "status": int
}
```
----------------------------------
### Результат проверки квеста (/review_result)
#### POST
##### REVIEW_RESULT
Клиент отправляет запрос с результатом проверки квеста (true/false)
```
{
    "token": *,
    "result": bool
}
```
Сервер возвращает:
```
{
    "status": int
}
```
----------------------------------
### Пруф Сделанного квеста (/prove_quest)
#### POST
##### DONE_QUEST_PROVE
Клиент отправляет запрос с фоткой (подтверждением) квеста, по ключу 'prove_image' - картинка, по ключу 'json' - json
```
'prove_image' = file
'json' = {
            "token": *,
            "quest_id": int,
        }
```
Сервер возвращает:
```
{
    "status": int
}
```
-----------------------------------
#### DUEL QUESTS API
-----------------------------------
### Коды состояний квестов
```
OUTCOMING_DUEL_REQUEST = 7; //пользователь отправил заявку
INCOMING_DUEL_REQUEST = 8; //пользователю отправили заявку
IN_PROGRESS = 1; //пользователь принял заявку или только один с юзеров скинул пруф
IS_DONE = 2; // оба юзера скинули пруф
SKIPPED = 5; //юзер скипнул заявку или заявку пользователя скипнул его опонент
SENDER_WIN = 9;
ACCEPTER_WIN = 10;
```
-----------------------------------
### Отправить запрос на дуель квестом с другом (/duel_send_request)
#### POST
##### DUEL_QUEST_REQUEST
Клиент отправляет запрос на вызов друга на дуель
```
{
    "token": *,
    "friend_id": int
}
```
Сервер возвращает:
```
{
    "status": int
}
```
-----------------------------------
### Запрос на получение квестов (/duel_incoming_requests)
#### POST
##### GET_DUEL_QUESTS
Клиент отправляет запрос на получение квестов, которые брошены на дуель друзьями пользователя
```
{
    "token": *
}
```
Сервер возвращает:
```
{
    "status": int,
    "duel_quests": [{
            "friend_id": int,
            "username": string,
            "quest_id": int
        }
    ]
}
```
-----------------------------------
### Запрос на получение квестов (/duel_in_progress)
#### POST
##### GET_DUEL_QUESTS
Клиент отправляет запрос на получение квестов, которые приняты пользователем и на которые получен только один пруф
```
{
    "token": *
}
```
Сервер возвращает:
```
{
    "status": int,
    "duel_quests": [{
            "friend_id": int,
            "username": string,
            "days_left": int,
            "quest": {
                "quest_id": int,
                "name": string,
                "description": string,
                "state": int,
                "prove_image_1": string,
                "prove_image_2": string,
                "reward": {
                    "gold": int,
                    "crystals": int,
                    "skill_id": int
                }
            }
        }
    ]
}
```
-----------------------------------
### Запрос на получение квестов (/duel_confirmed)
#### POST
##### GET_DUEL_QUESTS
Клиент отправляет запрос на получение квестов, которые проверенные другими пользователями
```
{
    "token": *
}
```
Сервер возвращает:
```
{
    "status": int,
    "duel_quests": [{
            "friend_id": int,
            "username": string,
            "days_left": int,
            "quest": {
                "quest_id": int,
                "name": string,
                "description": string,
                "state": int,
                "prove_image_1": string,
                "prove_image_2": string,
                "got_reward": bool,
                "reward": {
                    "gold": int,
                    "crystals": int,
                    "skill_id": int
                }
            }
        }
    ]
}
```
-----------------------------------
### Принять квест на дуель (/duel_accept_request)
#### POST
##### TAKE_DUEL_QUEST
Клиент отправляет запрос на подтверждение дуели
```
{
    "token": *,
    "quest_id": int,
    "friend_id": int
}
```
Сервер возвращает юзеру статус ОК или код ошибки
```
{
    "status": int
}
```
-----------------------------------
### Отказ от квеста на дуель (/duel_skip_request)
#### POST
##### TAKE_DUEL_QUEST
Запрос на отказ от дуели
```
{
    "token": *
    "quest_id": int,
    "friend_id": int
}
```
Сервер возвращает юзеру статус ОК или код ошибки
```
{
    "status": int
}
```
-----------------------------------
### Пруф Сделанного квеста (/prove_duel)
#### POST
##### DONE_DUEL_PROVE
Клиент отправляет запрос с фоткой (подтверждением) квеста, по ключу 'prove_image' - картинка, по ключу 'json' - json
```
'prove_image' = file
'json' = {
            "token": *,
            "quest_id": int,
            "friend_id": int
        }
```
Сервер возвращает:
```
{
    "status": int
}
```
-----------------------------------
### Проверка квестов других пользователей (/review_duel_quests)
#### POST
##### REVIEW_QUESTS_REQUEST
Запрос на доступные для проверки квесты
```
{
    "token": *
}
```
Сервер возвращает юзеру все доступные для проверки квесты
```
{
    "status": int,
    "quests": [
        {
            "quest_id": int,
            "name": string,
            "description": string,
            "state": bool,
            "prove_image_1": string,
            "prove_image_2": string,
            "reward": {
                "gold": int,
                "crystals":int,
                "skill_id": int
            }
        }
    ]
}
```
----------------------------------
### Результат проверки квеста (/review_duel_result)
#### POST
##### REVIEW_RESULT
Клиент отправляет запрос с результатом проверки квеста false - image_1 или true - image_2
```
{
    "token": *,
    "result": bool
}
```
Сервер возвращает:
```
{
    "status": int
}
```
----------------------------------
### Начисление скилла за дуельный квест (/get_quest_duel_reward)
#### POST
##### GET_DUEL_QUEST_REWARD
```
{
    "token": *,
    "friend_id":int,
    "quest_id": int,
}
```
Сервер возвращает:
```
{
    "status": int
}
```
----------------------------------
### Получение числа всех дуельных квестов, которые not accepted и на которые юзер не залил пруф (/active_duels)
#### POST
##### COUNT_ACTIVE_DUELS
Клиент отправляет запрос на получение количества активных дуельных квестов
```
{
    "token": *
}
```
Сервер возвращает:
```
{
    "status": int,
    "active_duels": int
}
```
-----------------------------------
#### FRIENDS API
-----------------------------------
### Коды состояний друзей
```
OUTCOMING_REQUEST = 0;
INCOMING_REQUEST = 1;
CONFIRMED_FRIENDS = 2;
```
-----------------------------------
### Запрос на получение всех друзей и заявок (/friends)
#### POST
##### FRIENDS_REQUEST
Клиент отправляет запрос на получение всех друзей и заявок
```
{
    "token": *
}
```
Сервер возвращает:
```
{
    "status": int,
    "friends": [
        "friend_id": int,
        "name": string,
        "state": int,
        "is_online": bool,
        "character":{
            "name": string,
            "lvl": int,
            "class_id": int,
            "avatar_id": int
        }
    ]
}
```
-----------------------------------
### Запрос на получение информации о пользователе по юзернейму (/friend_add_request)
#### POST
##### ADD_FRIEND_REQUEST
Клиент отправляет запрос на поиск юзера з юзернеймом
```
{
    "token": *,
    "username": string
}
```
Сервер возвращает:
```
{
    "status": int
}
```
-----------------------------------
### Запрос на добавление другого юзера в друзья (/friend_send_request)
#### POST
##### FRIEND_ACTION_REQUEST
Клиент отправляет запрос на добавление в друзья
```
{
    "token": *
    "friend_id": int
}
```
Сервер возвращает:
```
{
    "status": int
}
```
-----------------------------------
### Запрос на отмену добавление другого юзера в друзья (/friend_cancel_request)
#### POST
##### FRIEND_ACTION_REQUEST
Клиент отправляет запрос отклонение friend_request, то есть юзер перехотел иметь другого юзера в друзьях и отклоняет заявку
```
{
    "token": *
    "friend_id": int
}
```
Сервер возвращает:
```
{
    "status": int
}
```
-----------------------------------
### Запрос на удаление пользователя из друзей (/friend_delete)
#### POST
##### FRIEND_ACTION_REQUEST
Клиент отправляет запрос delete
```
{
    "token": *
    "friend_id": int
}
```
Сервер возвращает:
```
{
    "status": int
}
```
-----------------------------------
### Запрос на принятие заявки в друзья (/friend_accept_request)
#### POST
##### FRIEND_ACTION_REQUEST
Клиент отправляет запрос 
```
{
    "token": *
    "friend_id": int
}
```
Сервер возвращает:
```
{
    "status": int
}
```
-----------------------------------
### Запрос на отклонение заявки в друзья (/friend_skip_request)
#### POST
##### FRIEND_ACTION_REQUEST
Клиент отправляет запрос 
```
{
    "token": *
    "friend_id": int
}
```
Сервер возвращает:
```
{
    "status": int
}
```
-----------------------------------
#### ADVENTURE BATTLE API
-----------------------------------
Запрос на инициализацию боя с монстром. Запрашивается один раз при инициализации боя.
#### POST
```
{
    "type": "ADVENTURE_BATTLE_INIT",
    "token": *,
    "stage_id": int
}
```
Ответ при инициализации боя. Отпрвляется один раз при инициализации боя, отправляется каждому игроку 
```
{
    "status": int,
    "opponent_info": {
        "name": string,
        "current_hp": int,
        "max_hp": int
    },
    "player_info":{
        "current_hp": int,
        "max_hp": int
    },
    "is_current_turn": bool,
    "time": int,
    "type": "ADVENTURE_BATTLE_INIT"
}
```
-----------------------------------
Запрос на получение battle_condition Запрашивается при задержке или если ответ от сервера(battle_condition) не прошел валидацию.
#### POST
```
{
    "type": "ADVENTURE_BATTLE_CONDITION",
    "token": *
}
```
Ответ battle_condition Отправляется после любого SKILL_ACTION
```
{
    "status": int,
    "opponent_info": {
        "current_hp": int,
        "effects":[
            {
                "effect_id": int,
                "name": string,
                "duration": int
            }
        ]
    },
    "player_info": {
        "current_hp": int,
        "effects":[
            {
                "effect_id": int,
                "name": string,
                "duration": int
            }
        ]
    },
    "skills_condition": [
        {
            "skill_id": int,
            "cooldown": int
        },  
    ],
    "skills_damage":{
            "skill_id": int,
            "damage": int
        },
    "type": "ADVENTURE_BATTLE_CONDITION",
    "is_current_turn": bool,
    "did_win":int,
    "time":int
}
```
-----------------------------------
Ответ battle_condition(при победе) Отправляется после любого SKILL_ACTION Победа высчитывается на клиенте, то есть клиент чекает хп. Добавляем "reward", который потом просто отобразим.
```
{
    "status": int,
    "opponent_info": {
        "current_hp": int,
        "effects":[
            {
                "effect_id": int,
                "name": string,
                "duration": int
            }
        ]
    },
    "skills_condition": [
        {
            "skill_id": int,
            "cooldown": int
        },  
    ],
    "reward": {
        "gold": int,
        "exp": int
    } ,
    "skills_damage":{
        "skill_id": int,
        "damage": int
    },
    "player_info": {
        "current_hp": int,
        "effects":[
            {
                "effect_id": int,
                "name": string,
                "duration": int
            }
        ]
    },
    "did_win":int,
    "is_current_turn": int,
    "type": "ADVENTURE_BATTLE_CONDITION"
}
```
-----------------------------------
Запрос на action(скилл) Запрашивается при использовании какого-то скилла.
#### POST
```
{
    "type": "SKILL_ACTION",
    "token": *,
    "skill_id": *
}
```
Ответ на action(скилл) Отвечает BATTLE_CONDITION
-----------------------------------
Запрос времени
#### POST
```
{
    "type": "TIME_REQUEST",
    "token": *
}
```
Сервер возвращает
```
{
    "status": int,
    "type": "TIME_REQUEST"
    "time": int
}
```
-----------------------------------
#### TOURNAMENT BATTLE API
-----------------------------------
Запрос на инициализацию боя в турнире
#### POST
```
{
    "type": "TOURNAMENT_BATTLE_INIT",
    "token": *
}
```
Ответ при инициализации боя. Отправляется один раз при инициализации боя, отправляется каждому игроку 
```
{
    "status": int,
    "opponent_info": {
        "name": string,
        "current_hp": int,
        "max_hp": int,
        "lvl": int
    },
    "player_info":{
        "current_hp": int,
        "max_hp": int,
        "current_win_count": int,
        "lvl": int
    },
    "is_current_turn": bool,
    "time": int,
    "type": "TOURNAMENT_BATTLE_INIT"
}
```
-----------------------------------
Запрос на получение battle_condition 
#### POST
### TOURNAMENT_BATTLE
```
{
    "type": "PVP_BATTLE_CONDITION",
    "token": *
}
```
Ответ battle_condition Отправляется после любого SKILL_ACTION
```
{
    "status": int,
    "opponent_info": {
        "username": string,
        "opponent_id": int,
        "current_hp": int,
        "effects":[
            {
                "effect_id": int,
                "name": string,
                "duration": int
            }
        ]
    },
    "skills_condition": [
        {
            "skill_id": int,
            "cooldown": int
        },  
    ],
    "skills_damage":{
            "skill_id": int,
            "damage": int
        },
    "player_info": {
        "current_hp": int,
        "effects":[
            {
                "effect_id": int,
                "name": string,
                "duration": int
            }
        ]
    },
    "type": "PVP_BATTLE_CONDITION",
    "is_current_turn": bool,
    "did_win":int,
    "time":int
}
```
-----------------------------------
Ответ battle_condition(при победе)
```
{
    "status": int,
    "opponent_info": {
        "username": string,
        "opponent_id": int,
        "current_hp": int,
        "effects":[
            {
                "effect_id": int,
                "name": string,
                "duration": int
            }
        ]
    },
    "skills_condition": [
        {
            "skill_id": int,
            "cooldown": int
        },  
    ],
    "player_info": {
        "current_hp": int,
        "effects":[
            {
                "effect_id": int,
                "name": string,
                "duration": int
            }
        ]
    },
    "reward": {
        "gold": int,
        "exp": int
    } ,
    "skills_damage":{
        "skill_id": int,
        "damage": int
    },
    "did_win":int,
    "is_current_turn": int,
    "type": "PVP_BATTLE_CONDITION"
}
```
-----------------------------------
Запрос на action(скилл) Запрашивается при использовании какого-то скилла.
#### POST
```
{
    "type": "SKILL_ACTION",
    "token": *,
    "skill_id": *
}
```
Ответ на action(скилл) Отвечает BATTLE_CONDITION
-----------------------------------
#### ARENA API
-----------------------------------
### Оплата входа на арену (/arena_pay)
#### POST
##### ARENA_PAY_REQUEST
Клиент отправляет запрос на оплату боя на арене
```
{
    "token": *
}
```
Сервер возвращает статус
```
{
    "status": int
}
```
### Все скиллы (/arena_skills)
#### POST
##### ARENA_SKILLS_REQUEST
Запрос на получение скиллов
```
{
    "token": *
}
```
Сервер возвращает скиллы доступные для боя на арене
```
{
    "status": int,
    "skills": [
        int
    ]
}
```

-----------------------------------
Запрос на инициализацию боя. Запрашивается один раз при инициализации боя.
#### POST
```
{
    "type": "ARENA_BATTLE_INIT",
    "token": *,
    "skills": [ 
        int 
    ]
}
```
Ответ при инициализации боя на арене.
```
{
    "status": int,
    "opponent_info": {
        "name": string,
        "current_hp": int,
        "max_hp": int
    },
    "player_info":{
        "current_hp": int,
        "max_hp": int
    },
    "is_current_turn": bool,
    "time": int,
    "type": "ARENA_BATTLE_INIT"
}
```
-----------------------------------
Запрос на получение arena_condition Запрашивается при задержке или если ответ от сервера(arena_condition) не прошел валидацию.
#### POST
```
{
    "type": "PVP_BATTLE_CONDITION",
    "token": *
}
```
Ответ arena_condition Отправляется после любого SKILL_ACTION
```
{
    "status": int,
    "opponent_info": {
        "username": string,
        "opponent_id": int,
        "current_hp": int,
        "effects":[
            {
                "effect_id": int,
                "name": string,
                "duration": int
            }
        ]
    },
    "skills_condition": [
        {
            "skill_id": int,
            "cooldown": int
        },  
    ],
    "skills_damage":{
            "skill_id": int,
            "damage": int
        },
    "player_info": {
        "current_hp": int,
        "effects":[
            {
                "effect_id": int,
                "name": string,
                "duration": int
            }
        ]
    },
    "type": "PVP_BATTLE_CONDITION",
    "did_win":int,
    "is_current_turn": bool,
    "time":int
}
```
-----------------------------------
Ответ arena__battle_condition(при победе) Отправляется после любого SKILL_ACTION Победа высчитывается на клиенте, то есть клиент чекает хп. Добавляем "reward", который потом просто отобразим.
```
{
    "status": int,
    "opponent_info": {
        "username": string,
        "opponent_id": int,
        "current_hp": int,
        "effects":[
            {
                "effect_id": int,
                "name": string,
                "duration": int
            }
        ]
    },
    "skills_condition": [
        {
            "skill_id": int,
            "cooldown": int
        },  
    ],
    "player_info": {
        "current_hp": int,
        "effects":[
            {
                "effect_id": int,
                "name": string,
                "duration": int
            }
        ]
    },
    "reward": {
        "gold": int,
        "crystals": int
    } ,
    "skills_damage":{
        "skill_id": int,
        "damage": int
    },
    "did_win":int,
    "type": "PVP_BATTLE_CONDITION"
}
```
-----------------------------------
Запрос на action(скилл) Запрашивается при использовании какого-то скилла.
#### POST
```
{
    "type": "SKILL_ACTION",
    "token": *,
    "skill_id": *
}
```
Ответ на action(скилл) Отвечает BATTLE_CONDITION
-----------------------------------
#### SHOP API
-----------------------------------
### Получение шоп юнитов (/shop)
#### POST
##### SHOP_REQUEST
Запрос на получение всех шоп юнитов
```
    "token": *
```
Сервер возвращает:
```
{
    "status": int,
    "shop_units": [
        {
            "unit_id": int,
            "unit_name": string,
            "unit_count": int,
            "skill_id": int,
            "unit_type": string,
            "unit_price": int,
            "price_type": string
        }
    ]
}
```
-----------------------------------
#### Покупка юнитов (/unit_buy)
#### POST
##### BUY_SHOP_REQUEST
Запрос на покупку шоп юнита
```
    "token": *,
    "unit_id": int
```
Сервер возвращает:
```
{
    "status": int
}
```
-----------------------------------
### Коды ошибок
```
STATUS_OK = 0;
WRONG_JSON = 1;
USER_DOES_NOT_EXIST = 2;
WRONG_EMAIL = 3;
WRONG_PASSWORD = 4;
WRONG_TOKEN = 5;
USERNAME_IS_ALREADY_TAKEN = 6;
EMAIL_IS_ALREADY_TAKEN = 7;
QUEST_WITH_SUCH_ID_NOT_FOUND = 8;
SKILL_WITH_SUCH_ID_NOT_FOUND = 10;
NOT_YOUR_TURN = 11;
SKILL_IS_ON_COOLDOWN_OR_DOES_NOT_EXISTS = 12;
RUN_OUT_OF_TIME = 13;
QUEST_IS_ALREADY_IN_PROGRESS = 14;
SERVER_DID_NOT_GET_FILE = 15;
USER_HAS_NO_SUCH_QUEST = 16;
NOT_A_PICTURE = 17;
TOO_BIG_PICTURE = 18;
TURN_ACTION = 19;
WRONG_CLASS = 20;
CLASS_NOT_FOUND = 21;
BATTLE_DOES_NOT_EXIST = 22;
QUEST_IS_NOT_ACCEPTED = 23;
QUEST_IS_NOT_SKIPPED = 24;
NO_QUEST_FOR_VOTING = 25;
CLASSES_NOT_FOUND = 26;
WRONG_BATTLE_TYPE = 27;
SKILLS_NOT_FOUND = 28;
EMPTY_SKILLS_TO_SELECT = 29;
PERSONAGE_HAS_NO_SUCH_SKILLS = 30;
PERSONAGE_HAS_NO_ANY_SKILLS = 31;
BATTLE_ALREADY_EXISTS = 32;
EXCEED_ACTIVE_SKILLS_BAG_SIZE = 33;
LEVEL_OF_PERSONAGE_LESS_THAN_SKILLS = 34;
NOT_ENOUGH_MONEY = 35;
WRONG_UNIT = 36;
UNIT_IS_ALREADY_BOUGHT = 37;
DIFFERENT_CLASS_OF_PERSONAGE_AND_QUEST = 39;
DIFFERENT_LVL_OF_PERSONAGE_AND_QUEST = 40;
TOO_SHORT_USERNAME = 41;
TOO_LONG_USERNAME = 42;
TOO_SHORT_PASSWORD = 43;
TOO_LONG_PASSWORD = 44;
TOO_SHORT_CHAR_NAME = 45;
TOO_LONG_CHAR_NAME = 46;
INVALID_USERNAME = 47;
INVALID_CHAR_NAME = 48;
WRONG_ARENA_SKILLS = 51;
NO_FREE_SLOT_FOR_SKILL = 52;
USER_HAD_ALREADY_GOT_REWARD = 53;
QUEST_IS_NOT_REVIEWED = 55;
WRONG_AVATAR_ID = 56;
USER_ID_CANNOT_BE_EQUAL_TO_FRIEND = 57;
FRIEND_REQUEST_IS_ALREADY_SENT = 58;
FRIEND_REQUEST_NOT_FOUND = 59;
FRIEND_NOT_FOUND = 60;
ARE_ALREADY_FRIENDS = 61;
USER_NOT_FOUND = 62;
WRONG_STAGE = 63;
STAGE_IS_NOT_ENABLED = 64;
QUEST_DUEL_REQUEST_NOT_FOUND = 65;
QUEST_DUEL_IS_ALREADY_IN_PROGRESS = 66;
QUEST_DUEL_CAN_NOT_BE_SKIPPED = 67;
QUEST_DUEL_REQUEST_IS_ALREADY_SENDED = 68;
QUEST_DUEL_IS_NOT_ACCEPTED = 69;
DUEL_REQUEST_CANNOT_BE_SENT = 70;
```