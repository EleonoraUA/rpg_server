import uuid
import tornado.websocket

from app.models.translator import Translator
from libs.storage import battlestor
from libs.storage import socketstor
from app.models.requestrouter import requestrouter

class WSMonsterHandler(tornado.websocket.WebSocketHandler):
    """This class is responsible for handling websocket's events

    Attributes:
        id (str): unique identifier of connected websocket

    """

    def open(self):
        """Handles new connection to websocket and identify it
        """
        self.id = str(uuid.uuid4())
        self.router = requestrouter.RequestRouter()
        socketstor.add_new_connection(self)
        print 'new connection: Monster'

    def on_message(self, message):
        """Handles new message from websocket

        Use RequestRouter to identify message type

        Args:
            message (str): string that user sends to HTTP server

        """
        if message is not None:
            response_message = self.router.route_monster_battle_request_for_socket(message, self.id)
            if response_message is not None:
                self.write_message(response_message)

        print 'sending back message: %s' % response_message

    def on_close(self):
        """Handles closing of websocket connection and delete it from memory
        """
        token_to_close = socketstor.get_token_by_socket(self.id)
        translator = Translator()
        battlestor.delete_battle_by_token(token_to_close)
        socketstor.delete_connection(self)
        translator.send_request_battle_end(token_to_close)
        print 'connection closed'

    def check_origin(self, origin):
        return True