import json
import uuid
import tornado.websocket

from app.models.translator import Translator
from libs.storage import battlestor
from libs.storage import socketstor
from app.models.clientconnection import connection_manager
from app.models.requestrouter import requestrouter

PLAYER_OPPONENT_STRING = 'opponent'
BATTLE_CONDITION_STRING = 'battle_condition'

class WSArenaHandler(tornado.websocket.WebSocketHandler):
    """This class is responsible for handling websocket's events

    Attributes:
        id (str): unique identifier of connected websocket
    """

    def open(self):
        """Handles new connection to websocket for Arena and identify it
        """
        self.id = str(uuid.uuid4())
        self.router = requestrouter.RequestRouter()
        socketstor.add_new_connection(self)
        print 'new connection: Arena'

    def on_message(self, message):
        """Handles new message from websocket in Arena

        Use RequestRouter to identify message type

        Args:
            message (str): string that user sends to HTTP server
        """
        if message is not None:
            response_message = self.router.route_arena_battle_request_for_socket(message, self.id)
            if response_message is not None:
                self.write_message(response_message)
        print 'sending back message:'

    def on_close(self):
        """Handles closing of websocket connection and delete Arena battle from memory
        """
        token_to_close = socketstor.get_token_by_socket(self.id)
        translator = Translator()
        if battlestor.is_arena_player_in_battle(token_to_close):
            battlestor.delete_arena_by_token(token_to_close)
            response = translator.send_request_unexpected_close(token_to_close)
            response = json.loads(response)
            connection_manager.send_response_to_client(response[PLAYER_OPPONENT_STRING], response[BATTLE_CONDITION_STRING])
            battlestor.delete_arena_by_token(response[PLAYER_OPPONENT_STRING])
        battlestor.delete_battle_by_token(token_to_close)
        socketstor.delete_connection(self)
        translator.send_request_battle_end(token_to_close)
        print 'connection closed'

    def check_origin(self, origin):
        return True