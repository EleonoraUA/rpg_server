import json

from app.models.monsterbattle import monster_battle_manager
from app.models.pvpbattle.arenabattle import arena_battle_manager
from app.models.pvpbattle.tournamentbattle import tournament_battle_manager
from app.models.translator import Translator
from app.models.validator import Validator
from libs.storage import socketstor
from libs.storage import redisstor

BATTLE_INIT_REQUEST = 'ADVENTURE_BATTLE_INIT'
ARENA_INIT_REQUEST = "ARENA_BATTLE_INIT"
SKILL_ACTION_REQUEST = 'SKILL_ACTION'
BATTLE_CONDITION_REQUEST = 'ADVENTURE_BATTLE_CONDITION'
TOURNAMENT_BATTLE_INIT = 'TOURNAMENT_BATTLE_INIT'
TOURNAMENT_BATTLE_CONDITION = 'TOURNAMENT_BATTLE_CONDITION'

# Request/response array elements
RESPONSE_STATUS_STRING = "status"
USER_TOKEN_STRING = "token"
JSON_TYPE_STRING = "type"
ARENA_SKILLS_STRING = 'skills'

# Status code
RESPONSE_STATUS_OK = 0
WRONG_JSON_ERROR = 1


class RequestRouter(object):
    """Route actions on monster or Arena battle

    Use Validator to validate incoming request and Translator to send request to HTTP server

    Attributes:
        validator (Validator): use to validate incoming request
        translator (Translator): use to send request to HHTP server
    """

    def __init__(self):
        self.validator = Validator()
        self.translator = Translator()

    def route_arena_battle_request_for_socket(self, request, socket_id):
        """Route Arena battle actions

        Identify user action in the Arena battle and contains business Arena logic

        Args:
            request_json (json): incoming message from websocket connection
            socket_id (str): websocket identifier
        """
        if self.validator.is_json_valid(request):
            request_json = json.loads(request)
            request_type = request_json[JSON_TYPE_STRING]
            redisstor.set_user_is_online(request_json[USER_TOKEN_STRING])
            if (request_type == ARENA_INIT_REQUEST) & self.validator.is_skill_array_valid(request_json):
                if arena_battle_manager.is_arena_player_valid(request):
                    arena_battle_manager.set_new_arena_player(request_json[USER_TOKEN_STRING], request_json[ARENA_SKILLS_STRING], socket_id)
                return None
            elif (request_type == SKILL_ACTION_REQUEST) & self.validator.is_skill_valid(request_json):
                skill_action_server_response = self.translator.send_request_skill_action(request)
                skill_action_server_response = json.loads(skill_action_server_response)
                if skill_action_server_response[RESPONSE_STATUS_STRING] == RESPONSE_STATUS_OK:
                    arena_battle_manager.player_did_use_skill(request_json[USER_TOKEN_STRING])
                    condition = self.translator.send_request_pvp_condition_for_token(request_json[USER_TOKEN_STRING])
                    condition_json = json.loads(condition)
                    if condition_json[RESPONSE_STATUS_STRING] == RESPONSE_STATUS_OK:
                        del condition_json[RESPONSE_STATUS_STRING]
                        arena_battle_manager.init_turn_and_send_arena_condition_to_players(condition_json)
                    return None


    def route_monster_battle_request_for_socket(self, request_json, socket_id):
        """Route monster battle actions

        Identify user action in the monster battle and contains business battle logic

        Args:
            request_json (dict): incoming message from websocket connection
            socket_id (str): websocket identifier
        """
        if self.validator.is_json_valid(request_json):
            request = json.loads(request_json)
            request_type = request[JSON_TYPE_STRING]
            redisstor.set_user_is_online(request[USER_TOKEN_STRING])
            if request_type == BATTLE_INIT_REQUEST:
                socketstor.set_token_for_socket(request[USER_TOKEN_STRING], socket_id)
                battle_init_response = self.translator.send_request_battle_init(request_json)
                battle_init_response_json = json.loads(battle_init_response)
                if battle_init_response_json[RESPONSE_STATUS_STRING] == RESPONSE_STATUS_OK:
                    monster_battle_manager.init_battle_for_user(request[USER_TOKEN_STRING])
                return battle_init_response
            elif (request_type == SKILL_ACTION_REQUEST) & self.validator.is_skill_valid(request):
                skill_action_server_response = self.translator.send_request_skill_action(request_json)
                skill_action_server_response = json.loads(skill_action_server_response)
                if skill_action_server_response[RESPONSE_STATUS_STRING] == RESPONSE_STATUS_OK:
                    monster_battle_manager.player_did_use_skill(request[USER_TOKEN_STRING])
                    response = self.translator.send_request_battle_condition(request_json)
                    response_json = json.loads(response)
                    monster_battle_manager.check_player_did_win(response_json, request[USER_TOKEN_STRING])
                    return response
                else:
                    return skill_action_server_response
            elif request_type == BATTLE_CONDITION_REQUEST:
                return self.translator.send_request_battle_condition(request_json)
        return json.dumps({RESPONSE_STATUS_STRING : WRONG_JSON_ERROR})


    def route_tournament_battle_request_for_socket(self, request, socket_id):
        if self.validator.is_json_valid(request):
            request_json = json.loads(request)
            request_type = request_json[JSON_TYPE_STRING]
            redisstor.set_user_is_online(request_json[USER_TOKEN_STRING])
            if request_type == TOURNAMENT_BATTLE_INIT:
                tournament_battle_manager.set_new_tournament_player(request_json[USER_TOKEN_STRING], socket_id)
                return None
            elif request_type == SKILL_ACTION_REQUEST:
                skill_action_server_response = self.translator.send_request_skill_action(request)
                skill_action_server_response = json.loads(skill_action_server_response)
                if skill_action_server_response[RESPONSE_STATUS_STRING] == RESPONSE_STATUS_OK:
                    tournament_battle_manager.player_did_use_skill(request_json[USER_TOKEN_STRING])
                    condition = self.translator.send_request_pvp_condition_for_token(request_json[USER_TOKEN_STRING])
                    condition_json = json.loads(condition)
                    if condition_json[RESPONSE_STATUS_STRING] == RESPONSE_STATUS_OK:
                        del condition_json[RESPONSE_STATUS_STRING]
                        tournament_battle_manager.init_turn_and_send_condition_to_players(condition_json)
                    return None
                return None