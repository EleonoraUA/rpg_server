from libs.storage import battlestor
from libs.storage import socketstor
from app.models.clientconnection import connection_manager

RESPONSE_REWARD_STRING = "reward"
RESPONSE_STATUS_STRING = "status"
CURRENT_TURN_PLAYER_STRING = 'is_current_turn'
RESPONSE_STATUS_OK = 0

def set_new_tournament_player(token, socket_id):
    """ Add new tournament player for waiting an opponent

    Args:
        token (str): user identifier
        skills (array): set of skills that user want to use in Arena
    """
    if not battlestor.is_tournament_player_in_battle(token):
        socketstor.set_token_for_socket(token, socket_id)
        battlestor.add_new_tournament_player(token)

def player_did_use_skill(token):
    """Set that player did use a skill

    Args:
        token (str): user identifier
    """
    battlestor.set_tournament_skill_action(token)

def init_turn_and_send_condition_to_players(condition):
    """Init turn for player with current turn and sends battle condition to both players

    Args:
        condition (dict): battle condition to send
    """
    current_turn_player = condition[CURRENT_TURN_PLAYER_STRING]
    del condition[CURRENT_TURN_PLAYER_STRING]
    if RESPONSE_REWARD_STRING not in condition[current_turn_player]:
        battlestor.tournament_player_turn_init(current_turn_player)
    else:
        battlestor.delete_tournament_battle_for_players(condition.keys()[0], condition.keys()[1])
    connection_manager.send_response_to_client(condition.keys()[0], condition[condition.keys()[0]])
    connection_manager.send_response_to_client(condition.keys()[1], condition[condition.keys()[1]])
