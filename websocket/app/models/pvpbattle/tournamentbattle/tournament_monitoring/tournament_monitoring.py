#
# This module monitor new Tournament players and players timeout
#
import datetime
import json
import threading
import time

from app.models.pvpbattle.tournamentbattle import tournament_battle_manager
from app.models.translator import Translator
from libs.storage import battlestor

RESPONSE_STATUS_STRING = 'status'
RESPONSE_STATUS_OK = 0
MIN_NUMBER_PVP_PLAYERS = 1

def start_tournament_monitoring():
    """Start new thread to monitor battle
    """
    tournament_thread = threading.Thread(target=new_tournament_players_monitoring)
    tournament_thread.daemon = True
    tournament_thread.start()

def new_tournament_players_monitoring():
    """Monitoring user's Tournament battle

    Once a second it gets all user tokens that didn't use a skill, so they missed the turn and all
    user tokens who have joined Tournament.
    """
    translator = Translator()
    while True:
        time_now = datetime.datetime.now().strftime('%H%M%S')
        timeout_tokens = battlestor.get_tournament_players_with_timeout(time_now)
        tournament_tokens = battlestor.get_all_waiting_tournament_players()
        if timeout_tokens:
            for token in timeout_tokens:
                translator.send_request_turn_end(token)
                condition = translator.send_request_pvp_condition_for_token(token)
                condition = json.loads(condition)
                if condition[RESPONSE_STATUS_STRING] == RESPONSE_STATUS_OK:
                    del condition[RESPONSE_STATUS_STRING]
                    tournament_battle_manager.init_turn_and_send_condition_to_players(condition)

        if len(tournament_tokens) > MIN_NUMBER_PVP_PLAYERS:
            while len(tournament_tokens) > MIN_NUMBER_PVP_PLAYERS:
                player_1_token = tournament_tokens[0]
                player_2_token = tournament_tokens[1]
                print tournament_tokens
                condition = translator.send_request_tournament_init_for_players(player_1_token, player_2_token)
                condition = json.loads(condition)
                if condition[RESPONSE_STATUS_STRING] == RESPONSE_STATUS_OK:
                    tournament_tokens.remove(player_1_token)
                    tournament_tokens.remove(player_2_token)
                    del condition[RESPONSE_STATUS_STRING]
                    battlestor.set_tournament_player_is_in_battle(player_1_token)
                    battlestor.set_tournament_player_is_in_battle(player_2_token)
                    tournament_battle_manager.init_turn_and_send_condition_to_players(condition)
                else:
                    print condition
                    tournament_tokens.remove(player_1_token)
                    tournament_tokens.remove(player_2_token)
        time.sleep(1)
