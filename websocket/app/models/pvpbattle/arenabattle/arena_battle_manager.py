import json
from libs.storage import battlestor
from libs.storage import socketstor
from app.models.translator import Translator
from app.models.clientconnection import connection_manager

RESPONSE_REWARD_STRING = "reward"
RESPONSE_STATUS_STRING = "status"
CURRENT_TURN_PLAYER_STRING = 'is_current_turn'
RESPONSE_STATUS_OK = 0

def set_new_arena_player(token, skills, socket_id):
    """ Add new Arena player for waiting an opponent

    Args:
        token (str): user identifier
        skills (array): set of skills that user want to use in Arena
    """
    if not battlestor.is_arena_player_in_battle(token):
        socketstor.set_token_for_socket(token, socket_id)
        battlestor.add_new_arena_player(token)
        battlestor.set_arena_player_skills(token, skills)

def player_did_use_skill(token):
    """Set that player did use a skill

    Args:
        token (str): user identifier
    """
    battlestor.set_arena_skill_action(token)

def init_turn_and_send_arena_condition_to_players(condition):
    """Init turn for player with current turn and sends battle condition to both players

    Args:
        condition (dict): battle condition to send
    """
    current_turn_player = condition[CURRENT_TURN_PLAYER_STRING]
    del condition[CURRENT_TURN_PLAYER_STRING]
    if RESPONSE_REWARD_STRING not in condition[current_turn_player]:
        battlestor.arena_player_turn_init(current_turn_player)
    else:
        battlestor.delete_arena_battle_for_players(condition.keys()[0], condition.keys()[1])
    connection_manager.send_response_to_client(condition.keys()[0], condition[condition.keys()[0]])
    connection_manager.send_response_to_client(condition.keys()[1], condition[condition.keys()[1]])

def is_arena_player_valid(request):
    translator = Translator()
    player_is_valid_response = translator.send_request_is_arena_player_valid(request)
    player_is_valid_response = json.loads(player_is_valid_response)
    if player_is_valid_response[RESPONSE_STATUS_STRING] == RESPONSE_STATUS_OK:
        return True
    return False
