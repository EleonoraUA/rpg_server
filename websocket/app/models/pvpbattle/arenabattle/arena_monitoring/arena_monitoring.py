#
# This module monitor new Arena players and players timeout
#
import datetime
import json
import threading
import time

from app.models.pvpbattle.arenabattle import arena_battle_manager
from app.models.translator import Translator
from libs.storage import battlestor

RESPONSE_STATUS_STRING = 'status'
RESPONSE_STATUS_OK = 0
MIN_NUMBER_PVP_PLAYERS = 1

def start_arena_monitoring():
    """Start new thread to monitor battle
    """
    arena_thread = threading.Thread(target=new_arena_players_monitoring)
    arena_thread.daemon = True
    arena_thread.start()

def new_arena_players_monitoring():
    """Monitoring user's Arena battle

    Once a second it gets all user tokens that didn't use a skill, so they missed the turn and all
    user tokens who have joined Arena.
    """
    translator = Translator()
    while True:
        time_now = datetime.datetime.now().strftime('%H%M%S')
        timeout_tokens = battlestor.get_arena_players_with_timeout(time_now)
        arena_tokens = battlestor.get_all_waiting_arena_players()
        if timeout_tokens:
            for token in timeout_tokens:
                translator.send_request_turn_end(token)
                condition = translator.send_request_pvp_condition_for_token(token)
                condition = json.loads(condition)
                if condition[RESPONSE_STATUS_STRING] == RESPONSE_STATUS_OK:
                    del condition[RESPONSE_STATUS_STRING]
                    arena_battle_manager.init_turn_and_send_arena_condition_to_players(condition)

        if len(arena_tokens) > MIN_NUMBER_PVP_PLAYERS:
            while len(arena_tokens) > MIN_NUMBER_PVP_PLAYERS:
                player_1_token = arena_tokens[0]
                player_2_token = arena_tokens[1]
                condition = translator.send_request_arena_init_for_players(player_1_token, player_2_token)
                condition = json.loads(condition)
                if condition[RESPONSE_STATUS_STRING] == RESPONSE_STATUS_OK:
                    arena_tokens.remove(player_1_token)
                    arena_tokens.remove(player_2_token)
                    del condition[RESPONSE_STATUS_STRING]
                    battlestor.set_arena_player_is_in_battle(player_1_token)
                    battlestor.set_arena_player_is_in_battle(player_2_token)
                    arena_battle_manager.init_turn_and_send_arena_condition_to_players(condition)
        time.sleep(1)
