#
# This module contains part of monster battle business logic
#
from libs.storage import battlestor

RESPONSE_REWARD_STRING = "reward"

def init_battle_for_user(token):
    """Init new monster battle

    Args:
        token (str): user identifier
    """
    battlestor.delete_battle_by_token(token)
    battlestor.set_turn_init(token)

def player_did_use_skill(token):
    """Write player did use skill in the battle

    Args:
        token (str): user identifier
    """
    battlestor.set_skill_action(token)

def check_player_did_win(condition, token):
    """Check if player did win the battle and set monster turn

    Args:
        condition (dict): current battle condition
        token (str): user identifier
    """
    if RESPONSE_REWARD_STRING not in condition:
        battlestor.set_monster_turn(token)
        battlestor.set_skill_action(token)

def check_monster_did_win(condition, token):
    """Check if monster did win the battle

    Args:
        condition (dict): current battle condition
        token (str): user identifier
    """
    if RESPONSE_REWARD_STRING not in condition:
        battlestor.set_turn_init(token)

def init_monster_turn(token):
    """Init monster turn

    Args:
        token (str): user identifier
    """
    battlestor.set_skill_action(token)
    battlestor.set_monster_turn(token)
