#
# This module monitor monster battle players timeout
#
import datetime
import json
import threading
import time
import sys

from app.models.translator import Translator
from libs.storage import battlestor
from libs.storage import socketstor
from app.models.clientconnection import connection_manager
from app.models.monsterbattle import monster_battle_manager

def start_player_timeout_monitoring():
    """Start new thread to monitor battle
    """
    monitoring_thread = threading.Thread(target=player_timeout_monitoring)
    monitoring_thread.daemon = True
    monitoring_thread.start()

def player_timeout_monitoring():
    """Monitoring user's battle condition

    Once a second it gets all user tokens that didn't use a skill, so they missed the turn.
    It sends a message to a HTTP server to end user's turn and inits a monster
    """
    translator = Translator()
    while True:
        time_now = datetime.datetime.now().strftime('%H%M%S')
        tokens = battlestor.get_tokens_by_client_end_time(time_now)
        if tokens:
            for token in tokens:
                socket = socketstor.get_socket_by_token(token)
                if socket is not None:
                    condition = translator.send_request_turn_end(token)
                    condition = json.loads(condition)
                    connection_manager.send_response_to_client(token, condition)
                    monster_battle_manager.init_monster_turn(token)
                else:
                    battlestor.delete_battle_by_token(token)

        tokens = battlestor.get_tokens_by_monster_turn_time(time_now)
        if tokens:
            for token in tokens:
                socket = socketstor.get_socket_by_token(token)
                if socket is not None:
                    condition = translator.send_request_monster_turn(token)
                    print condition
                    try:
                        connection_manager.send_response_to_client(token, condition)
                    except Exception:
                        print sys.exc_info()[0]
                    monster_battle_manager.check_monster_did_win(condition, token)
                    battlestor.delete_monster_turn_time(time_now, token)
        time.sleep(1)