#
# This module is used to send a response to websocket client
#

from libs.storage import socketstor

def send_response_to_client(token, response):
    """Send response to websocket client

    Args:
        token (str): user identifier
        response (dict): response to send
    """
    client_socket = socketstor.get_socket_by_token(token)
    if client_socket:
        client_socket.write_message(response)
