import json

MAX_ARENA_SKILLS_COUNT = 5
USER_TOKEN_STRING_LENGTH = 43
USER_TOKEN_STRING_IDENTIFIER = 'token'
JSON_TYPE_STRING = 'type'
ARENA_SKILLS_STRING = 'skills'
SKILL_ID_STRING = 'skill_id'

class Validator(object):
    """Validate incoming messages from websocket
    """

    def is_json_valid(self, request_json):
        """Validate json from request

        Args:
            request_json (json): incoming message from websocket connection
        """
        try:
            json_object = json.loads(request_json)
            if (JSON_TYPE_STRING not in json_object) or not (self.is_token_valid(json_object)):
                raise ValueError
        except ValueError, e:
            return False
        return True

    def is_token_valid(self, request):
        """Validate token from request

        Args:
            request (dict): request loaded from json
        """
        if (USER_TOKEN_STRING_IDENTIFIER in request):
            if isinstance(request[USER_TOKEN_STRING_IDENTIFIER], basestring) and (len(request[USER_TOKEN_STRING_IDENTIFIER]) == USER_TOKEN_STRING_LENGTH):
                return True
            return False
        else:
            return False

    def is_skill_array_valid(self, request):
        """Validate arena skills set

        Args:
            request (dict): request loaded from json
        """
        if ARENA_SKILLS_STRING in request:
            if isinstance(request[ARENA_SKILLS_STRING], list) and len(request[ARENA_SKILLS_STRING]) <= MAX_ARENA_SKILLS_COUNT:
                for skill in request[ARENA_SKILLS_STRING]:
                    if (not isinstance(skill, int)):
                        return False
                return True
        return False

    def is_skill_valid(self, request):
        """Validate one skill for skill action

        Args:
            request (dict): request loaded from json
        """
        if (SKILL_ID_STRING in request):
            if isinstance(request[SKILL_ID_STRING], int):
                return True
        return False