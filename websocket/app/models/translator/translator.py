import httplib
import json

from libs.storage import battlestor
# Request types
BATTLE_INIT_REQUEST = 'ADVENTURE_BATTLE_INIT'
ARENA_INIT_REQUEST = "ARENA_BATTLE_INIT"
TIME_REQUEST = 'TIME_REQUEST'
SKILL_ACTION_REQUEST = 'SKILL_ACTION'
BATTLE_CONDITION_REQUEST = 'ADVENTURE_BATTLE_CONDITION'
TOURNAMENT_BATTLE_INIT_REQUEST = 'TOURNAMENT_BATTLE_INIT'
PVP_CONDITION_REQUEST = 'PVP_BATTLE_CONDITION'

# URLs
BATTLE_INIT_URL = '/battle_init'
ARENA_INIT_URL = '/arena_init'
BATTLE_END_URL = '/end_battle'
END_TURN_URL = '/end_turn'
MONSTER_TURN_URL = '/monster_turn'
SKILL_ACTION_URL = '/skill_action'
BATTLE_CONDITION_URL = '/battle_condition'
PVP_CONDITION_URL = '/pvp_battle_condition'
TOKEN_EXISTS_URL = '/token_exists'
UNEXPECTED_CLOSE_URL = '/unexpected_close'
ARENA_PLAYER_VALIDATION_URL = '/arena_player_validation'
TOURNAMENT_BATTLE_INIT_URL = '/tournament_init'

# Request/response array elements
RESPONSE_STATUS = "status"
USER_TOKEN_STRING = "token"
JSON_TYPE_STRING = "type"
PLAYER_1_STRING = "player_1"
PLAYER_2_STRING = "player_2"
PLAYER_1_SKILLS = 'player_1_skills'
PLAYER_2_SKILLS = 'player_2_skills'


# Status code
RESPONSE_STATUS_OK = 0
WRONG_JSON_ERROR = 1

# Host
HTTP_HOST = "0.0.0.0"
HTPP_PORT = 8000

class Translator(object):
    """Send request from websocket to HTTP server and return server response
    """

    def send_request_turn_end(self, token):
        """Send request to HTTP server to end user's turn

        Args:
            token (str): user identifier
        """
        return self.send_http_request_by_url(END_TURN_URL,
                                             json.dumps({USER_TOKEN_STRING : token}))

    def send_request_battle_end(self, token):
        """Send request to HTTP server to end user's battle

        Args:
            token (str): user identifier
        """
        return self.send_http_request_by_url(BATTLE_END_URL,
                                             json.dumps({USER_TOKEN_STRING: token}))

    def send_request_monster_turn(self, token):
        """Send request to HTTP server to start monster turn

        Args:
            token (str): user identifier
        """
        response = self.send_http_request_by_url(MONSTER_TURN_URL,
                                                 json.dumps({USER_TOKEN_STRING: token}))
        response = json.loads(response)
        if (response[RESPONSE_STATUS] == RESPONSE_STATUS_OK):
            return self.send_http_request_by_url(BATTLE_CONDITION_URL,
                                                 json.dumps({USER_TOKEN_STRING : token}))
        else:
            return response

    def send_http_request_by_url(self, request_url, request_json):
        """Private method to send request to HTTP server

        Args:
            request_url (str): URL to send request
            request_json (json): json to send
        """
        httpServ = httplib.HTTPConnection(HTTP_HOST, HTPP_PORT)
        httpServ.connect()

        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

        httpServ.request('POST', request_url, request_json, headers)

        response = httpServ.getresponse()
        httpServ.close()

        return response.read()

    def send_request_arena_init_for_players(self, player_1_token, player_2_token):
        """Send request to HTTP server

        Args:
            request_url (str): URL to send request
            request_json (json): json to send
        """
        request_json = json.dumps({
            JSON_TYPE_STRING: ARENA_INIT_REQUEST,
            PLAYER_1_STRING: player_1_token,
            PLAYER_2_STRING: player_2_token,
            PLAYER_1_SKILLS: battlestor.get_arena_player_skills(player_1_token),
            PLAYER_2_SKILLS: battlestor.get_arena_player_skills(player_2_token)
        })
        print request_json
        return self.send_http_request_by_url(ARENA_INIT_URL, request_json)

    def send_request_tournament_init_for_players(self, player_1_token, player_2_token):
        """Send request to HTTP server

        Args:
            request_url (str): URL to send request
            request_json (json): json to send
        """
        request_json = json.dumps({
            JSON_TYPE_STRING: TOURNAMENT_BATTLE_INIT_REQUEST,
            PLAYER_1_STRING: player_1_token,
            PLAYER_2_STRING: player_2_token
        })
        return self.send_http_request_by_url(TOURNAMENT_BATTLE_INIT_URL, request_json)

    def send_request_unexpected_close(self, token):
        """Send request with unexpected websocket close

        Args:
            token (str): user identifier
        """
        request_json = json.dumps({USER_TOKEN_STRING : token})
        return self.send_http_request_by_url(UNEXPECTED_CLOSE_URL, request_json)

    def send_request_pvp_condition_for_token(self, token):
        """Send request for arena condition for player

        Args:
            token (str): user identifier
        """
        request_json = json.dumps({JSON_TYPE_STRING: PVP_CONDITION_REQUEST,
                                   USER_TOKEN_STRING: token})
        return self.send_http_request_by_url(PVP_CONDITION_URL, request_json)

    def send_request_battle_condition(self, request_json):
        """Send request for battle condition for player

        Args:
            request_json (dict): incoming request
        """
        return self.send_http_request_by_url(BATTLE_CONDITION_URL, request_json)

    def send_request_battle_init(self, request_json):
        """Send request to init monster battle

        Args:
            request_json (dict): incoming request
        """
        return self.send_http_request_by_url(BATTLE_INIT_URL, request_json)

    def send_request_skill_action(self, request_json):
        """Send notify player did use skill

        Args:
            request_json (dict): incoming request
        """
        return self.send_http_request_by_url(SKILL_ACTION_URL, request_json)

    def send_request_is_token_exists(self, token):
        """Send request to find out if such token exists

        Args:
            token (str): user identifier
        """
        request_json = json.dumps({USER_TOKEN_STRING: token})
        return self.send_http_request_by_url(TOKEN_EXISTS_URL, request_json)

    def send_request_is_arena_player_valid(self, request):
        """Send request to find out if arena player can play

        Args:
            request (dict): incoming request
        """
        return self.send_http_request_by_url(ARENA_PLAYER_VALIDATION_URL, request)
