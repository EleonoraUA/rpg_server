#
# This module stores active sockets
#

websockets = dict()
tokens = dict()
socket_ids = dict()
arena_players = dict()

def add_new_connection(socket):
    """Add new websocket connection

    Args:
        socket (WSHandler): websocket to add
    """
    global websockets
    websockets[socket.id] = socket

def add_new_arena_player_socket(token, socket_id):
    """Add new arena player socket

    Args:
        token (str): user identifier
        socket_id (int): websocket identifier
    """
    arena_players[token] = socket_id

def delete_connection(socket):
    """Delete websocket connection when it closes

    Args:
        socket (WSHandler): websocket to delete
    """
    global websockets
    global tokens
    global socket_ids
    if socket.id in tokens:
        token = tokens[socket.id]
        tokens.pop(socket.id, None)
        socket_ids.pop(token, None)
    websockets.pop(socket.id, None)

def get_socket_by_token(token):
    """Find socket id by user's token

    Args:
        token (str): user identifier
    """
    global websockets
    global socket_ids
    if token in socket_ids:
        socket_id = socket_ids[token]
        if socket_id in websockets:
            return websockets[socket_id]

def set_token_for_socket(token, socket_id):
    """Set socket id by user's token

    Args:
        token (str): user identifier
        socket_id (str): websocket identifier
    """
    global socket_ids
    global tokens
    socket_ids[token] = socket_id
    tokens[socket_id] = token

def get_token_by_socket(socket_id):
    """Find token by socket id

    Args:
        socket_id (str): websocket identifier
    """
    global tokens
    if socket_id in tokens:
        return tokens[socket_id]