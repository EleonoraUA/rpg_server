#
# This module stores and manage battle condition
#
import datetime

monster_turn_end_time = dict()
monster_turn_end_token = dict()

client_turn_end_time = dict()
client_turn_end_token = dict()

arena_player_token = dict()
arena_player_timeout = dict()
arena_waiting_players = []
arena_active_players = []
arena_player_skills = dict()

tournament_player_token = dict()
tournament_player_timeout = dict()
tournament_waiting_players = []
tournament_active_players = []

MONSTER_TURN_DELAY_TIME = 2
CLIENT_TURN_TIME = 30
ARENA_OPPONENT_WAITING_TIMEOUT = 30
TOURNAMENT_OPPONENT_WAITING_TIMEOUT = 30


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                              Monster battle API                                   #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def set_monster_turn(token):
    """Set opponent/monster turn after delay

    Args:
        token (str): user identifier
    """
    global monster_turn_end_time
    global monster_turn_end_token
    turn_time = (datetime.datetime.now() + datetime.timedelta(seconds=MONSTER_TURN_DELAY_TIME)).strftime('%H%M%S')
    if turn_time not in monster_turn_end_token:
        monster_turn_end_token[turn_time] = []
    monster_turn_end_time[token] = turn_time
    monster_turn_end_token[turn_time].append(token)

def delete_monster_turn_time(time, token):
    """Unset opponent/monster turn after client's

    This function is used to free memory after closing socket connection or if monster turn has end

    Args:
        time (str): time of planned monster turn
        token (str): user identifier
    """
    global monster_turn_end_time
    global monster_turn_end_token
    monster_turn_end_time.pop(token, None)
    monster_turn_end_token.pop(time, None)

def set_turn_init(token):
    """Initialize user's turn for specified time

    Args:
        token (str): user identifier
    """
    global client_turn_end_time
    global client_turn_end_token
    end_time = (datetime.datetime.now() + datetime.timedelta(seconds=CLIENT_TURN_TIME)).strftime('%H%M%S')
    if end_time not in client_turn_end_token:
        client_turn_end_token[end_time] = []
    client_turn_end_token[end_time].append(token)
    client_turn_end_time[token] = end_time

def set_skill_action(token):
    """Set skill action of user

    This function is used to free memory if user has used skill or missed his turn

    Args:
        token (str): user identifier
    """
    global client_turn_end_time
    global client_turn_end_token
    if token in client_turn_end_time:
        end_time = client_turn_end_time[token]
        client_turn_end_time.pop(token, None)
        client_turn_end_token.pop(end_time, None)

def get_tokens_by_client_end_time(time):
    """Get users by specified end turn time

    This function is used to get all users that didn't use skill and missed their turns

    Args:
        time (str): users turn end time
    """
    global client_turn_end_token
    if time in client_turn_end_token:
        return client_turn_end_token[time]
    else:
        return None

def get_tokens_by_monster_turn_time(time):
    """Get users that are waiting for monster turn

    Args:
        time (str): monster turn planned time
    """
    global monster_turn_end_token
    if time in monster_turn_end_token:
        return monster_turn_end_token[time]
    else:
        return None

def delete_battle_by_token(token):
    """Delete user's battle

    This function is used to free memory if connection was closed or battle has ended

    Args:
        token (str): user identifier
    """
    global client_turn_end_time
    global client_turn_end_token
    global monster_turn_end_time
    global arena_waiting_players
    if token in client_turn_end_time:
        end_client_turn_time = client_turn_end_time[token]
        client_turn_end_time.pop(token, None)
        client_turn_end_token.pop(end_client_turn_time, None)
    if token in monster_turn_end_time:
        monster_turn_time = monster_turn_end_time[token]
        delete_monster_turn_time(monster_turn_time, token)
    if token in arena_waiting_players:
        arena_waiting_players.remove(token)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                   Arena API                                       #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def add_new_arena_player(token):
    """Add new player to wait for an opponent

    Args:
        token (str): user identifier
    """
    global arena_waiting_players
    arena_waiting_players.append(token)

def get_all_waiting_arena_players():
    """Get all Arena players that are waiting for an opponent
    """
    global arena_waiting_players
    return arena_waiting_players

def set_arena_player_is_in_battle(token):
    """Set Arena player started battle

    Args:
        token (str): user identifier
    """
    global arena_waiting_players
    global arena_active_players
    arena_active_players.append(token)
    if (token in arena_waiting_players) or (token in arena_waiting_players):
        arena_waiting_players.remove(token)

def set_arena_skill_action(token):
    """Set Arena player did use skill

    Args:
        token (str): user identifier
    """
    global arena_player_token
    global arena_player_timeout
    if token in arena_player_timeout:
        timeout = arena_player_timeout[token]
        arena_player_timeout.pop(token, None)
        arena_player_token.pop(timeout, None)

def get_arena_players_with_timeout(timeout):
    """Get all players that missed their turn

    Args:
        timeout (str): time now
    """
    global arena_player_token
    if timeout in arena_player_token:
        return arena_player_token[timeout]

def arena_player_turn_init(token):
    """Init Arena player turn

    Args:
        token (str): user identifier
    """
    global arena_player_token
    global arena_player_timeout
    opponent_waiting_timeout = (datetime.datetime.now() + datetime.timedelta(seconds=ARENA_OPPONENT_WAITING_TIMEOUT)).strftime('%H%M%S')
    if opponent_waiting_timeout not in arena_player_token:
        arena_player_token[opponent_waiting_timeout] = []
    arena_player_token[opponent_waiting_timeout].append(token)
    arena_player_timeout[token] = opponent_waiting_timeout

def is_arena_player_in_battle(token):
    """Get if Arena player is in battle or wait for an opponent

    Args:
        token (str): user identifier
    """
    global arena_active_players
    global arena_waiting_players
    if token in arena_active_players:
        return True
    else:
        return False

def delete_arena_battle_for_players(player_1_token, player_2_token):
    """Delete battle for two users

    Args:
        player_1_token (str): user identifier
        player_2_token (str): user identifier
    """
    print 'close'
    print player_1_token + player_2_token
    delete_arena_by_token(player_1_token)
    delete_arena_by_token(player_2_token)

def delete_arena_by_token(token):
    """Delete all Arena info for user

    Args:
        token (str): user identifier
    """
    global arena_player_token
    global arena_player_timeout
    global arena_waiting_players
    global arena_active_players
    if token in arena_player_timeout:
        timeout = arena_player_timeout[token]
        arena_player_timeout.pop(token, None)
        arena_player_token.pop(timeout, None)
    if token in arena_active_players:
        arena_active_players.remove(token)
    if token in arena_waiting_players:
        arena_waiting_players.remove(token)

def set_arena_player_skills(token, skills):
    """Set skills set that user want to use in Arena

    Args:
        token (str): user identifier
    """
    global arena_player_skills
    arena_player_skills[token] = skills

def get_arena_player_skills(token):
    """Get skills set that user want to use in Arena

    Args:
        token (str): user identifier
    """
    global arena_player_skills
    if token in arena_player_skills:
        return arena_player_skills[token]


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                 Tournament API                                    #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def add_new_tournament_player(token):
    global tournament_waiting_players
    tournament_waiting_players.append(token)

def get_all_waiting_tournament_players():
    global tournament_waiting_players
    return tournament_waiting_players

def set_tournament_player_is_in_battle(token):
    global tournament_waiting_players
    global tournament_active_players
    tournament_active_players.append(token)
    print 'active'
    print tournament_active_players
    if token in tournament_waiting_players:
        tournament_waiting_players.remove(token)

def set_tournament_skill_action(token):
    """Set tournament player did use skill

    Args:
        token (str): user identifier
    """
    global tournament_player_token
    global tournament_player_timeout
    if token in tournament_player_timeout:
        timeout = tournament_player_timeout[token]
        tournament_player_timeout.pop(token, None)
        tournament_player_token.pop(timeout, None)

def get_tournament_players_with_timeout(timeout):
    """Get all players that missed their turn

    Args:
        timeout (str): time now
    """
    global tournament_player_token
    if timeout in tournament_player_token:
        return tournament_player_token[timeout]

def tournament_player_turn_init(token):
    """Init tournament player turn

    Args:
        token (str): user identifier
    """
    global tournament_player_token
    global tournament_player_timeout
    opponent_waiting_timeout = (datetime.datetime.now() + datetime.timedelta(seconds=TOURNAMENT_OPPONENT_WAITING_TIMEOUT)).strftime('%H%M%S')
    if opponent_waiting_timeout not in tournament_player_token:
        tournament_player_token[opponent_waiting_timeout] = []
    tournament_player_token[opponent_waiting_timeout].append(token)
    tournament_player_timeout[token] = opponent_waiting_timeout

def is_tournament_player_in_battle(token):
    """Get if tournament player is in battle or wait for an opponent

    Args:
        token (str): user identifier
    """
    global tournament_active_players
    global tournament_waiting_players
    print token
    print tournament_active_players
    if (token in tournament_active_players) or (token in tournament_waiting_players):
        return True
    return False

def delete_tournament_battle_for_players(player_1_token, player_2_token):
    """Delete battle for two users

    Args:
        player_1_token (str): user identifier
        player_2_token (str): user identifier
    """
    delete_tournament_by_token(player_1_token)
    delete_tournament_by_token(player_2_token)

def delete_tournament_by_token(token):
    """Delete all tournament info for user

    Args:
        token (str): user identifier
    """
    global tournament_player_token
    global tournament_player_timeout
    global tournament_waiting_players
    global tournament_active_players
    if token in tournament_player_timeout:
        timeout = tournament_player_timeout[token]
        tournament_player_timeout.pop(token, None)
        tournament_player_token.pop(timeout, None)
    if token in tournament_active_players:
        tournament_active_players.remove(token)
    if token in tournament_waiting_players:
        tournament_waiting_players.remove(token)
