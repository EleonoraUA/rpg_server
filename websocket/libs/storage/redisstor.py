import redis
import datetime

USER_IS_ONLINE_TIMEOUT_SECONDS = 600

r = redis.StrictRedis(host='localhost', port=6379, db=0)

def set_user_is_online(token):
    global r
    timeout = (datetime.datetime.now() + datetime.timedelta(seconds=USER_IS_ONLINE_TIMEOUT_SECONDS)).strftime('%Y%m%d%H%M%S')
    print datetime.datetime.now()
    print timeout
    r.set('online:' + token, timeout)