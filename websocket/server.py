import sys
import tornado.httpserver
import tornado.ioloop
import tornado.web

from app.handlers.WSArenaHandler import WSArenaHandler
from app.handlers.WSMonsterHandler import WSMonsterHandler
from app.handlers.WSTournamentHandler import WSTournamentHandler
from app.models.monsterbattle.monster_turn_monitoring import monster_turn_monitoring
from app.models.pvpbattle.arenabattle.arena_monitoring import arena_monitoring
from app.models.pvpbattle.tournamentbattle.tournament_monitoring import tournament_monitoring

application = tornado.web.Application([
    (r'/monster_battle', WSMonsterHandler),
    (r'/arena_battle', WSArenaHandler),
    (r'/tournament_battle', WSTournamentHandler)
])

arena_monitoring.start_arena_monitoring()
monster_turn_monitoring.start_monster_monitoring()
tournament_monitoring.start_tournament_monitoring()

if __name__ == "__main__":
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(8888)
    #myIP = socket.gethostbyname(socket.gethostname())
    myIP = '10.55.33.15'
    print '*** Websocket Server Started at %s***' % myIP
    try:
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        sys.exit(0)
